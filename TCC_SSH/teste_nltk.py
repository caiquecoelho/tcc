#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
import pandas as pd
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline
from sklearn import cross_validation

import nltk

from sklearn.externals.six import StringIO

def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

def removeStopWords(texto):
	frases = []

	for (palavras, emocao) in texto:
		semstop = [p for p in palavras.split() if p not in stopwordsnltk]
		frases.append((semstop, emocao))

	return frases


def aplicaStemmer(texto):
	stemmer = nltk.stem.RSLPStemmer()
	frasesStemming = []
	for (palavras, emocao) in texto:
		#print(palavras)
		comStemming = [str(stemmer.stem(p)) for p in palavras if p not in stopwordsnltk]
		frasesStemming.append((comStemming, emocao))

	return frasesStemming

def buscaPalavras(frases):
	todasPalavras = []
	for (palavras, emocao) in frases:
		todasPalavras.extend(palavras)

	return todasPalavras

def buscaFrequencia(palavras):
	palavras = nltk.FreqDist(palavras)
	return palavras

def buscaPalavrasUnicas(frequencia):
	freq = frequencia.keys()
	return freq

def extratorPalavras(documento):
	doc = set(documento)
	caracteristicas = {}
	for palavras in palavrasUnicas:
		caracteristicas['%s' %palavras] = (palavras in doc)

	return caracteristicas

accuracyMultinomial = list()

#classificacoes = pd.read_csv("/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10")
classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database2.csv")
textosPuros = classificacoes['Tarefa']
#print(textosPuros)
marcacoes = classificacoes['Estimativa']
tam = len(textosPuros)

textos = []
for i in range(tam):
	texto = str(textosPuros[i])
	texto = texto.lower()
	estimativa = str(marcacoes[i])
	append = (texto, estimativa)
	textos.append(append)
	

#coloco todo mundo em minusculo para nao diferenciar, maiusculo e divido palavra por palavra 
textosQuebrados = textosPuros.str.lower().str.split(' ')
#textosQuebrados = textosPuros.str.split(' ')


#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()

for lista in textosQuebrados:
	dicionario.update(lista)

totalDePalavras = len(dicionario)

#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}


vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in textosQuebrados]


stopwordsnltk = nltk.corpus.stopwords.words('portuguese')
stopwordsnltk.append('vou')
stopwordsnltk.append('vai')
stopwordsnltk.append('tão')
stopwordsnltk.append('alguma')
stopwordsnltk.append('interesse')
stopwordsnltk.append('ter')
stopwordsnltk.append('caso')
stopwordsnltk.append('abaixo')
stopwordsnltk.append('animais')
stopwordsnltk.append('ainda')
stopwordsnltk.append('outras')
stopwordsnltk.append('etc.')
stopwordsnltk.append('em')



frasesStopWord = removeStopWords(textos)
#frasesComStemming = aplicaStemmer(frasesStopWord)
palavras = buscaPalavras(frasesStopWord)
frequencia = buscaFrequencia(palavras)
palavrasUnicas = buscaPalavrasUnicas(frequencia)


X = np.array(vetoresDeTexto)
Y = np.array(marcacoes.tolist())

#data_train_total = frasesComStemming
data_train_total = frasesStopWord
tamanho = len(data_train_total)
train_tam = int(tamanho/10)
data_train = data_train_total[int(train_tam * 8):]
data_test = data_train_total[:int(train_tam * 2)]
target_train_total = marcacoes

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


'''
count_vect = CountVectorizer()

X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target
'''


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)

accuracyMultinomial = list()



'''
baseCompleta = nltk.classify.apply_features(extratorPalavras, data_train)
baseCompletaTeste = nltk.classify.apply_features(extratorPalavras, data_test)

classificador = nltk.NaiveBayesClassifier.train(baseCompleta)

print(nltk.classify.accuracy(classificador, baseCompletaTeste))

'''



training_set = nltk.classify.apply_features(extratorPalavras, data_train_total)

cv = cross_validation.KFold(len(training_set), n_folds=3)

for traincv, testcv in cv:
	classifier = nltk.NaiveBayesClassifier.train(training_set[traincv[0]:traincv[len(traincv)-1]])
	accuracyMultinomial.append(nltk.classify.util.accuracy(classifier, training_set[testcv[0]:testcv[len(testcv)-1]]))
	print ('accuracy:', nltk.classify.util.accuracy(classifier, training_set[testcv[0]:testcv[len(testcv)-1]]))

	print(classifier.show_most_informative_features(20))

	erros = []
	for (frase, classe) in training_set:
		#print(frase)
		#print(classe)
		resultado = classifier.classify(frase)
		if resultado != classe:
			erros.append((classe, resultado, frase))

	#for (classe, resultado, frase) in erros:
	#	print(classe, resultado, frase)

	from nltk.metrics import ConfusionMatrix
	esperado = []
	previsto = []
	for(frase, classe) in training_set:
		resultado = classifier.classify(frase)
		esperado.append(classe)
		previsto.append(resultado)

	#esperado = 'alegria alegria alegria alegria medo medo surpresa surpresa'.split()
	#previsto = 'alegria alegria medo surpresa medo medo medo surpresa'.split()
	matrix = ConfusionMatrix(esperado, previsto)
	print(matrix)


print(accuracyMultinomial)
avg = np.mean(accuracyMultinomial)
print(avg)
