#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
import pandas as pd
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline
from sklearn import cross_validation

import nltk

from sklearn.externals.six import StringIO

def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

def removeStopWords(texto):
	frases = []

	for (palavras, emocao) in texto:
		semstop = [p for p in palavras.split() if p not in stopwordsnltk]
		frases.append((semstop, emocao))

	return frases


def aplicaStemmer(texto):
	stemmer = nltk.stem.RSLPStemmer()
	frasesStemming = []
	for (palavras, emocao) in texto:
		#print(palavras)
		comStemming = [str(stemmer.stem(p)) for p in palavras if p not in stopwordsnltk]
		frasesStemming.append((comStemming, emocao))

	return frasesStemming

def buscaPalavras(frases):
	todasPalavras = []
	for (palavras, emocao) in frases:
		todasPalavras.extend(palavras)

	return todasPalavras

def buscaFrequencia(palavras):
	palavras = nltk.FreqDist(palavras)
	return palavras

def buscaPalavrasUnicas(frequencia):
	freq = frequencia.keys()
	return freq

def extratorPalavras(documento):
	doc = set(documento)
	caracteristicas = {}
	for palavras in palavrasUnicas:
		caracteristicas['%s' %palavras] = (palavras in doc)

	return caracteristicas

accuracyMultinomial = list()
accuracyRF = list()

#classificacoes = pd.read_csv("/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10")
classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database2.csv")
textosPuros = classificacoes['Tarefa']
#print(textosPuros)
marcacoes = classificacoes['Estimativa']
tam = len(textosPuros)

textos = []
for i in range(tam):
	texto = str(textosPuros[i])
	texto = texto.lower()
	estimativa = str(marcacoes[i])
	append = (texto, estimativa)
	textos.append(append)
	#print(append)


#print(type(textos))

#print(textos)

#textos = np.array(textos)
#print(textosQuebrados)


#coloco todo mundo em minusculo para nao diferenciar, maiusculo e divido palavra por palavra 
textosQuebrados = textosPuros.str.lower().str.split(' ')

print(type(textosQuebrados))
print(textosQuebrados)

#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()
for lista in textosQuebrados:
	dicionario.update(lista)

totalDePalavras = len(dicionario)
#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}

#print totalDePalavras

#print vetorizar_texto(textosQuebrados[0], tradutor)
vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in textosQuebrados]


stopwordsnltk = nltk.corpus.stopwords.words('portuguese')
stopwordsnltk.append('vou')
stopwordsnltk.append('vai')
stopwordsnltk.append('tão')

frasesStopWord = removeStopWords(textos)
frasesComStemming = aplicaStemmer(frasesStopWord)
palavras = buscaPalavras(frasesComStemming)
frequencia = buscaFrequencia(palavras)
palavrasUnicas = buscaPalavrasUnicas(frequencia)


X = np.array(vetoresDeTexto)
Y = np.array(marcacoes.tolist())

#print(X)

data_train_total = frasesComStemming
'''
tamanho = len(data_train_total)
train_tam = int(tamanho/10)
data_train = data_train_total[int(train_tam * 8):]
data_test = data_train_total[:int(train_tam * 2)]
'''
target_train_total = marcacoes


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)

accuracyMultinomial = list()



####### Parameters tuning

#MultinomialNB
param_grid = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'fit_prior': (True, False)
}

multinomialNB = MultinomialNB()

multinomialNB_grid = GridSearchCV(multinomialNB, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
multinomialNB_grid.fit(X, Y)
multinomialNBBestParams = multinomialNB_grid.best_params_

print("MultinomialNB Best Params: ")
print(multinomialNBBestParams)
print("")


for train, test in kf.split(X, Y):
	data_train, target_train = X[train], Y[train]
	data_test, target_test   = X[test], Y[test]

	########################################################## MultinomialNB
	multinomial = MultinomialNB(**multinomialNBBestParams)
	multinomial.fit(data_train, target_train)
	output = multinomial.predict(data_test)
	accuracyMultinomial.append(metrics.accuracy_score(target_test, output))

	########################################################## RandomForestClassifier

	random_clf = RandomForestClassifier(min_samples_split= 30, n_estimators= 20, random_state= 0, criterion= 'gini', max_depth= 20)
	random_clf.fit(data_train, target_train)

	output = random_clf.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))


print(accuracyMultinomial)
avg = np.mean(accuracyMultinomial)
print(avg)

print()
print(accuracyRF)
avg = np.mean(accuracyRF)
print(avg)

