#!-*- coding: utf8 -*-
import time
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.cluster import AgglomerativeClustering


#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 15 palavras
text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/Estimativas/database_3/6_Categorias/15", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/15", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

count_vect = CountVectorizer(ngram_range=(1, 2))
#count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(text.data)

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)


data_train_total = X_train_tfidf
target_train_total = text.target

tam = len(target_train_total)

print("\nESTIMATIVAS REAIS")
print(target_train_total)
print("\n")

#Distribuicao dos dados
print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")
um = 0
dois = 0
tres = 0
quatro = 0
seis = 0
oito = 0

for t in text.target:
	if(text.target_names[t] == "1"):
		um += 1 
	if(text.target_names[t] == "2"):
		dois += 1 
	if(text.target_names[t] == "3"):
		tres += 1 
	if(text.target_names[t] == "4"):
		quatro += 1 
	if(text.target_names[t] == "6"):
		seis += 1 
	if(text.target_names[t] == "8"):
		oito += 1 

if(dois == 0 and quatro == 0 and oito == 0):
	print("ATENÇÃO VOCÊ ESTÁ USANDO O DATASET COM APENAS 3 CATEGORIAS\n")

porcentagem_estimativa_1 = 100.0 * um/tam
porcentagem_estimativa_2 = 100.0 * dois/tam
porcentagem_estimativa_3 = 100.0 * tres/tam
porcentagem_estimativa_4 = 100.0 * quatro/tam
porcentagem_estimativa_6 = 100.0 * seis/tam
porcentagem_estimativa_8 = 100.0 * oito/tam

print("Total de tarefas com estimativa 1: " +str(um) + " em porcentagem temos: " +str(porcentagem_estimativa_1))
print("Total de tarefas com estimativa 2: " +str(dois) + " em porcentagem temos: " +str(porcentagem_estimativa_2))
print("Total de tarefas com estimativa 3: " +str(tres) + " em porcentagem temos: " +str(porcentagem_estimativa_3))
print("Total de tarefas com estimativa 4: " +str(quatro) + " em porcentagem temos: " +str(porcentagem_estimativa_4))
print("Total de tarefas com estimativa 6: " +str(seis) + " em porcentagem temos: " +str(porcentagem_estimativa_6))
print("Total de tarefas com estimativa 8: " +str(oito) + " em porcentagem temos: " +str(porcentagem_estimativa_8))

classe_majoritaria = 1
total_classe_majoritaria = um

if(dois > total_classe_majoritaria):
	classe_majoritaria = 2
	total_classe_majoritaria = dois

if(tres > total_classe_majoritaria):
	classe_majoritaria = 3
	total_classe_majoritaria = tres

if(quatro > total_classe_majoritaria):
	classe_majoritaria = 4
	total_classe_majoritaria = quatro

if(seis > total_classe_majoritaria):
	classe_majoritaria = 6
	total_classe_majoritaria = seis

if(oito > total_classe_majoritaria):
	classe_majoritaria = 8
	total_classe_majoritaria = oito

total_de_elementos = len(target_train_total)
taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

print("Classe Majoritária é: " +str(classe_majoritaria))
print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
print(str(taxa_de_acerto_algoritmo_burro) + "%")

################################################# AGGLOMERATIVE CLUSTERING

# Ward can only work with euclidean distances
for linkage in ('ward', 'average', 'complete'):
	clustering = AgglomerativeClustering(linkage=linkage, n_clusters=6, affinity = 'euclidean')
	t0 = time.time()
	clustering.fit(data_train_total.toarray())
	y = clustering.fit_predict(data_train_total.toarray())
	elapsed_time = time.time() - t0
	print("##################################################################################")
	print("\n")
	print("%s, euclidean : %.2fs" % (linkage, elapsed_time))
	print("FIT_PREDICT:")
	print y
	print "Tamanho: " +str(tam)
	print type(y)

	#Distribuicao dos dados
	print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")
	um = 0
	dois = 0
	tres = 0
	quatro = 0
	seis = 0
	oito = 0

	for index in range(0, tam):

		if(text.target_names[y[index]] == "1"):
			um += 1 
		if(text.target_names[y[index]] == "2"):
			dois += 1 
		if(text.target_names[y[index]] == "3"):
			tres += 1 
		if(text.target_names[y[index]] == "4"):
			quatro += 1 
		if(text.target_names[y[index]] == "6"):
			seis += 1 
		if(text.target_names[y[index]] == "8"):
			oito += 1 

	if(dois == 0 and quatro == 0 and oito == 0):
		print("ATENÇÃO VOCÊ ESTÁ USANDO O DATASET COM APENAS 3 CATEGORIAS\n")

	porcentagem_clustering_1 = 100.0 * um/tam
	porcentagem_clustering_2 = 100.0 * dois/tam
	porcentagem_clustering_3 = 100.0 * tres/tam
	porcentagem_clustering_4 = 100.0 * quatro/tam
	porcentagem_clustering_6 = 100.0 * seis/tam
	porcentagem_clustering_8 = 100.0 * oito/tam

	print("Total de tarefas no cluster 1: " +str(um) + " em porcentagem temos: " +str(porcentagem_clustering_1))
	print("Total de tarefas no cluster 2: " +str(dois) + " em porcentagem temos: " +str(porcentagem_clustering_2))
	print("Total de tarefas no cluster 3: " +str(tres) + " em porcentagem temos: " +str(porcentagem_clustering_3))
	print("Total de tarefas no cluster 4: " +str(quatro) + " em porcentagem temos: " +str(porcentagem_clustering_4))
	print("Total de tarefas no clustera 6: " +str(seis) + " em porcentagem temos: " +str(porcentagem_clustering_6))
	print("Total de tarefas no cluster 8: " +str(oito) + " em porcentagem temos: " +str(porcentagem_clustering_8))

	for index in range(0, tam):
		estimativa_predict = text.target_names[y[index]]
		nome_arquivo = str(linkage) + "_euclidean_" "estimativa_" +str(estimativa_predict) +"_tarefa_"+str(index)
		#file = open(nome_arquivo, "w")
		#dado = str(text.data[index])
		#file.write(dado) 
		#file.close() 

for linkage in ('average', 'complete'):
	clustering = AgglomerativeClustering(linkage=linkage, n_clusters=6, affinity = 'cosine')
	t0 = time.time()
	clustering.fit(data_train_total.toarray())
	y = clustering.fit_predict(data_train_total.toarray())
	elapsed_time = time.time() - t0
	print("##################################################################################")
	print("\n")
	print("%s, cosine : %.2fs" % (linkage, elapsed_time))
	print("FIT_PREDICT:")
	print y
	print "Tamanho: " +str(tam)
	print type(y)


	#Distribuicao dos dados
	print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")
	um = 0
	dois = 0
	tres = 0
	quatro = 0
	seis = 0
	oito = 0

	for index in range(0, tam):

		if(text.target_names[y[index]] == "1"):
			um += 1 
		if(text.target_names[y[index]] == "2"):
			dois += 1 
		if(text.target_names[y[index]] == "3"):
			tres += 1 
		if(text.target_names[y[index]] == "4"):
			quatro += 1 
		if(text.target_names[y[index]] == "6"):
			seis += 1 
		if(text.target_names[y[index]] == "8"):
			oito += 1 

	if(dois == 0 and quatro == 0 and oito == 0):
		print("ATENÇÃO VOCÊ ESTÁ USANDO O DATASET COM APENAS 3 CATEGORIAS\n")

	porcentagem_clustering_1 = 100.0 * um/tam
	porcentagem_clustering_2 = 100.0 * dois/tam
	porcentagem_clustering_3 = 100.0 * tres/tam
	porcentagem_clustering_4 = 100.0 * quatro/tam
	porcentagem_clustering_6 = 100.0 * seis/tam
	porcentagem_clustering_8 = 100.0 * oito/tam

	print("Total de tarefas no cluster 1: " +str(um) + " em porcentagem temos: " +str(porcentagem_clustering_1))
	print("Total de tarefas no cluster 2: " +str(dois) + " em porcentagem temos: " +str(porcentagem_clustering_2))
	print("Total de tarefas no cluster 3: " +str(tres) + " em porcentagem temos: " +str(porcentagem_clustering_3))
	print("Total de tarefas no cluster 4: " +str(quatro) + " em porcentagem temos: " +str(porcentagem_clustering_4))
	print("Total de tarefas no cluster 6: " +str(seis) + " em porcentagem temos: " +str(porcentagem_clustering_6))
	print("Total de tarefas no cluster 8: " +str(oito) + " em porcentagem temos: " +str(porcentagem_clustering_8))


	for index in range(0, tam):
		estimativa_predict = text.target_names[y[index]]
		nome_arquivo = str(linkage) + "_cosine_" "estimativa_" +str(estimativa_predict) +"_tarefa_"+str(index)
		#file = open(nome_arquivo, "w")
		#dado = str(text.data[index])
		#file.write(dado) 
		#file.close() 