#!-*- coding: utf8 -*-

from __future__ import print_function
from sklearn.datasets import load_files
from sklearn import metrics
from sklearn import preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation

n_features = 1000 #construa um vocabulario que considere apenas as principais max_features ordenadas pela frequencia do termo em todo o corpus
n_components = 6 #numero de topicos de saida
n_top_words = 20  #numero de palavras em cada topico de saída

def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += ", ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    print()


#Possibilidades de estimativa
categories = ['cluster_1', 'cluster_2', 'cluster_3', 'cluster_4', 'cluster_6', 'cluster_8']

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 15 palavras
#average_cosine, average_euclidean, complete_cosine, complete_euclidean, ward_euclidean
text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/estimativa_clustering/average_cosine", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

n_samples = len(text.data)
data_samples = text.data[:n_samples]

tfidf_vectorizer = TfidfVectorizer(max_df=0.95, min_df=2, max_features=n_features)
#tfidf_vectorizer = TfidfVectorizer()
tfidf = tfidf_vectorizer.fit_transform(data_samples)

tf_vectorizer = CountVectorizer(ngram_range=(1, 2), max_df=0.95, min_df=2, max_features=n_features)
#tf_vectorizer = CountVectorizer(ngram_range=(1,2))
tf = tf_vectorizer.fit_transform(data_samples)


########################################### sklearn.decomposition.LatentDirichletAllocation


# Fit the NMF model
print("Fitting the NMF model (Frobenius norm) with tf-idf features, " "n_samples=%d and n_features=%d..." % (n_samples, n_features))
nmf = NMF(n_components=n_components, random_state=1, alpha=.1, l1_ratio=.5).fit(tfidf)

print("\nTopics in NMF model (Frobenius norm):")
tfidf_feature_names = tfidf_vectorizer.get_feature_names()
print_top_words(nmf, tfidf_feature_names, n_top_words)




# Fit the LDA model
print("Fitting LDA models with tf features, " "n_samples=%d and n_features=%d..."
	% (n_samples, n_features))

lda = LatentDirichletAllocation(n_components = n_components, max_iter=5, learning_method='online', learning_offset=50., random_state=0)

lda.fit(tf)

print("\nTopics in LDA model:")
tf_feature_names = tf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, n_top_words)