A estimativa considera a geração de um intent para exibição do local no google maps traçando a rota até o local.
Caso essa localização deva ser exibida dentro do app a pontuação é de 4 pontos.
Caso essa localização deva ser exibida dentro do app junto à uma rota a pontuação é de 8 pontos.