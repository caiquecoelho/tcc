Expansível
Registro de anotações da aula em visualização.
Na Área do Curso pode-se visualizar todas as anotações realizadas em aulas e editá-las.
Obs. criar ou editar uma anotação ele dá erro que se deve enviar o parametro “AlteradoEm”. Na doc não tem esse parametro e não faz sentido mesmo ter esse parametro