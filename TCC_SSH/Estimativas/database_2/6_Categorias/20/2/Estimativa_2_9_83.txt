É mostrado quando não tem um usuário logado.
Tela com:
   - Placeholder da foto do usuário, texto de login.
   - Botão de fazer login (Manda para a tela de login)
   - Botão para cadastro (Mandar para a tela de cadastro)
   - Botão de configurações
    - Botão para tela de fale conosco
    - Botão para tela de sobre nós
    - Botão para tela de termos de uso
    - Botão para tela de politicas de privacidade