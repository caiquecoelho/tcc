1. Dia da semana autorizado a usar o cartão.
2. Horário permitido para uso do cartão.
3. Valor máximo de gasto por dia por cartão.
4. Valor máximo de gasto por corrida de taxi.
5. Empresa exige desconto de X%
6. Empresa exige digitação de centro de custo? Sim ou Não
7. Empresa exige digitação do campo de observação? Sim ou Não