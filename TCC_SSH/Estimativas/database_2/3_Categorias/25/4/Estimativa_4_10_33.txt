Tela com:
  - Quantidade de pontos acumulados (Quando logado) .
  -Logo, nome, e pontos, icone com porcentagem.
  -Descrição do cupon
  - Botão para detalhes.
  - Botão canto superior direito (ação: vai para tela de meus cupons)
  - Quantidade de cupons restantes.
  - Header com pesquisa.
  - Páginação