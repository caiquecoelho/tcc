Tela com escolha de tipos e subtimos de produtos, que são:

- Herbcidas: 
  -- pré-emergentes
  -- pós-emergente

- Fungicidas:
  -- sistêmicos
  -- contato

- Inseticida:
  -- Não tem subtipo

Pode adicionar mais de um tipo de produto assim podendo apagar (ao menos 1 selecionado)

Salvar os tipos e subtipos de produtos no banco local

Duvidas:

- Pode adicionar o mesmo tipo de produto mais de uma vez?
- Pode adicionar o mesmo tipo de produto com subtipos diferentes?
- Tem um máximo de produtos para adicionar?