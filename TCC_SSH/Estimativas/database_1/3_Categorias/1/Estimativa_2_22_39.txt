Células com:
   -Nome da ONG
   -Quantos pontos a pessoa ganhou
   -Quando foi (data)
   -O quanto foi doado (valor)
-Ação: Abre popup com mesma coisa da celulas, mais número do cartão (só os úlitmos digitos)
   -Botão de fechar o popup
   -Botão de compartilhar (ação: Compartilhar doação nas redes)
   -Botão para tela da ONG
-paginação
-Obs. O doador pode contestar o pagamento e o sistema tem que reverter a pontuação e etc.. E o estado pode mudar após a ""aprovação"" do pagamento (não é estado final). Então tem que tratar todos os estados possíveis (retirado do mercadopago):
approved
O pagamento foi aprovado e creditado.
pending
O usuário não concluiu o processo de pagamento.
in_process
O pagamento está sendo analisado.
rejected
O pagamento foi recusado. O usuário pode tentar novamente.
refunded
(estado terminal) O pagamento foi devolvido ao usuário.
cancelled
(estado terminal) O pagamento foi cancelado por superar o tempo necessário para ser efetuado ou por alguma das partes.
in_mediation
Foi iniciada uma disputa para o pagamento.
charged_back
(estado terminal) Realizaram uma contestação no cartão de crédito.