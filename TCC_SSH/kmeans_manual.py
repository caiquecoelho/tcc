#!/usr/bin/env python
#!-*- coding: utf8 -*-

from __future__ import print_function

import time
import sys
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pandas as pd
import nltk

from sklearn.metrics import adjusted_rand_score
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import adjusted_rand_score


def distribuicao_de_dados_no_dataset():
	print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

	total = 0
	total_classe_majoritaria = 0
	classe_majoritaria = categories[0]
	
	for categorie in categories:
		for t in text.target:
			if(text.target_names[t] == categorie):
				total += 1 

		porcentagem = 100.0 * total/tam
		print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + " tarefas, em porcentagem temos: " +str(int(porcentagem))  + "%")

		if(total > total_classe_majoritaria):
			classe_majoritaria = categorie
			total_classe_majoritaria = total

		total = 0

	total_de_elementos = len(target_train_total)
	taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

	print("\nClasse Majoritária é: " +str(classe_majoritaria))
	print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
	print(str(taxa_de_acerto_algoritmo_burro) + "%")
	print("\n\n")


def distribuicao_dos_dados_nos_clusters():
	print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")

	total = 0

	for cluster in range(0, n_clusters):
		for index in range(0, tam):
			if(y[index] == cluster):
				total+=1

		porcentagem = 100.0 * total/tam
		print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: " +str(int(porcentagem))  + "%")

		total = 0

def salvar_output_clusters():
	file = open(nome_arquivo, "w")
	dado = str(text.data[index])
	file.write(dado) 
	file.close()


def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

def removeStopWords(texto):
	frases = []

	for palavras in texto:
		semstop = [p for p in palavras.split() if p not in stop_words]
		frases.append(semstop)

	return frases


def aplicaStemmer(texto):
	stemmer = nltk.stem.RSLPStemmer()
	frasesStemming = []
	for palavras in texto:
		#print(palavras)
		comStemming = [str(stemmer.stem(p)) for p in palavras if p not in stop_words]
		frasesStemming.append(comStemming)

	return frasesStemming

def buscaPalavras(frases):
	todasPalavras = []
	for palavras in frases:
		todasPalavras.extend(palavras)

	return todasPalavras

def buscaFrequencia(palavras):
	palavras = nltk.FreqDist(palavras)
	return palavras

def buscaPalavrasUnicas(frequencia):
	freq = frequencia.keys()
	return freq

def extratorPalavras(documento):
	doc = set(documento)
	caracteristicas = {}
	for palavras in palavrasUnicas:
		caracteristicas['%s' %palavras] = (palavras in doc)

	return caracteristicas


########################################################################################################

if len (sys.argv)!=2:
	print("Use:",sys.argv[0],"n_clusters")
	exit()


n_clusters = int(sys.argv[1])

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai"}

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


#Extracao de palavras e vetorizacao manual
#classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database2.csv")
classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database_final.csv")
nomesTarefas = classificacoes['Tarefa']
tarefas = classificacoes['Tarefa'] +" "+ classificacoes['Descricao']
tipo = classificacoes['Tipo']
descricoesTarefas = classificacoes['Descricao']
marcacoes = classificacoes['Estimativa']
tam = len(nomesTarefas)

#print(tarefas[1166])

stop_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
caracteres = ["~", "^", "=", "?","!",'"', "+", ",", ".", "-", "'", "@", "#", "$", "%", "&", "*", "(", ")", "_", "<", ">", "{", "}", "[", "]", ";", ":", "\n", '\t', '/', '...']

textos = []
for i in range(tam):
	texto = str(tarefas[i])
	texto = texto.lower()

	for number in stop_numbers:
		texto = texto.replace(number, "")
	for caractere in caracteres:
		texto = texto.replace(caractere, "")

	textos.append(texto)

#print(textos)

textosQuebrados = tarefas.str.lower().str.split(' ')


for number in stop_numbers:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(number, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
		while(indice < tamTexto):
			texto.pop(indice)
			indice +=1
			tamTexto -= 1
		indice = 0


for caractere in caracteres:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(caractere, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
			elif(palavra == caractere):
				texto.pop(indice)
				indice +=1
				tamTexto -=1
		while(indice < tamTexto):
			texto.pop(indice)
			#print(texto)
			indice +=1
			tamTexto -= 1
		indice = 0


#print(textosQuebrados[0])
#for texto in textosQuebrados:
#	print(texto)

	

#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()
i = 0
for lista in textosQuebrados:
	#print(lista)
	#print(i)
	dicionario.update(lista)
	i+=1

totalDePalavras = len(dicionario)
#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}

#print totalDePalavras

#print vetorizar_texto(textosQuebrados[0], tradutor)
vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in textosQuebrados]


#Verificando se a vetorizacao esta correta
'''
print(textosQuebrados[0])


indice = 0
indiceTradudor = 0
tamTradutor = len(tradutor)
#print(tradutor['chat'])
for existe in vetoresDeTexto[0]:
	if(existe != 0):
		for palavra in tradutor:
			#print(tradutor[palavra])
			if(tradutor[palavra] == indice):
				break
			indiceTradudor += 1 
		print(indiceTradudor)
		indiceTradudor = 0
	indice+=1


print(tradutor)
'''

'''
frasesStopWord = removeStopWords(textos)
#print(frasesStopWord)
frasesComStemming = aplicaStemmer(frasesStopWord)
palavras = buscaPalavras(frasesStopWord)
frequencia = buscaFrequencia(palavras)
palavrasUnicas = buscaPalavrasUnicas(frequencia)
'''


X = np.array(vetoresDeTexto)
y = np.array(marcacoes.tolist())


#distribuicao_de_dados_no_dataset()

tam = len(X)

#true_k = 2
model = KMeans(n_clusters = n_clusters, init = 'k-means++', max_iter= 100, n_init = 1)
y = model.fit_predict(X)
#model.fit(data_train_total)

print("\n\n")
print("Cluster Result with fit_predict method:")
print(y)

distribuicao_dos_dados_nos_clusters()


'''
print("Top terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()
for i in range(n_clusters):
    print ("Cluster %d:" % i,)
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind])
    print("")
'''

'''
for index in range(0, tam):
		estimativa_predict = model.labels_[index]
		nome_arquivo = "cluster_" +str(estimativa_predict) +"_tarefa_"+str(text.target[index])
		salvar_output_clusters()
'''