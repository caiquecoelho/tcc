#Possibilidades de estimativa
#categories = ['2', '3', '4', '6', '8']
categories = ['1', '2', '3', '4', '6', '8']

#Importando algoritmo para extracao de texto das descricoes de tarefas
from sklearn.datasets import load_files

#Extracao de descricoes de tarefa
text = load_files(container_path = "/home/caique_coelho/Documentos/TCC/estimativas", categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Possibilidade de estimativas carregadas
#print("Possibilidades de estimativas carregadas: ")
#print(text.target_names)

#Tamanho do conteudo importado
#print("Tamanho do conteudo carregado: ")
#print(len(text.data))

#Filenames
#print("Filenames: ")
#print(len(text.filenames))

#Imprime a primeira linha do primeiro arquivo carregado
#print("\n".join(text.data[0].split("\n")[:3]))


#print(text.target_names[text.target[0]])

#The category integer id of each sample is stored in the
#print(text.target[:10])

#As categorias podem ser vistas com os seus nomes com o atributo text.target_names[indice]
#for t in text.target[:10]:
#	print(text.target_names[t])


#Importando feature para transformar os documentos em vetores de numeros
from sklearn.feature_extraction.text import CountVectorizer
#Instanciando CountVectorizer
count_vect = CountVectorizer()
#Transformando o texto em um vetor de numeros
#O indice de uma palavra no vocabulario fica linkado com a sua frequencia no corpo no documento
X_train_counts = count_vect.fit_transform(text.data)

#print("Shape")
#print(X_train_counts.shape)

#print("U'algorithm")
#print(count_vect.vocabulary_.get(u'algorithm'))

#Importando TfidTransformer afim de divir a quantidade de ocorrencia das palavas pelo numero de palavras no documento
#E afim de reduzir o peso das palavras que ocorrem muitas vezes em um documento
from sklearn.feature_extraction.text import TfidfTransformer
#Instanciando TfidTransformer
tfidf_transformer = TfidfTransformer()
#Divindo a quantidade de ocorrencia das palavras pelo numero de palavras do documento a aplicando os pesos inversos
#Term Frequencies e Term Frequency times Inverse Document Frequency
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
#print(X_train_tfidf.shape)

#Preparando dados que serao preditos, eh feito quase o mesmo processo passado para extrair o texto, no entanto nao usamos
#fit_transform apenas transform, uma vez que esses dados nao sao de treino
# Estimativas reais: 3, 6, 8 e 1
docs_new = ['Ao digitar, o app esconde a opcao dos pontos de interesse e passa a exibir sugestoes de locais', 'Quando usuario fez login o botao de doacao vai para uma webview. Quando o login ainda nao foi feito vai para a tela de login',
'Filtro com rolagem infinita por mes', 'Caso o usuario nao possua todos dados necessarios em seu facebook, o cadastro sera aberto para o usuario completar seus dados']
X_new_counts = count_vect.transform(docs_new)
X_new_tfidf = tfidf_transformer.transform(X_new_counts)

# Estimativas reais: 2, 1, 4 
docs_new_validacao = ['Login com e-mail ou facebook, google+, twitter', 'Enviar instrucoes de recuperacao de senha para o e-mail cadastrado', 'Mostrar historico do cavalo, Provas que o animal participou']
X_new_counts_validacao = count_vect.transform(docs_new_validacao)
X_new_tfidf_validacao = tfidf_transformer.transform(X_new_counts_validacao)

#importando algoritmo de regressao linear do sklearn
from sklearn import linear_model
#instanciando o algoritmo
linear = linear_model.LinearRegression()

#definindo conjunto de dados a serem treinados
#dados
x_train= X_train_tfidf
#marcacao ou estimativa de treino
y_train= text.target

#treinando algoritmo
linear.fit(x_train, y_train)

#check score
linear.score(x_train, y_train)

#print('Coefficient: \n', linear.coef_)
#print('Intercept: \n', linear.intercept_)

#Predizendo resultado com regressao linear teste
predicted= linear.predict(X_new_tfidf)
print("Predict Regressao Linear Teste: ")
print("Estimativa real: 3, 6, 8, 1")
print(predicted)

print("")
print("")

#Predizendo resultado com regressao linear validacao
predicted= linear.predict(X_new_tfidf_validacao)
print("Predict Regressao Linear Validacao: ")
print("Estimativa real: 2, 1, 4")
print(predicted)

print("")
print("")

#importando algoritmo de classificacao MultiNomial Naive Bayes
from sklearn.naive_bayes import MultinomialNB
#instanciando o algoritmo e treinando com dados e marcacoes
clf = MultinomialNB().fit(X_train_tfidf, text.target)
print(X_train_tfidf)
#predizendo resultado com MultinomialNB
predicted = clf.predict(X_new_tfidf)

#resultado com MultinomialNB
print("MultinomialNB Teste: ")
print("Estimativa real: 3, 6, 8, 1")
for doc, category in zip(docs_new, predicted):
	print('%r => %s' % (doc, text.target_names[category]))

print("")
print("")

#Validacao com MultinomialNB
predicted_validacao = clf.predict(X_new_tfidf_validacao)

#resultado com MultinomialNB
print("MultinomialNB Validacao: ")
print("Estimativa real: 2, 1, 4")
for doc, category in zip(docs_new_validacao, predicted_validacao):
	print('%r => %s' % (doc, text.target_names[category]))

print("")
print("")