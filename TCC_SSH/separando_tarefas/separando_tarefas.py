#x dados que eu tenho e y lado que queremos prever
import csv

def separando():

	arquivo = open('database_final.csv', 'rb')
	leitor = csv.reader(arquivo)

	leitor.next()

	i = 1
	j = 1
	estimativa_atual = -2;
	estimativa_anterior = -1;

	#for Tarefa, Estimativa in leitor:
	for Tarefa, Estimativa, Tipo, Projeto, Descricao in leitor:

		if(str(Tipo).lower() == 'ios' or str(Tipo).lower() == 'app'):

			#print(str(Tipo))

			estimativa_atual = int(Estimativa)
			if(estimativa_anterior != estimativa_atual):
				j = 1
				estimativa_anterior = estimativa_atual

			nome_arquivo = "Estimativa_" +str(Estimativa) + "_" + str(j) +"_" + str(i) + ".txt"
			#nome_arquivo = str(Estimativa) + "PTs_" + str(Tipo) + "_" + str(j) +"_" + str(i) + ".txt"

			dado = str(Tarefa) + '\n' + str(Descricao)

			stop_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
			for number in stop_numbers:
				dado = dado.replace(number, " ")

			dado = dado.replace(",", " ")
			dado = dado.replace(".", " ")
			dado = dado.replace("-", " ")
			dado = dado.replace("'", " ")
			dado = dado.replace("@", " ")
			dado = dado.replace("#", " ")
			dado = dado.replace("$", " ")
			dado = dado.replace("%", " ")
			dado = dado.replace("&", " ")
			dado = dado.replace("*", " ")
			dado = dado.replace("(", " ")
			dado = dado.replace(")", " ")
			dado = dado.replace("_", " ")
			dado = dado.replace("<", " ")
			dado = dado.replace(">", " ")
			dado = dado.replace("{", " ")
			dado = dado.replace("}", " ")
			dado = dado.replace("[", " ")
			dado = dado.replace("]", " ")
			dado = dado.replace("\\", " ")
			dado = dado.replace("/", " ")
			dado = dado.replace("\n", " ")
			dado = dado.replace("\t", " ")
			dado = dado.replace("...", " ")
			dado = dado.replace(":", " ")
			dado = dado.replace(";", " ")

			dado.lower()

			#print(dado)

			#contando palavras na tarefa
			dadosQuebrados = dado.split(' ')

			numeroPalavras = 0
			for palavra in dadosQuebrados:
				numeroPalavras += 1

			#if(numeroPalavras >= 10):

			file = open(nome_arquivo, "w") 
		 
			file.write(dado) 
			file.close() 

			i = i + 1
			j = j + 1


separando()