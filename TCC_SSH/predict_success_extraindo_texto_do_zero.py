#!-*- coding: utf8 -*-

import pandas as pd
from collections import Counter
import numpy as np
from sklearn.cross_validation import cross_val_score
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import pandas as pd
import numpy as np
import scipy
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
from sklearn import model_selection
from sklearn import neural_network
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
import csv
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier


texto1 = "Se eu comprar cinco anos antecipados, eu ganho algum desconto?"
texto2 = "O exercício 15 do curso de Java 1 está com a resposta errada. Pode conferir por favor?"
texto3 = "Existe algum curso para cuidar do marketing da minha empresa"

def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

classificacoes = pd.read_csv("/exp/caique/TCC_SSH/separando_tarefas/database2.csv")
textosPuros = classificacoes['Tarefa']
#coloco todo mundo em minusculo para nao diferenciar, maiusculo e divido palavra por palavra 
textosQuebrados = textosPuros.str.lower().str.split(' ')

#print(type(textosQuebrados))

#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()
#print(type(dicionario))
for lista in textosQuebrados:
	dicionario.update(lista)

totalDePalavras = len(dicionario)
#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}

#print totalDePalavras

#print vetorizar_texto(textosQuebrados[0], tradutor)
vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in textosQuebrados]

marcacoes = classificacoes['Estimativa']

X = np.array(vetoresDeTexto)
Y = np.array(marcacoes.tolist())

data_train_total = X
target_train_total = Y


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyDT = list()
accuracySvm = list()
accuracyKnn = list()
accuracyLogReg = list()
accuracyNB = list()
accuracyMLP = list()

####### Parameters tuning


# DECISION TREE
decision_tree = tree.DecisionTreeClassifier()
param_dist = {'max_depth': [3, 4, 5, 6, 7, 8, 9, 10]}
random_search = RandomizedSearchCV(decision_tree, param_distributions=param_dist, n_iter=8, cv=3, scoring='accuracy')
random_search.fit(data_train_total, target_train_total)
decisionTreeBestParams = random_search.best_params_
print("Decision Tree Best Params: ")
print decisionTreeBestParams
print("")

# SVM
svc = svm.SVC()
param_dist = {'C': scipy.stats.expon(scale=100), 'gamma': scipy.stats.expon(scale=.1), 'kernel': ['rbf', 'linear'], 'max_iter': [2000]}
random_search = RandomizedSearchCV(svc, param_distributions=param_dist, n_iter=30, cv=3, scoring='accuracy')
random_search.fit(data_train_total, target_train_total)
svmBestParams = random_search.best_params_
print("SVM Best Params: ")
print svmBestParams
print("")

# KNN
# ValueError: Expected n_neighbors <= n_samples,  but n_samples = 12, n_neighbors = 13
knn = neighbors.KNeighborsClassifier(metric='euclidean')
param_dist = {'n_neighbors': list(np.arange(1, 15)), 'metric': ['euclidean'], 'weights': ['uniform', 'distance']}
random_search = RandomizedSearchCV(knn, param_distributions=param_dist, n_iter=28, scoring='accuracy')
random_search.fit(data_train_total, target_train_total)
knnBestParams = random_search.best_params_
print("KNN Best Params: ")
print knnBestParams
print("")

# MLP
mlp = neural_network.MLPClassifier()
param_dist = {'solver': ['sgd', 'adam'], 'learning_rate': ['constant', 'adaptive'],
				'momentum': scipy.stats.expon(scale=.1),
				'alpha': scipy.stats.expon(scale=.0001), 'activation': ['logistic'],
				'learning_rate_init': scipy.stats.expon(scale=.01),
				'hidden_layer_sizes': [(200, 50, 20), (100, 20), (100, 50), (200, 20), (200, 50), (50, 10),
				(200,), (100,), (50,)], 'max_iter': [2000]}
random_search = RandomizedSearchCV(mlp, param_distributions=param_dist, n_iter=50, cv=3, scoring='accuracy')
random_search.fit(data_train_total, target_train_total)
mlpBestParams = random_search.best_params_
print("MLP Best Params: ")
print mlpBestParams
print("")


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

	########################################################## DECISION TREE

	decision_tree = tree.DecisionTreeClassifier(**decisionTreeBestParams)
	decision_tree = decision_tree.fit(data_train, target_train)

	output = decision_tree.predict(data_test)
	accuracyDT.append(metrics.accuracy_score(target_test, output))




	########################################################## SVC

	svc = svm.SVC(**svmBestParams)
	svc = svc.fit(data_train, target_train)

	output = svc.predict(data_test)
	accuracySvm.append(metrics.accuracy_score(target_test, output))




 	########################################################## KNN
	
	knn = neighbors.KNeighborsClassifier(**knnBestParams)
	knn = knn.fit(data_train, target_train)

	output = knn.predict(data_test)
	accuracyKnn.append(metrics.accuracy_score(target_test, output))




	########################################################## LINEAR REGRESSION
	
	logReg = linear_model.LogisticRegression()
	logReg = logReg.fit(data_train, target_train)

	output = logReg.predict(data_test)
	accuracyLogReg.append(metrics.accuracy_score(target_test, output))




	########################################################## NAIVE BAYES

	naive = naive_bayes.BernoulliNB()
	naive = naive.fit(data_train, target_train)

	output = naive.predict(data_test)
	accuracyNB.append(metrics.accuracy_score(target_test, output))




	########################################################## MLP

	mlp = neural_network.MLPClassifier(**mlpBestParams)
	mlp.fit(data_train, target_train)

	output = mlp.predict(data_test)
	accuracyMLP.append(metrics.accuracy_score(target_test, output))

		########################################################## MultinomialNB

	multinomial = MultinomialNB()
	multinomial.fit(data_train, target_train)

	output = multinomial.predict(data_test)
	accuracyMultinomial.append(metrics.accuracy_score(target_test, output))

########################################################## AdaBoost

	adaBoost = AdaBoostClassifier()
	adaBoost.fit(data_train, target_train)

	output = adaBoost.predict(data_test)
	accuracyAdaBoost.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsRest

	oneVsRest = OneVsRestClassifier(LinearSVC(random_state = 0))
	oneVsRest.fit(data_train, target_train)

	output = oneVsRest.predict(data_test)
	accuracyOneVsRest.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsOne

	oneVsOne = OneVsOneClassifier(LinearSVC(random_state = 0))
	oneVsOne.fit(data_train, target_train)

	output = oneVsOne.predict(data_test)
	accuracyOneVsOne.append(metrics.accuracy_score(target_test, output))


########################################################## LogisticRegression

	logisticRegression = LogisticRegression()
	logisticRegression.fit(data_train, target_train)

	output = logisticRegression.predict(data_test)
	accuracyLR.append(metrics.accuracy_score(target_test, output))


########################################################## BaggingClassifier

	bagging = BaggingClassifier()
	bagging.fit(data_train, target_train)

	output = bagging.predict(data_test)
	accuracyBagging.append(metrics.accuracy_score(target_test, output))


########################################################## GradientBoostingClassifier

	gradientBoosting = GradientBoostingClassifier()
	gradientBoosting.fit(data_train, target_train)

	gradientBoosting = gradientBoosting.predict(data_test)
	accuracyGB.append(metrics.accuracy_score(target_test, output))


########################################################## RandomForestClassifier

	random = RandomForestClassifier()
	random.fit(data_train, target_train)

	output = random.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))



##### DT AVG
avg = np.mean(accuracyDT)
std = np.std(accuracyDT)
top_avg = avg
top_avg_name = "DT"
print("DT: " +str(accuracyDT))
print("DT Media: " +str(avg))
print("")

##### SVM AVG
avg = np.mean(accuracySvm)
std = np.std(accuracySvm)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SVM"

print("SVM: " +str(accuracySvm))
print("SVM Media: " +str(avg))
print("")

##### KNN AVG
avg = np.mean(accuracyKnn)
std = np.std(accuracyKnn)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "KNN"

print("KNN: " +str(accuracyKnn))
print("KNN Media: " +str(avg))
print("")

##### LOGREG AVG
avg = np.mean(accuracyLogReg)
std = np.std(accuracyLogReg)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LOGREG"

print("LOGREG: " +str(accuracyLogReg))
print("LOGREG Media: " +str(avg))
print("")

##### NB AVG
avg = np.mean(accuracyNB)
std = np.std(accuracyNB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "NB"

print("NB: " +str(accuracyNB))
print("NB Media: " +str(avg))
print("")

##### MLP AVG
avg = np.mean(accuracyMLP)
std = np.std(accuracyMLP)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "MLP"

print("MLP: " +str(accuracyMLP))
print("MLP Media: " +str(avg))
print("\n")

##### Multinomial Naive Bayes AVG
avg = np.mean(accuracyMultinomial)
std = np.std(accuracyMultinomial)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Multinomial Naive Bayes"

print("Multinomial Naive Bayes: " +str(accuracyMLP))
print("Multinomial Naive Bayes: " +str(avg))
print("\n")

##### AdaBoost AVG
avg = np.mean(accuracyAdaBoost)
std = np.std(accuracyAdaBoost)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost"

print("AdaBoost: " +str(accuracyAdaBoost))
print("AdaBoost: " +str(avg))
print("\n")

##### OneVsRest AVG
avg = np.mean(accuracyOneVsRest)
std = np.std(accuracyOneVsRest)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsRest"

print("OneVsRest: " +str(accuracyOneVsRest))
print("OneVsRest: " +str(avg))
print("\n")

##### OneVsOne AVG
avg = np.mean(accuracyOneVsOne)
std = np.std(accuracyOneVsOne)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsOne"

print("OneVsOne: " +str(accuracyOneVsOne))
print("OneVsOne: " +str(avg))
print("\n")

##### Logistic Regression AVG
avg = np.mean(accuracyLR)
std = np.std(accuracyLR)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LogisticRegression"

print("LogisticRegression: " +str(accuracyLR))
print("LogisticRegression: " +str(avg))
print("\n")

##### Bagging AVG
avg = np.mean(accuracyBagging)
std = np.std(accuracyBagging)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "BaggingClassifier"

print("BaggingClassifier: " +str(accuracyBagging))
print("BaggingClassifier: " +str(avg))
print("\n")

##### GradientBoosting AVG
avg = np.mean(accuracyGB)
std = np.std(accuracyGB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "GradientBoosting"

print("GradientBoosting: " +str(accuracyGB))
print("GradientBoosting: " +str(avg))
print("\n")

##### RandomForest AVG
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "RandomForest"

print("RandomForest: " +str(accuracyRF))
print("RandomForest: " +str(avg))
print("\n")

print("Top Avg is " + top_avg_name +str(": ") +str(top_avg))
