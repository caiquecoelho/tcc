# -*- coding: utf-8 -*-

from __future__ import print_function

from sklearn.datasets import fetch_20newsgroups
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import load_files
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from operator import itemgetter
from optparse import OptionParser
from time import time

import logging
import sys
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import itertools

import nltk
import nltk.stem

import numpy as np

stop_words = {"de","a","o","que","e","do","da", "das", "em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai",
"vou", "tão", "alguma", "interesse", "ter", "caso", "abaixo", "animais", "ainda", "outras", "etc.", "em", "piloto", "corrida", "a", "b", "c", 
"d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}

dictionary_stemmer = {}


def distribuicao_de_dados_no_dataset():
  print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

  total = 0
  total_classe_majoritaria = 0
  classe_majoritaria = categories[0]

  all_estimativas = []
  for t in dataset.target:
    all_estimativas.append(int(dataset.target_names[t]))
  
  for categorie in categories:
    for t in dataset.target:
      if(dataset.target_names[t] == categorie):
        total += 1 

    porcentagem = 100.0 * total/tam
    print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

    if(total > total_classe_majoritaria):
      classe_majoritaria = categorie
      total_classe_majoritaria = total

    total = 0

  total_de_elementos = len(dataset.data)
  taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

  print("\nClasse Majoritária é: " +str(classe_majoritaria))
  print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
  print(str(taxa_de_acerto_algoritmo_burro) + "%")
  all_estimativas = np.array(all_estimativas)
  desvio_dataset = all_estimativas.std()
  media_dataset = all_estimativas.mean()
  print("Média do dataset inteiro: " + str(media_dataset))
  print("\nDesvio padrão do dataset inteiro: " +str(desvio_dataset))
  print("\n")


def distribuicao_dos_dados_nos_clusters():
  print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")

  total = 0

  for cluster in range(0, n_clusters):
    for index in range(0, tam):
      if(cluster_labels[index] == cluster):
        total+=1

    porcentagem = 100.0 * total/tam
    print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

    total = 0

def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int",
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--no-minibatch",
              action="store_false", dest="minibatch", default=True,
              help="Use ordinary k-means algorithm (in batch mode).")
op.add_option("--no-idf",
              action="store_false", dest="use_idf", default=True,
              help="Disable Inverse Document Frequency feature weighting.")
op.add_option("--use-hashing",
              action="store_true", default=False,
              help="Use a hashing feature vectorizer")
op.add_option("--use-normalizer",
              action="store_true", default=False,
              help="Use a hashing feature vectorizer")
op.add_option("--use-stemmer-snowball",
              action="store_true", default=False,
              help="Stemming all words with stemmer Snowball")
op.add_option("--use-stemmer-rslps",
              action="store_true", default=False,
              help="Stemming all words with stemmer RSLPS")
op.add_option("--n-features", type=int, default=None,
              help="Maximum number of features (dimensions)"
                   " to extract from text.")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")
op.add_option("--print-distribuicao-clusters",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")
op.add_option("--terms-per-cluster",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")

print(__doc__)
op.print_help()

argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)


portuguese_stemmer = nltk.stem.RSLPStemmer()

if opts.use_stemmer_snowball:
  portuguese_stemmer = nltk.stem.SnowballStemmer('portuguese')


def stemmed_reverse(doc):
  analyzer = TfidfVectorizer().build_analyzer()

  for w in analyzer(doc):

    if w not in stop_words:

      word_stemmed = portuguese_stemmer.stem(w)

      existe = False
      if(word_stemmed in dictionary_stemmer):
        existe = True      

      if(not existe):
        dictionary_stemmer[word_stemmed] = [[w,1]]

      else:
        lists_word_stemmed_from_dictionary = dictionary_stemmer[word_stemmed]
        achou = False
        for i in range(len(lists_word_stemmed_from_dictionary)):
          list_word = lists_word_stemmed_from_dictionary[i]

          if(list_word[0] == w):
            list_word[1] = list_word[1] + 1
            achou = True
            lists_word_stemmed_from_dictionary[i] == list_word
            dictionary_stemmer[word_stemmed] = lists_word_stemmed_from_dictionary

        if(not achou):
          lists_word_stemmed_from_dictionary.append([w, 1])
          dictionary_stemmer[word_stemmed] = lists_word_stemmed_from_dictionary
  '''  
  if w not in stop_words:
    return (portuguese_stemmer.stem(w) for w in analyzer(doc))
  else:
    return 'XXX'
  '''
  return (portuguese_stemmer.stem(w) for w in analyzer(doc))


'''
class StemmedTfidfVectorizer(TfidfVectorizer):
  def build_analyzer(self):
    analyzer = super(TfidfVectorizer, self).build_analyzer()

    return lambda doc: stemmed_reverse(doc)
'''


class StemmedTfidfVectorizer(TfidfVectorizer):
  def build_analyzer(self):
    analyzer = super(TfidfVectorizer, self).build_analyzer()

    return lambda doc: ([portuguese_stemmer.stem(w) for w in analyzer(doc)])



# #############################################################################
# Load some categories from the training set
categories = ['1', '2', '3', '4', '6', '8']

#Database Final com tarefas de todos os tipos, min: 10 palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/10_palavras/tudo", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de API, min: 10 palavras
dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/10_palavras/api", 
 categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem_api, min: 10 palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/10_palavras/sem_api", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, min: 10 palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/10_palavras/ios", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#######################################################################################################

#Database Final com tarefas de todos os tipos, sem limite de palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
#  categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de API, sem limite de palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas//sem_limite_palavras/api", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem_api, sem limite de palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/sem_limite_palavras/sem_api", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, sem limite de palavras
#dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/sem_limite_palavras/ios", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#print(dataset.data)

list_words_stemmers = []
list_words_stemmer = []
list_words_to_stemmer = []

print("\n%d documents" % len(dataset.data))
print("%d categories" % len(dataset.target_names))
print()

labels = dataset.target
#true_k = np.unique(labels).shape[0]
tam = len(dataset.target)

#print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()
if opts.use_hashing:
    if opts.use_idf:
        # Perform an IDF normalization on the output of HashingVectorizer
        hasher = HashingVectorizer(n_features=10000,
                                   stop_words=stop_words, alternate_sign=False,
                                   norm=None, binary=False)
        vectorizer = make_pipeline(hasher, TfidfTransformer())
    else:
        vectorizer = HashingVectorizer(n_features=10000,
                                       stop_words=stop_words,
                                       alternate_sign=False, norm='l2',
                                       binary=False)
elif opts.use_stemmer_rslps:
    
    vectorizer = StemmedTfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
    

    
    vectorizer2 = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf,
                                 analyzer = stemmed_reverse)
    

    print("USE STEMER RSLPS")

    if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)
      vectorizer_normalized2 = make_pipeline(vectorizer2, normalizer)


elif opts.use_stemmer_snowball:
    
    vectorizer = StemmedTfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
    

    vectorizer2 = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf,
                                 analyzer = stemmed_reverse)

    print("USE STEMER Snowball")

    if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)
      vectorizer_normalized2 = make_pipeline(vectorizer2, normalizer)


else:
   
   vectorizer = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)

   if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)

if opts.use_normalizer:

  X = vectorizer_normalized.fit_transform(dataset.data)
  X2 = vectorizer_normalized2.fit_transform(dataset.data)

else:
  X = vectorizer.fit_transform(dataset.data)
  X2 = vectorizer2.fit_transform(dataset.data)

'''
print("\n\nX original: ")
print(X.shape[0])
print(X)
'''

#print(dictionary_stemmer)
#print(len(dictionary_stemmer))

#print("done in %fs" % (time() - t0))


distribuicao_de_dados_no_dataset()

print("n_samples: %d, n_features: %d" % X.shape)
print()

if opts.n_components:
    print("\nPerforming dimensionality reduction using LSA")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)

    #print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    #print("Explained variance of the SVD step: {}%".format(
    #    int(explained_variance * 100)))

else:
  X = X.toarray()

'''
print("\n\nX after LSA: ")
print(len(X))
print(X)

print()
'''


# #############################################################################
# Do the actual clustering

#range_n_clusters = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 25, 30]
range_n_clusters = [10, 20, 30]
#true_k = np.unique(labels).shape[0]
for n_clusters in range_n_clusters:
  # Create a subplot with 1 row and 2 columns
    #Para plotar o grafico de visualização dos dados nos cluters
    #fig, (ax1, ax2) = plt.subplots(1, 2)
    #Para pltar apenas o score da silhoette em cada cluster
    fig, (ax1) = plt.subplots(1)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value
    # seed of 10 for reproducibility.

    type = ''
    if opts.minibatch:
      clusterer = MiniBatchKMeans(n_clusters=n_clusters, init='k-means++', n_init=10, max_iter=300,
                           init_size=1000, batch_size=1000, verbose=opts.verbose, random_state =10)
      type = 'MiniBatchKmeans'
    else:
      clusterer = KMeans(n_clusters=n_clusters, init='k-means++', random_state =10, 
                  verbose=opts.verbose)
      type = 'Kmeans'

    #print("Clustering sparse data with %s" % km)
    t0 = time()
    cluster_labels = clusterer.fit_predict(X)

    estimativas_dos_clusters = list()
    sum_estimativas_dos_clusters = list()

    if(n_clusters == 10):
      for i in range(0, n_clusters):
        estimativas_no_cluster = []
        for j in range(len(cluster_labels)):
          if(cluster_labels[j] == i):
            estimativas_no_cluster.append(int(dataset.target_names[dataset.target[j]]))

        estimativas_no_cluster = np.array(estimativas_no_cluster)
        soma = 0
        for est in estimativas_no_cluster:
          soma += est

        estimativas_dos_clusters.append(estimativas_no_cluster)
        sum_estimativas_dos_clusters.append(soma)
        print("\nCluster " +str(i))
        desvio = estimativas_no_cluster.std()
        media = estimativas_no_cluster.mean()
        #print("estimativas no Cluster: ")
        #print(estimativas_no_cluster)
        print("Média das tarefas no Cluster " +str(i) + " é de: " +str(media))
        print("Desvio Padrão do Cluster " +str(i) + " é de: " +str(desvio))
        
    #print("done in %0.3fs" % (time() - t0))
    #print()

    #print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, km.labels_))
    #print("Completeness: %0.3f" % metrics.completeness_score(labels, km.labels_))
    #print("V-measure: %0.3f" % metrics.v_measure_score(labels, km.labels_))
    #print("Adjusted Rand-Index: %.3f"
    #      % metrics.adjusted_rand_score(labels, km.labels_))
    #print("Silhouette Coefficient: %0.3f"
    #      % metrics.silhouette_score(X, km.labels_, sample_size=1000))

    #print()

    if not opts.use_hashing and opts.terms_per_cluster:

      if(n_clusters == 10):

        print("\nTop terms per cluster with " +str(n_clusters) + " clusters:")

        if opts.n_components:
            original_space_centroids = svd.inverse_transform(clusterer.cluster_centers_)
            order_centroids = original_space_centroids.argsort()[:, ::-1]
        else:
            order_centroids = clusterer.cluster_centers_.argsort()[:, ::-1]

        terms = vectorizer.get_feature_names()


        dicionario_top_terms = {}
        for i in range(n_clusters):
            print("Cluster %d:" % i, end='')
            for ind in order_centroids[i, :10]:
              
              if opts.use_stemmer_rslps or opts.use_stemmer_snowball:

                lists_word_stemmed_from_dictionary = dictionary_stemmer[terms[ind]]

                word_maior = ''
                maior = 0
                
                for list_word in lists_word_stemmed_from_dictionary:
                  word = list_word[0]
                  vezes = list_word[1]
                  if(vezes > maior):
                    word_maior = word
                    maior = vezes


                existe0 = False
                if(word_maior in dicionario_top_terms):
                  existe0 = True
                  lista_word = dicionario_top_terms[word_maior]
                  if(i not in lista_word):
                    lista_word.append(i)  

                if(not existe0):
                  dicionario_top_terms[word_maior] = [i]

                print(' %s' % word_maior, end='')
              else:
                print(' %s' % terms[ind], end='')
            print()
        print()


        #Print media por termos
        
        listaPalavrasMedia = list()
        print("dicionario_top_terms")
        print(dicionario_top_terms)

        estimativas_do_termo = []
        for item in dicionario_top_terms:
          lista = dicionario_top_terms[item]
          for j in lista:
            estimates = estimativas_dos_clusters[j]
            for estimate in estimates:
              estimativas_do_termo.append(estimate)

          #print(lista)
          #print(estimativas_do_termo)
          item_media = [item, float("{0:.2f}".format(np.mean(estimativas_do_termo)))]
          listaPalavrasMedia.append(item_media)
          print()
          print(item)
          print("Média" + ": " + str(np.mean(estimativas_do_termo)))
          print("Desvio Padrão" + ": " + str(np.std(estimativas_do_termo)))
          estimativas_do_termo = []
        
        listaNew = sorted(listaPalavrasMedia, key=itemgetter(1))


        #for item in listaNew:
        #  print(item)

        '''
        aula = estimativas_dos_clusters[0].tolist()
        print("Aula: ")
        print("Media: {0:.2f}".format(np.mean(aula)))
        print("Desvio Padrão: {0:.2f}".format(np.std(aula)))
        print()
 
        soma = sum(estimativas_dos_clusters[1]) + sum(estimativas_dos_clusters[7])
        count = len(estimativas_dos_clusters[1]) + len(estimativas_dos_clusters[7])
        pagamento = estimativas_dos_clusters[1].tolist() + estimativas_dos_clusters[7].tolist()
        print("Pagamento: ")
        print("Media: {0:.2f}".format(np.mean(pagamento)))
        print("Desvio Padrão: {0:.2f}".format(np.std(pagamento)))
        print()

        soma = sum(estimativas_dos_clusters[2]) + sum(estimativas_dos_clusters[4]) + sum(estimativas_dos_clusters[14])
        count = len(estimativas_dos_clusters[2]) + len(estimativas_dos_clusters[4]) + len(estimativas_dos_clusters[14])
        mensagens = estimativas_dos_clusters[2].tolist() + estimativas_dos_clusters[4].tolist() + estimativas_dos_clusters[14].tolist()
        print("Mensagens: ")
        print("Media: {0:.2f}".format(np.mean(mensagens)))
        print("Desvio Padrão: {0:.2f}".format(np.std(mensagens)))
        print()

        soma = sum(estimativas_dos_clusters[3]) + sum(estimativas_dos_clusters[10]) + sum(estimativas_dos_clusters[21]) + sum(estimativas_dos_clusters[28])
        count = len(estimativas_dos_clusters[3]) + len(estimativas_dos_clusters[10]) + len(estimativas_dos_clusters[21]) + len(estimativas_dos_clusters[28])
        login = estimativas_dos_clusters[3].tolist() + estimativas_dos_clusters[10].tolist() + estimativas_dos_clusters[21].tolist() + estimativas_dos_clusters[28].tolist()
        print("Login: ")
        print("Media: {0:.2f}".format(np.mean(login)))
        print("Desvio Padrão: {0:.2f}".format(np.std(login)))
        print()

        soma = sum(estimativas_dos_clusters[5]) + sum(estimativas_dos_clusters[25])
        count = len(estimativas_dos_clusters[5]) + len(estimativas_dos_clusters[25])
        listagem = estimativas_dos_clusters[5].tolist() + estimativas_dos_clusters[25].tolist()
        print("Listagem: ")
        print("Media: {0:.2f}".format(np.mean(listagem)))
        print("Desvio Padrão: {0:.2f}".format(np.std(listagem)))
        print()

        soma = sum(estimativas_dos_clusters[6]) + sum(estimativas_dos_clusters[15]) + sum(estimativas_dos_clusters[22])
        count = len(estimativas_dos_clusters[6]) + len(estimativas_dos_clusters[15]) + len(estimativas_dos_clusters[22])
        formularios = estimativas_dos_clusters[6].tolist() + estimativas_dos_clusters[15].tolist() + estimativas_dos_clusters[22].tolist()
        print("Formulários: ")
        print("Media: {0:.2f}".format(np.mean(formularios)))
        print("Desvio Padrão: {0:.2f}".format(np.std(formularios)))
        print()

        soma = sum(estimativas_dos_clusters[7]) + sum(estimativas_dos_clusters[8]) + sum(estimativas_dos_clusters[13]) + sum(estimativas_dos_clusters[18]) + sum(estimativas_dos_clusters[26])
        count = len(estimativas_dos_clusters[7]) + len(estimativas_dos_clusters[8]) + len(estimativas_dos_clusters[13]) + len(estimativas_dos_clusters[18]) + len(estimativas_dos_clusters[26])
        tratamento = estimativas_dos_clusters[7].tolist() + estimativas_dos_clusters[8].tolist() + estimativas_dos_clusters[13].tolist() + estimativas_dos_clusters[18].tolist() + estimativas_dos_clusters[26].tolist()
        print("Tratamento: ")
        print("Media: {0:.2f}".format(np.mean(tratamento)))
        print("Desvio Padrão: {0:.2f}".format(np.std(tratamento)))
        print()

        textos = estimativas_dos_clusters[16].tolist()
        print("Textos: ")
        print("Media: {0:.2f}".format(np.mean(textos)))
        print("Desvio Padrão: {0:.2f}".format(np.std(textos)))
        print()

        soma = sum(estimativas_dos_clusters[9]) + sum(estimativas_dos_clusters[27])
        count = len(estimativas_dos_clusters[9]) + len(estimativas_dos_clusters[27])
        busca = estimativas_dos_clusters[9].tolist() + estimativas_dos_clusters[27].tolist()
        print("Busca: ")
        print("Media: {0:.2f}".format(np.mean(busca)))
        print("Desvio Padrão: {0:.2f}".format(np.std(busca)))
        print()

        pedido = estimativas_dos_clusters[11].tolist()
        print("Pedido: ")
        print("Media: {0:.2f}".format(np.mean(pedido)))
        print("Desvio Padrão: {0:.2f}".format(np.std(pedido)))
        print()

        fotos = estimativas_dos_clusters[12].tolist()
        print("Fotos: ")
        print("Media: {0:.2f}".format(np.mean(fotos)))
        print("Desvio Padrão: {0:.2f}".format(np.std(fotos)))
        print()

        soma = sum(estimativas_dos_clusters[20]) + sum(estimativas_dos_clusters[23]) + sum(estimativas_dos_clusters[24]) + sum(estimativas_dos_clusters[25]) + sum(estimativas_dos_clusters[29])
        count = len(estimativas_dos_clusters[20]) + len(estimativas_dos_clusters[23]) + len(estimativas_dos_clusters[24]) + len(estimativas_dos_clusters[25]) + len(estimativas_dos_clusters[29])
        telas = estimativas_dos_clusters[20].tolist() + estimativas_dos_clusters[23].tolist() + estimativas_dos_clusters[24].tolist() + estimativas_dos_clusters[25].tolist() + estimativas_dos_clusters[29].tolist()
        print("Telas: ")
        print("Media: {0:.2f}".format(np.mean(telas)))
        print("Desvio Padrão: {0:.2f}".format(np.std(telas)))
        print()

        agendamento = estimativas_dos_clusters[19].tolist()
        print("Agendamento: ")
        print("Media: {0:.2f}".format(np.mean(agendamento)))
        print("Desvio Padrão: {0:.2f}".format(np.std(agendamento)))
        print()
        '''

        '''
        formularios = estimativas_dos_clusters[0].tolist() + estimativas_dos_clusters[4].tolist()
        print("Formulários: ")
        print("Media: {0:.2f}".format(np.mean(formularios)))
        print("Desvio Padrão: {0:.2f}".format(np.std(formularios)))
        print()

        listagem = estimativas_dos_clusters[1].tolist() + estimativas_dos_clusters[7].tolist()
        print("Listagem: ")
        print("Media: {0:.2f}".format(np.mean(listagem)))
        print("Desvio Padrão: {0:.2f}".format(np.std(listagem)))
        print()

        login = estimativas_dos_clusters[2].tolist()
        print("Login: ")
        print("Media: {0:.2f}".format(np.mean(login)))
        print("Desvio Padrão: {0:.2f}".format(np.std(login)))
        print()

        mensagens = estimativas_dos_clusters[3].tolist() + estimativas_dos_clusters[9].tolist()
        print("Mensagens: ")
        print("Media: {0:.2f}".format(np.mean(mensagens)))
        print("Desvio Padrão: {0:.2f}".format(np.std(mensagens)))
        print()

        tratamento = estimativas_dos_clusters[5].tolist() + estimativas_dos_clusters[8].tolist()
        print("Tratamento: ")
        print("Media: {0:.2f}".format(np.mean(tratamento)))
        print("Desvio Padrão: {0:.2f}".format(np.std(tratamento)))
        print()

        pagamento= estimativas_dos_clusters[6].tolist()
        print("Pagamento: ")
        print("Media: {0:.2f}".format(np.mean(pagamento)))
        print("Desvio Padrão: {0:.2f}".format(np.std(pagamento)))
        print()
        '''

    print()

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
     # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
            "The average silhouette_score is :", silhouette_avg)

    
    if(opts.print_distribuicao_clusters and n_clusters == 10):
      distribuicao_dos_dados_nos_clusters()
      print("\n")

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    
    #Salva o output da clusterização com 10 clusters
    '''
    if(n_clusters == 20):
      for index in range(0, len(dataset.target)):
        #print(cluster_labels)
        nome_arquivo = "cluster_" +str(cluster_labels[index]) +"_estimativa_"+str(dataset.target_names[dataset.target[index]]) +"_tarefa_"+str(index)
        file = open(nome_arquivo, "w")
        string = dataset.data[index]
        string_decode = string.decode("utf-8")
        dado = str(string_decode)
        file.write(dado) 
        file.close()
    '''

    y_lower = 10
    for i in range(n_clusters):
      # Aggregate the silhouette scores for samples belonging to
      # cluster i, and sort them
      ith_cluster_silhouette_values = \
      sample_silhouette_values[cluster_labels == i]

      ith_cluster_silhouette_values.sort()

      size_cluster_i = ith_cluster_silhouette_values.shape[0]
      y_upper = y_lower + size_cluster_i

      color = cm.spectral(float(i) / n_clusters)
      ax1.fill_betweenx(np.arange(y_lower, y_upper),
                        0, ith_cluster_silhouette_values,
                        facecolor=color, edgecolor=color, alpha=0.7)

      # Label the silhouette plots with their cluster numbers at the middle
      ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

      # Compute the new y_lower for next plot
      y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    #Para plotar o grafico de visualização dos dados nos cluters
    '''
    colors = cm.spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                  c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                  c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                      s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")
    '''

    plt.suptitle(("Silhouette analysis for " +str(type) +" clustering on sample data "
                    "with n_clusters = %d" % n_clusters),
                   fontsize=14, fontweight='bold')

    #plt.show()
    name_image = "Silhouette_for_" +str(n_clusters) + "_clusters.png" 
    #plt.savefig(name_image)