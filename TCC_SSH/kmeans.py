#!/usr/bin/env python
#!-*- coding: utf8 -*-
import time
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import adjusted_rand_score
from sklearn.cluster import KMeans
import sys


def distribuicao_dos_dados_no_dataset():
	print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

	total = 0
	total_classe_majoritaria = 0
	classe_majoritaria = categories[0]
	
	for categorie in categories:
		for t in text.target:
			if(text.target_names[t] == categorie):
				total += 1 

		porcentagem = 100.0 * total/tam
		print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + "tarefas, em porcentagem temos: " +str(int(porcentagem))  + "%")

		if(total > total_classe_majoritaria):
			classe_majoritaria = categorie
			total_classe_majoritaria = total

		total = 0

	total_de_elementos = len(target_train_total)
	taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

	print("\nClasse Majoritária é: " +str(classe_majoritaria))
	print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
	print(str(taxa_de_acerto_algoritmo_burro) + "%")
	print("\n\n")


def distribuicao_dos_dados_nos_clusters():
	print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")

	total = 0

	for cluster in range(0, n_clusters):
		for index in range(0, tam):
			if(y[index] == cluster):
				total+=1

		porcentagem = 100.0 * total/tam
		print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: " +str(int(porcentagem))  + "%")

		total = 0

def salvar_output_clusters():
	file = open(nome_arquivo, "w")
	dado = str(text.data[index])
	file.write(dado) 
	file.close() 


########################################################################################################

if len (sys.argv)!=2:
	print("Use:",sys.argv[0],"n_clusters")
	exit()


stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai"}

#Salvando output em um arquivo txt
#orig_stdout = sys.stdout
#f = open('20_clusters_semngram.txt', 'w')
#sys.stdout = f

n_clusters = int(sys.argv[1])

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 15 palavras
#text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/Estimativas/database_3/6_categorias/15", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#Database Final com tarefas de todos os tipos, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/tudo", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem_api, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/tudo", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/ios", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#######################################################################################################

#Database Final com tarefas de todos os tipos, sem limite de palavras
text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem limite de palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, sem limite de palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/ios", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#count_vect = CountVectorizer(stop_words=stop_words)
#print(text.data)

#vectorizer = TfidfVectorizer(stop_words= stop_words, ngram_range= (1,2))
vectorizer = TfidfVectorizer(stop_words= stop_words)
X = vectorizer.fit_transform(text.data)

'''
X_train_counts = count_vect.fit_transform(text.data)

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)


data_train_total = X_train_tfidf
target_train_total = text.target
'''
#distribuicao_de_dados_no_dataset()

tam = len(text.data)

#true_k = 2
model = KMeans(n_clusters = n_clusters, init = 'k-means++', max_iter= 100, n_init = 1)
model.fit(X)
y = model.fit_predict(X)
#model.fit(data_train_total)

'''
print("\n\n")
print("Cluster Result with only fit method:")
print(model.labels_)
print("\n")
'''

print("\n\n")
print("Cluster Result with fit_predict method:")
print(y)

distribuicao_dos_dados_nos_clusters()

'''
tam = len(model.labels_)
print(tam)
indice = 0
diferente = 0
for i in model.labels_:
	if(i != cluster_labels[indice]):
		diferente += 1
	indice+=1

print(diferente)
'''


'''
print("Top terms per cluster:")
order_centroids = model.cluster_centers_.argsort()[:, ::-1]
terms = vectorizer.get_feature_names()
for i in range(n_clusters):
    print ("Cluster %d:" % i,)
    for ind in order_centroids[i, :10]:
        print(' %s' % terms[ind])
    print("")
'''


'''
for index in range(0, tam):
		estimativa_predict = model.labels_[index]
		nome_arquivo = "cluster_" +str(estimativa_predict) +"_tarefa_"+str(text.target[index])
		salvar_output_clusters()
'''