#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
import time
from sklearn import model_selection
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import OneVsOneClassifier

t0 = time.time()

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai"}


n_features = 1000 #construa um vocabulario que considere apenas as principais max_features ordenadas pela frequencia do termo em todo o corpus

#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database 3, 6 categorias, min: 15 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/15", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#To Array nos RIDGES??????????????????????????????????????????????????????????????????ww

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)

#Colocar ONEvsONE e ONEvsRest

accuracyRidge = list()
accuracyLinearSVC = list()
accuracySVC = list()
accuracyET = list()
accuracyMLP = list()
accuracyBG = list()
accuracyRF = list()
accuracyPerceptron = list()
accuracyOnevsOneRidge = list()
accuracyOnevsOneLinearSVC = list()
accuracyOnevsOneSVC = list()
accuracyOnevsOneET = list()
accuracyOneVSRestRidge = list()
accuracyOneVSRestLinearSVC = list()
accuracyOnevsOneRF = list()
accuracyOnevsOneLR = list()
accuracyOnevsOneNB = list()
accuracyOnevsOneKNN = list()
accuracyOnevsOneMLP = list()
accuracyOnevsOneBG = list()
accuracyOnevsOnePerceptron = list()


####### Parameters tuning



#RidgeClassifier
#'solver' : ('auto', 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga')
# SVD solver does not support sparse inputs currently
# Solver saga not understood
params = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'normalize': (False, True),
	'fit_intercept': (False, True),
	'tol': (1e-1, 1e-2, 1e-3, 1e-4, 1e-5),
	'solver' : ('auto', 'cholesky', 'lsqr', 'sparse_cg', 'sag'),
	'class_weight': ('balanced', None),
	'random_state': (None, 0, 42),
	'max_iter=None': (None, 10, 100, 200, 500, 1000, 2000)
}

ridge = RidgeClassifier()

ridge_grid = GridSearchCV(ridge, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

ridge_grid.fit(data_train_total.toarray(), target_train_total)
ridgeBestParams = ridge_grid.best_params_

print("Ridge Best Params: ")
print(ridgeBestParams)
print("")

#SVC
params = {
	'tol': (1e-2, 1e-3),
	'kernel': ['linear', 'rbf', 'poly'], 
	'C': [0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10],
	'gamma': ['auto', 1, 0.1, 0.01, 0.001, 0.0001],
	'decision_function_shape': ('ovo', 'ovr'),
	'random_state': (None, 0, 42)
}

svc = SVC()

svc_grid = GridSearchCV(svc, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

svc_grid.fit(data_train_total, target_train_total)
svcBestParams = svc_grid.best_params_

print("SVC Best Params: ")
print(svcBestParams)
print("")

#LinearSVC
# loss': ('hinge', 'squared_hinge'),
# dual : (False, True)
# Unsupported set of arguments: The combination of penalty='l1' and loss='squared_hinge' are not supported when dual=True
params = {
	'loss': ('squared_hinge', 'hinge'),
	'fit_intercept': (False, True),
	'tol': (1e-1, 1e-2, 1e-3, 1e-4, 1e-5),
	'C': [0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10],
	'class_weight': ('balanced', None),
	'max_iter': 'max_iter': (100, 200, 500, 800, 1000, 1500,  2000),
	'multi_class': ('ovr', 'crammer_singer')
}

linearSVC = LinearSVC(class_weight='auto')

linearSVC_grid = GridSearchCV(linearSVC, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

linearSVC_grid.fit(data_train_total, target_train_total)
linearSVCBestParams = linearSVC_grid.best_params_

print("LinearSVC Best Params: ")
print(linearSVCBestParams)
print("")

#ExtraTress sem base_estimator
params = {
	'n_estimators': (16, 32)
}

extraTress = ExtraTreesClassifier()

extraTrees_grid = GridSearchCV(extraTress, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

extraTrees_grid.fit(data_train_total, target_train_total)
extraTressBestParams = extraTrees_grid.best_params_

print("ExtraTress Best Params: ")
print(extraTreesBestParams)
print("")



for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	



########################################################## Ridge

	ridge = RidgeClassifier(**ridgeBestParams)

	ridge.fit(data_train.toarray(), target_train)

	output = ridge.predict(data_test)
	accuracyRidge.append(metrics.accuracy_score(target_test, output))

########################################################## SVC
	
	svc_clf = SVC(**svcBestParams)

	svc_clf.fit(data_train, target_train)

	output = svc_clf.predict(data_test.toarray())
	accuracySVC.append(metrics.accuracy_score(target_test, output))

########################################################## Linear SVC
	
	linearSvc_clf = LinearSVC(**linearSVCBestParams)

	linearSvc_clf.fit(data_train, target_train)

	output = linearSvc_clf.predict(data_test.toarray())
	accuracyLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## Extra Tress
	
	et_clf = ExtraTreesClassifier()

	et_clf.fit(data_train, target_train)

	output = et_clf.predict(data_test.toarray())
	accuracyET.append(metrics.accuracy_score(target_test, output))


########################################################## One VS One - Ridge

	oneVsOneRidge = OneVsOneClassifier(RidgeClassifier(**ridgeBestParams))

	oneVsOneRidge.fit(data_train.toarray(), target_train)

	output = oneVsOneRidge.predict(data_test)
	accuracyOnevsOneRidge.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - SVC
	
	oneVsOneSVC = OneVsOneClassifier(SVC(**svcBestParams))

	oneVsOneSVC.fit(data_train, target_train)

	output = oneVsOneSVC.predict(data_test.toarray())
	accuracyOnevsOneSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Linear SVC
	
	oneVsOneLinearSVC = OneVsOneClassifier(LinearSVC(**linearSVCBestParams))

	oneVsOneLinearSVC.fit(data_train, target_train)

	output = oneVsOneLinearSVC.predict(data_test.toarray())
	accuracyOnevsOneLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Extra Tress
	
	oneVsOneET = OneVsOneClassifier(ExtraTreesClassifier(n_estimators = 500))

	oneVsOneET.fit(data_train, target_train)

	output = oneVsOneET.predict(data_test.toarray())
	accuracyOnevsOneET.append(metrics.accuracy_score(target_test, output))

########################################################## One VS Rest - Ridge

	oneVsRestRidge = OneVsRestClassifier(RidgeClassifier(**ridgeBestParams))

	oneVsRestRidge.fit(data_train.toarray(), target_train)

	output = oneVsRestRidge.predict(data_test)
	accuracyOneVSRestRidge.append(metrics.accuracy_score(target_test, output))


########################################################## One VS Rest - LinearSVC

	oneVsRestLinearSVC = OneVsRestClassifier(LinearSVC(**linearSVCBestParams))

	oneVsRestLinearSVC.fit(data_train, target_train)

	output = oneVsRestLinearSVC.predict(data_test)
	accuracyOneVSRestLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Extra Tress
	
	oneVsOneET = OneVsOneClassifier(RandomForest(n_estimators = 500))

	oneVsOneET.fit(data_train, target_train)

	output = oneVsOneET.predict(data_test.toarray())
	accuracyOnevsOneET.append(metrics.accuracy_score(target_test, output))



##### Ridge AVG
avg = np.mean(accuracyRidge)
top_avg = avg
std = np.std(accuracyRidge)
top_avg_name = "Ridge"

print("Ridge: " +str(accuracyRidge))
print("Ridge Media: " +str(avg))
print("\n")

##### LinearSVC AVG
avg = np.mean(accuracyLinearSVC)
std = np.std(accuracyLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LinearSVC"

print("LinearSVC: " +str(accuracyLinearSVC))
print("LinearSVC Media: " +str(avg))
print("\n")

##### SVC AVG
avg = np.mean(accuracySVC)
std = np.std(accuracySVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SVC"

print("SVC: " +str(accuracySVC))
print("SVC Media: " +str(avg))
print("\n")

##### Extra Trees AVG
avg = np.mean(accuracyET)
std = np.std(accuracyET)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Extra Trees"

print("Extra Trees: " +str(accuracyET))
print("Extra Trees Media: " +str(avg))
print("\n")


##### One vs One - Ridge AVG
avg = np.mean(accuracyOnevsOneRidge)
std = np.std(accuracyOnevsOneRidge)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - Ridge AVG"

print("One vs One - Ridge AVG: " +str(accuracyOnevsOneRidge))
print("One vs One - Ridge AVG Media: " +str(avg))
print("\n")

##### One vs One - LinearSVC AVG
avg = np.mean(accuracyOnevsOneLinearSVC)
std = np.std(accuracyOnevsOneLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - LinearSVC AVG"

print("One vs One - LinearSVC AVG: " +str(accuracyOnevsOneLinearSVC))
print("One vs One - LinearSVC AVG Media: " +str(avg))
print("\n")

##### One vs One - SVC AVG
avg = np.mean(accuracyOnevsOneSVC)
std = np.std(accuracyOnevsOneSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - SVC AVG"

print("One vs One - SVC AVG: " +str(accuracyOnevsOneSVC))
print("One vs One - SVC AVG Media: " +str(avg))
print("\n")

##### One vs One - Extra Trees AVG
avg = np.mean(accuracyOnevsOneET)
std = np.std(accuracyOnevsOneET)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS One- Extra Trees"

print("One VS One- Extra Trees: " +str(accuracyOnevsOneET))
print("One VS One- Extra Trees Media: " +str(avg))
print("\n")

##### One vs Rest- Ridge AVG
avg = np.mean(accuracyOneVSRestRidge)
std = np.std(accuracyOneVSRestRidge)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS Rest - Ridge"

print("One VS Rest - Ridge: " +str(accuracyOneVSRestRidge))
print("One VS Rest - Ridge: " +str(avg))
print("\n")

##### One vs Rest - Linear SVC AVG
avg = np.mean(accuracyOneVSRestLinearSVC)
std = np.std(accuracyOneVSRestLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs Rest - Linear SVC"

print("One vs Rest - Linear SVC: " +str(accuracyOneVSRestLinearSVC))
print("One vs Rest - Linear SVC: " +str(avg))
print("\n")


print("\n")

print("TOP AVG IS " + top_avg_name +str(": ") +str(top_avg))

print("\n\n")

elapsed_time_seconds = time.time() - t0

elapsed_time_minutes = int(elapsed_time_seconds) / 60

print("Tempo total executando: %.2fm" % (elapsed_time_minutes))