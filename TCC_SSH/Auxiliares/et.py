#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/Estimativas/database_3/6_categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target

kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyET = list()


#ExtraTress sem base_estimator
params = {
	'n_estimators': (20, 2000),
	'max_features': ('auto', 'sqrt', 'log2', None),
	'max_depth': (None, 2, 5),
	'min_samples_split': (2, 3, 4)
}

extraTress = ExtraTreesClassifier()

extraTrees_grid = GridSearchCV(extraTress, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

extraTrees_grid.fit(data_train_total, target_train_total)
extraTreesBestParams = extraTrees_grid.best_params_

print("ExtraTress Best Params: ")
print(extraTreesBestParams)
print("")
	

for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]


########################################################## Extra Tress
	
	et_clf = ExtraTreesClassifier(**extraTreesBestParams)

	et_clf.fit(data_train, target_train)

	output = et_clf.predict(data_test.toarray())
	accuracyET.append(metrics.accuracy_score(target_test, output))


##### ExtraTress AVG
avg = np.mean(accuracyET)
std = np.std(accuracyET)

print("ExtraTress: " +str(accuracyET))
print("ExtraTress Media: " +str(avg))
print("\n")
