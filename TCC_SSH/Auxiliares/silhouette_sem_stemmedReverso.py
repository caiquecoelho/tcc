# -*- coding: utf-8 -*-

from __future__ import print_function

from sklearn.datasets import fetch_20newsgroups
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import load_files

from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics import silhouette_samples, silhouette_score
import logging
from optparse import OptionParser
import sys
from time import time
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import numpy as np

import nltk
import nltk.stem

import numpy as np

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai",
"vou", "tão", "alguma", "interesse", "ter", "caso", "abaixo", "animais", "ainda", "outras", "etc.", "em", "piloto", "corrida", "a", "b", "c", 
"d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}


def distribuicao_de_dados_no_dataset():
  print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

  total = 0
  total_classe_majoritaria = 0
  classe_majoritaria = categories[0]
  
  for categorie in categories:
    for t in dataset.target:
      if(dataset.target_names[t] == categorie):
        total += 1 

    porcentagem = 100.0 * total/tam
    print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

    if(total > total_classe_majoritaria):
      classe_majoritaria = categorie
      total_classe_majoritaria = total

    total = 0

  total_de_elementos = len(dataset.data)
  taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

  print("\nClasse Majoritária é: " +str(classe_majoritaria))
  print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
  print(str(taxa_de_acerto_algoritmo_burro) + "%")
  print("\n\n")


def distribuicao_dos_dados_nos_clusters():
  print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")

  total = 0

  for cluster in range(0, n_clusters):
    for index in range(0, tam):
      if(cluster_labels[index] == cluster):
        total+=1

    porcentagem = 100.0 * total/tam
    print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

    total = 0

def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int",
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--no-minibatch",
              action="store_false", dest="minibatch", default=True,
              help="Use ordinary k-means algorithm (in batch mode).")
op.add_option("--no-idf",
              action="store_false", dest="use_idf", default=True,
              help="Disable Inverse Document Frequency feature weighting.")
op.add_option("--use-hashing",
              action="store_true", default=False,
              help="Use a hashing feature vectorizer")
op.add_option("--use-normalizer",
              action="store_true", default=False,
              help="Use a hashing feature vectorizer")
op.add_option("--use-stemmer-snowball",
              action="store_true", default=False,
              help="Stemming all words with stemmer Snowball")
op.add_option("--use-stemmer-rslps",
              action="store_true", default=False,
              help="Stemming all words with stemmer RSLPS")
op.add_option("--n-features", type=int, default=None,
              help="Maximum number of features (dimensions)"
                   " to extract from text.")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")
op.add_option("--print-distribuicao-clusters",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")
op.add_option("--terms-per-cluster",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")

print(__doc__)
op.print_help()

argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)


portuguese_stemmer = nltk.stem.RSLPStemmer()

if opts.use_stemmer_snowball:
  portuguese_stemmer = nltk.stem.SnowballStemmer('portuguese')


def stemmed_reverse(doc):
  analyzer = TfidfVectorizer().build_analyzer()

  for w in analyzer(doc):
    word_stemmed = portuguese_stemmer.stem(w)

    #if word_stemmed not in list_words_stemmers:

    if(len(list_words_stemmer) == 0):
      #print("MAOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOEEEEEEEEEEEEEEEEEEEEEEEEEEEE")
      list_words_stemmers.append(word_stemmed)
      list_words_to_stemmer.append([[w, 1]])

    else:
      #print("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
      for i in range(len(list_words_stemmer)):
        if(word_stemmed == list_words_stemmer[i]):
          break

        elif(i == len(list_words_stemmer) -1):
          list_words_stemmers.append(word_stemmed)
          list_words_to_stemmer.append([[w, 1]])

      else:
        k = 0
        for i in range(len(list_words_stemmer)):
          print(word_stemmed)
          print(list_word_stemmers[i])
          if(word_stemmed == list_words_stemmers[i]):
            print(i)
            k = i
            break

        if(len(list_words_to_stemmer) > 0):
          list_word_stemmed = list_words_to_stemmer[k]
          tam_list = len(list_word_stemmed)
        
          for list in list_word_stemmed:
            if list[0] == w:
              list[1] == list[1] + 1

            elif j == tam_list - 1:
              list_word_stemmed.append([w, 1])

    
    return (portuguese_stemmer.stem(w) for w in analyzer(doc))





class StemmedTfidfVectorizer(TfidfVectorizer):
  def build_analyzer(self):
    analyzer = super(TfidfVectorizer, self).build_analyzer()
    '''
    for w in analyzer(dataset.data):
      word_stemmed = portuguese_stemmer.stem(w)

      if word_stemmed not in list_words_stemmer:
        list_words_stemmers.append(word_stemmed)
        list_words_to_stemmer.append([[w, 1]])

      else:
        i = 0
        for word in list_words_stemmers:
          if(word_stemmed != word):
            i += 1
          else:
            break

        list_word_stemmed = list_words_to_stemmer[i]
        for list in list_word_stemmed:
          if list[0] == w:
            list[1] == list[1] + 1

          else:
            list_word_stemmed.append([w, 1])

        i = 0

      dicionario_stemmer[word_stemmed] = []
      '''
    return lambda doc: ([portuguese_stemmer.stem(w) for w in analyzer(doc)])



# #############################################################################
# Load some categories from the training set
categories = ['1', '2', '3', '4', '6', '8']

#Database Final com tarefas de todos os tipos, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/tudo", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem_api, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/sem_api", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/ios", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#######################################################################################################

#Database Final com tarefas de todos os tipos, sem limite de palavras
dataset = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
  categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem_api, min: 10 palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/sem_api", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, sem limite de palavras
#text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/ios", 
# categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#print(dataset.data)

list_words_stemmers = []
list_words_stemmer = []
list_words_to_stemmer = []

print("\n%d documents" % len(dataset.data))
print("%d categories" % len(dataset.target_names))
print()

labels = dataset.target
#true_k = np.unique(labels).shape[0]
tam = len(dataset.target)

#print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()
if opts.use_hashing:
    if opts.use_idf:
        # Perform an IDF normalization on the output of HashingVectorizer
        hasher = HashingVectorizer(n_features=10000,
                                   stop_words=stop_words, alternate_sign=False,
                                   norm=None, binary=False)
        vectorizer = make_pipeline(hasher, TfidfTransformer())
    else:
        vectorizer = HashingVectorizer(n_features=10000,
                                       stop_words=stop_words,
                                       alternate_sign=False, norm='l2',
                                       binary=False)
elif opts.use_stemmer_rslps:
    vectorizer = StemmedTfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
    print("USE STEMER RSLPS")

    if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)


elif opts.use_stemmer_snowball:
    vectorizer = StemmedTfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
    print("USE STEMER Snowball")

    if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)


else:
   vectorizer = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf,
                                 analyzer = stemmed_reverse)

   '''
   vectorizer = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
  '''

   if opts.use_normalizer:
      normalizer = Normalizer(copy=False)
      vectorizer_normalized = make_pipeline(vectorizer, normalizer)

if opts.use_normalizer:

  X = vectorizer_normalized.fit_transform(dataset.data)

else:
  X = vectorizer.fit_transform(dataset.data)

'''
print("\n\nX original: ")
print(X.shape[0])
print(X)
'''

#print(list_words_to_stemmer)
#print(len(list_words_to_stemmer))
#print(list_words_stemmers)

#print("done in %fs" % (time() - t0))


distribuicao_de_dados_no_dataset()

print("n_samples: %d, n_features: %d" % X.shape)
print()

if opts.n_components:
    print("\nPerforming dimensionality reduction using LSA")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)

    #print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    #print("Explained variance of the SVD step: {}%".format(
    #    int(explained_variance * 100)))

else:
  X = X.toarray()

'''
print("\n\nX after LSA: ")
print(len(X))
print(X)

print()
'''


# #############################################################################
# Do the actual clustering

range_n_clusters = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
#true_k = np.unique(labels).shape[0]
for n_clusters in range_n_clusters:
  # Create a subplot with 1 row and 2 columns
    #Para plotar o grafico de visualização dos dados nos cluters
    #fig, (ax1, ax2) = plt.subplots(1, 2)
    #Para pltar apenas o score da silhoette em cada cluster
    fig, (ax1) = plt.subplots(1)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value
    # seed of 10 for reproducibility.

    type = ''
    if opts.minibatch:
      clusterer = MiniBatchKMeans(n_clusters=n_clusters, init='k-means++', n_init=10, max_iter=300,
                           init_size=1000, batch_size=1000, verbose=opts.verbose, random_state =10)
      type = 'MiniBatchKmeans'
    else:
      clusterer = KMeans(n_clusters=n_clusters, init='k-means++', random_state =10, 
                  verbose=opts.verbose)
      type = 'Kmeans'

    #print("Clustering sparse data with %s" % km)
    t0 = time()
    cluster_labels = clusterer.fit_predict(X)

    if(n_clusters == 10):
      for i in range(0, n_clusters):
        estimativas_no_cluster = []
        for j in range(len(cluster_labels)):
          if(cluster_labels[j] == i):
            estimativas_no_cluster.append(int(dataset.target_names[dataset.target[j]]))

        estimativas_no_cluster = np.array(estimativas_no_cluster)
        print("\nCluster " +str(i))
        #print(estimativas_no_cluster)
        desvio = estimativas_no_cluster.std()
        media = estimativas_no_cluster.mean()

        print("Desvio Padrão do Cluster " +str(i) + " é de: " +str(desvio))
        print("Média das tarefas no Cluster " +str(i) + " é de: " +str(media))
        
    #print("done in %0.3fs" % (time() - t0))
    #print()

    #print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, km.labels_))
    #print("Completeness: %0.3f" % metrics.completeness_score(labels, km.labels_))
    #print("V-measure: %0.3f" % metrics.v_measure_score(labels, km.labels_))
    #print("Adjusted Rand-Index: %.3f"
    #      % metrics.adjusted_rand_score(labels, km.labels_))
    #print("Silhouette Coefficient: %0.3f"
    #      % metrics.silhouette_score(X, km.labels_, sample_size=1000))

    #print()

    
    if not opts.use_hashing and opts.terms_per_cluster:

      if(n_clusters == 10):
        print("\nTop terms per cluster with " +str(n_clusters) + " clusters:")

        if opts.n_components:
            original_space_centroids = svd.inverse_transform(clusterer.cluster_centers_)
            order_centroids = original_space_centroids.argsort()[:, ::-1]
        else:
            order_centroids = clusterer.cluster_centers_.argsort()[:, ::-1]

        terms = vectorizer.get_feature_names()
        for i in range(n_clusters):
            print("Cluster %d:" % i, end='')
            for ind in order_centroids[i, :10]:
                print(' %s' % terms[ind], end='')
            print()
        print()


    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
     # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
            "The average silhouette_score is :", silhouette_avg)

    
    if(opts.print_distribuicao_clusters):
      distribuicao_dos_dados_nos_clusters()
      print("\n")

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    
    #Salva o output da clusterização com 10 clusters
    '''
    if(n_clusters == 10):
      for index in range(0, len(dataset.target)):
        #print(cluster_labels)
        nome_arquivo = "cluster_" +str(cluster_labels[index]) +"_tarefa_"+str(index)
        file = open(nome_arquivo, "w")
        string = dataset.data[index]
        string_decode = string.decode("utf-8")
        dado = str(string_decode)
        file.write(dado) 
        file.close()
    '''
    

    y_lower = 10
    for i in range(n_clusters):
      # Aggregate the silhouette scores for samples belonging to
      # cluster i, and sort them
      ith_cluster_silhouette_values = \
      sample_silhouette_values[cluster_labels == i]

      ith_cluster_silhouette_values.sort()

      size_cluster_i = ith_cluster_silhouette_values.shape[0]
      y_upper = y_lower + size_cluster_i

      color = cm.spectral(float(i) / n_clusters)
      ax1.fill_betweenx(np.arange(y_lower, y_upper),
                        0, ith_cluster_silhouette_values,
                        facecolor=color, edgecolor=color, alpha=0.7)

      # Label the silhouette plots with their cluster numbers at the middle
      ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

      # Compute the new y_lower for next plot
      y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    #Para plotar o grafico de visualização dos dados nos cluters
    '''
    colors = cm.spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                  c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                  c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                      s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")
    '''

    plt.suptitle(("Silhouette analysis for " +str(type) +" clustering on sample data "
                    "with n_clusters = %d" % n_clusters),
                   fontsize=14, fontweight='bold')

    #plt.show()
    name_image = "Silhouette_for_" +str(n_clusters) + "_clusters.png" 
    #plt.savefig(name_image)