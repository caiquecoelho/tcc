#!/usr/bin/env python
#!-*- coding: utf8 -*-
from __future__ import print_function

from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import adjusted_rand_score

import time
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import sys
import pandas as pd
import nltk


def distribuicao_de_dados_no_dataset():
	print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

	total = 0
	total_classe_majoritaria = 0
	classe_majoritaria = categories[0]
	
	for categorie in categories:
		for t in text.target:
			if(text.target_names[t] == categorie):
				total += 1 

		porcentagem = 100.0 * total/tam
		print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + " tarefas, em porcentagem temos: " +str(int(porcentagem))  + "%")

		if(total > total_classe_majoritaria):
			classe_majoritaria = categorie
			total_classe_majoritaria = total

		total = 0

	total_de_elementos = len(target_train_total)
	taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

	print("\nClasse Majoritária é: " +str(classe_majoritaria))
	print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
	print(str(taxa_de_acerto_algoritmo_burro) + "%")
	print("\n\n")


def distribuicao_dos_dados_nos_clusters():
	print("Distribuição dos nos clusters")

	total = 0

	for cluster in range(0, n_clusters):
		for index in range(0, tam):
			if(cluster_labels[index] == cluster):
				total+=1

		porcentagem = 100.0 * total/tam
		print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: " +str(int(porcentagem)) + "%")

		total = 0

def salvar_output_clusters():
	file = open(nome_arquivo, "w")
	dado = str(text.data[index])
	file.write(dado) 
	file.close()


def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

def removeStopWords(texto):
	frases = []

	for palavras in texto:
		semstop = [p for p in palavras.split() if p not in stop_words]
		frases.append(semstop)

	return frases


def aplicaStemmer(texto):
	stemmer = nltk.stem.RSLPStemmer()
	frasesStemming = []
	for palavras in texto:
		#print(palavras)
		comStemming = [str(stemmer.stem(p)) for p in palavras if p not in stop_words]
		frasesStemming.append(comStemming)

	return frasesStemming

def buscaPalavras(frases):
	todasPalavras = []
	for palavras in frases:
		todasPalavras.extend(palavras)

	return todasPalavras

def buscaFrequencia(palavras):
	palavras = nltk.FreqDist(palavras)
	return palavras

def buscaPalavrasUnicas(frequencia):
	freq = frequencia.keys()
	return freq

def extratorPalavras(documento):
	doc = set(documento)
	caracteristicas = {}
	for palavras in palavrasUnicas:
		caracteristicas['%s' %palavras] = (palavras in doc)

	return caracteristicas


########################################################################################################

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai"}

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


#Extracao de palavras e vetorizacao manual
#classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database2.csv")
classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database_final.csv")
nomesTarefas = classificacoes['Tarefa']
tarefas = classificacoes['Tarefa'] +" "+ classificacoes['Descricao']
tipo = classificacoes['Tipo']
descricoesTarefas = classificacoes['Descricao']
marcacoes = classificacoes['Estimativa']
tam = len(nomesTarefas)

#print(tarefas[1166])

stop_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
caracteres = ["~", "^", "=", "?","!",'"', "+", ",", ".", "-", "'", "@", "#", "$", "%", "&", "*", "(", ")", "_", "<", ">", "{", "}", "[", "]", ";", ":", "\n", '\t', '/', '...']

textos = []
for i in range(tam):
	texto = str(tarefas[i])
	texto = texto.lower()

	for number in stop_numbers:
		texto = texto.replace(number, "")
	for caractere in caracteres:
		texto = texto.replace(caractere, "")

	textos.append(texto)

#print(textos)

textosQuebrados = tarefas.str.lower().str.split(' ')


for number in stop_numbers:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(number, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
		while(indice < tamTexto):
			texto.pop(indice)
			indice +=1
			tamTexto -= 1
		indice = 0


for caractere in caracteres:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(caractere, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
			elif(palavra == caractere):
				texto.pop(indice)
				indice +=1
				tamTexto -=1
		while(indice < tamTexto):
			texto.pop(indice)
			#print(texto)
			indice +=1
			tamTexto -= 1
		indice = 0


#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()
i = 0
for lista in textosQuebrados:
	#print(lista)
	#print(i)
	dicionario.update(lista)
	i+=1

totalDePalavras = len(dicionario)
#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}

#print totalDePalavras

#print vetorizar_texto(textosQuebrados[0], tradutor)
vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in textosQuebrados]
#print(vetoresDeTexto[0])
#print(tradutor)

'''
frasesStopWord = removeStopWords(textos)
#print(frasesStopWord)
frasesComStemming = aplicaStemmer(frasesStopWord)
palavras = buscaPalavras(frasesStopWord)
frequencia = buscaFrequencia(palavras)
palavrasUnicas = buscaPalavrasUnicas(frequencia)
'''


X = np.array(vetoresDeTexto)
y = np.array(marcacoes.tolist())


#distribuicao_de_dados_no_dataset()

range_n_clusters = [2, 4, 6, 8, 10]

for n_clusters in range_n_clusters:
 	# Create a subplot with 1 row and 2 columns
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(X)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    distribuicao_dos_dados_nos_clusters()

    print("\n\n")

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,
                          facecolor=color, edgecolor=color, alpha=0.7)

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cm.spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
	ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                    s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    plt.show()