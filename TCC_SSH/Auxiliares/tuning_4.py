#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
from sklearn import model_selection
from sklearn import neural_network
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline

from sklearn.externals.six import StringIO
#import pydotplus

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa

#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyDT = list()
accuracySvm = list()
accuracyKnn = list()
accuracyMLP = list()
accuracyMultinomial = list()
accuracyAdaBoost = list()
accuracyAdaBoostTree = list()
accuracyOneVsRest = list()
accuracyOneVsOne = list()
accuracyLR = list()
accuracyBagging = list()
accuracyBaggingTree = list()
accuracyGB = list()
accuracyGBparams = list()
accuracyRF = list()
accuracySGDC = list()


####### Parameters tuning

# DECISION TREE
decision_tree = tree.DecisionTreeClassifier()
param_grid = {'max_depth': [None, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
'criterion': ('gini', 'entropy'),
'random_state': (None, 0, 42)
}
random_search = GridSearchCV(decision_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
decisionTreeBestParams = random_search.best_params_
decisionTree_clf = random_search.best_estimator_
print("Decision Tree Best Params: ")
print(decisionTreeBestParams)
print("")

# SVM
'''
svc = svm.SVC()
#param_grid = {'C': [scipy.stats.expon(scale=100), scipy.stats.expon(scale=200)], 'gamma': [scipy.stats.expon(scale=.1), scipy.stats.expon(scale=.05)], 'kernel': ['rbf', 'linear'], 'max_iter': [1000, 2000]}
#max_iter : [5, 10, 100]
#ConvergenceWarning: Solver terminated early (max_iter=5, 10, 100).  Consider pre-processing your data with StandardScaler or MinMaxScaler.
#Ainda da este erro acima para max_iter = 150 ou 200
param_grid = {
	'tol': (1e-1, 1e-2, 1e-3, 1e-4, 1e-5),
	'kernel': ['linear', 'rbf', 'poly'], 
	'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000],
	'gamma': ['auto', 0.00001, 0.0001, 0.001, 0.01, 0.1],
	'decision_function_shape': ('ovo', 'ovr'),
	'probability': [False, True],
	'shrinking': [True, False],
	'max_iter': [-1, 150, 200, 500, 800, 1000, 1500, 2000],
	'class_weight': [None, 'balanced'],
	'random_state': (None, 0, 42)
}
random_search = GridSearchCV(svc, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
svmBestParams = random_search.best_params_
print("SVM Best Params: ")
print(svmBestParams)
print("")
'''
# KNN
# ValueError: Expected n_neighbors <= n_samples,  but n_samples = 12, n_neighbors = 13
knn = neighbors.KNeighborsClassifier()
param_grid = {'n_neighbors': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], 
	'algorithm': ['auto', 'brute'],
	'metric': ['euclidean', 'minkowski'], 
	'weights': ['uniform', 'distance']
}
random_search = GridSearchCV(knn, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
knnBestParams = random_search.best_params_
knn_clf = random_search.best_estimator_
print("KNN Best Params: ")
print(knnBestParams)
print("")


# MLP
'''
#'learning_rate': ['constant', 'invscaling', 'adaptive'],
#'learning_rate_init': [scipy.stats.expon(scale=.001), 0.0001, 0.01],
mlp = neural_network.MLPClassifier()
param_grid = {'solver': ['sgd', 'adam'], 
				'momentum': [0.1, 0.3, 0.6, 0.9],
				'alpha': [10 ** x for x in range(-6, 1)], 
				'learning_rate': ['constant', 'invscaling', 'adaptive'],
				'activation': ['identity', 'logistic', 'tanh', 'relu'],
				'hidden_layer_sizes': [(200, 50, 20), (100, 20), (100, 50), (200, 20), (200, 50), (50, 10),
				(200,), (100,), (50,)], 
				'max_iter': [2000, 1000]
			}
random_search = GridSearchCV(mlp, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
mlpBestParams = random_search.best_params_
print("MLP Best Params: ")
print(mlpBestParams)
print("")
'''
#LogisticRegression
#ValueError: Logistic Regression supports only liblinear, newton-cg, lbfgs and sag solvers, got saga
#ValueError: Solver newton-cg supports only l2 penalties, got l1 penalty.
#ValueError: Solver liblinear does not support a multinomial backend.
#Testar sem liblinear e sem multinomial
#'multi_class': ('ovr', 'multinomial'),

#'C': [0.001, 0.01, 0.1, 0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10, 100, 1000
#'max_iter': (50, 100, 150, 200, 400),
param_grid = {'C': [0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10],
				'random_state': (None, 0, 42),
				'tol': (1e-5, 1e-4, 1e-3, 1e-2),
				'fit_intercept': (True, False),
				'class_weight': (None, 'balanced'),
				'solver': ('newton-cg', 'lbfgs', 'liblinear', 'sag'),
				'max_iter': (100, 150, 200),
				'warm_start': (True, False)
}
grid_search = GridSearchCV(LogisticRegression(), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
grid_search.fit(data_train_total, target_train_total)
logisticRegressionBestParams = grid_search.best_params_
logisticRegression_clf = grid_search.best_estimator_
print("LogisticRegression Best Params: ")
print(logisticRegressionBestParams)
print("")

#Random Forest
'''
param_grid = {
				'n_estimators': (10, 20, 30, 50, 100, 300, 500, 700, 1000),
				'criterion': ('gini', 'entropy'),
				'max_depth': (None, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30),
				'random_state': (None, 0, 42)
				'warm_start': (False, True),
				'oob_score': (False, True)
}

random = RandomForestClassifier()

random_grid = GridSearchCV(random, param_grid,  cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_grid.fit(data_train_total, target_train_total)
randomBestParams = random_grid.best_params_

print("Random Forest Best Params: ")
print(randomBestParams)
print("")
'''


#Bagging Classifier com base_estimator
#'warm_start': (True, False)
#Out of bag estimate only available if warm_start=False
# 'oob_score' : ['true', 'false']
#UserWarning: Some inputs do not have OOB scores. This probably means too few estimators were used to compute any reliable oob estimates.
'''
param_grid = {
    'max_samples' : [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.8, 1],
    'n_estimators': (5, 10, 50, 100, 200, 500), 
    'max_features': (0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 0.7, 1, 2),
    'bootstrap': (False, True),
    'bootstrap_features': (False, True),
    'random_state': (None, 0, 42),
	'warm_start': (False, True),
	'oob_score': (False, True)
}

baggingTree_grid = GridSearchCV(BaggingClassifier(tree.DecisionTreeClassifier(**decisionTreeBestParams)), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
baggingTree_grid.fit(data_train_total, target_train_total)
baggingTreeBestParams = baggingTree_grid.best_params_

print("Bagging Decision Tree Best Params: ")
print(baggingTreeBestParams)
print("")
'''

#Bagging Classifier sem base_estimator
#'warm_start': (True, False)
#Out of bag estimate only available if warm_start=False
# 'oob_score' : ['true', 'false']
#UserWarning: Some inputs do not have OOB scores. This probably means too few estimators were used to compute any reliable oob estimates.
'''
param_grid = {
    'max_samples' : [0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.8, 1],
    'n_estimators': (5, 10, 50, 100, 200, 500), 
    'max_features': (0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 0.7, 1, 2),
    'bootstrap': (False, True),
    'bootstrap_features': (False, True),
    'random_state': (None, 0, 42),
	'warm_start': (False, True),
	'oob_score': (False, True)
}

bagging_grid = GridSearchCV(BaggingClassifier(), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
bagging_grid.fit(data_train_total, target_train_total)
baggingBestParams = bagging_grid.best_params_

print("Bagging Best Params: ")
print(baggingBestParams)
print("")
'''


#SGDC

param_grid = {
	'alpha': [10 ** x for x in range(-8, 1)], 
	'fit_intercept': (True, False),
	'penalty': ('l1', 'l2'),
	'loss': ('hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron', 'squared_loss', 'huber', 'epsilon_insensitive', 'squared_epsilon_insensitive'),
	'learning_rate': ('constant', 'optimal', 'invscaling'),
	'eta0': (1, 0.1, 0.05, 0.01, 0.001, 0.0001, 0.00001, 0.000001),
	'random_state': (None, 0, 42)
}

sgdc = SGDClassifier()

sgdc_grid = GridSearchCV(sgdc, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
sgdc_grid.fit(data_train_total, target_train_total)
sgdcBestParams = sgdc_grid.best_params_
sgdc_clf = sgdc_grid.best_estimator_
print("SGDC Best Params: ")
print(sgdcBestParams)
print("")

#AdaBoost sem base_estimator

param_grid = {
	'n_estimators': (5, 10, 20, 50, 80, 100, 200),
	'learning_rate': (0.0001, 0.001, 0.01, 0.1, 1, 1.5, 2, 4),
	'random_state': (None, 0, 42)
}

adaBoost = AdaBoostClassifier()

ada_grid = GridSearchCV(adaBoost, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
ada_grid.fit(data_train_total, target_train_total)
adaBestParams = ada_grid.best_params_
ada_clf = ada_grid.best_estimator_
print("AdaBoost Best Params: ")
print(adaBestParams)
print("")

#AdaBoost com base_estimator

param_grid = {
	'n_estimators': (5, 10, 20, 50, 80, 100, 200),
	'learning_rate': (0.0001, 0.001, 0.01, 0.1, 1, 1.5, 2, 4),
	'random_state': (None, 0, 42)
}

adaBoost_tree = AdaBoostClassifier(tree.DecisionTreeClassifier(**decisionTreeBestParams))

adaTree_grid = GridSearchCV(adaBoost_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
adaTree_grid.fit(data_train_total, target_train_total)
adaTree_clf = adaTree_grid.best_estimator_
adaTreeBestParams = adaTree_grid.best_params_

print("AdaBoost - Decision Tree Best Params: ")
print(adaTreeBestParams)
print("")

#MultinomialNB

param_grid = {
	'alpha': (0.5, 1, 1.5, 2),
	'fit_prior': (True, False)
}

multinomialNB = MultinomialNB()

multinomialNB_grid = GridSearchCV(multinomialNB, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
multinomialNB_grid.fit(data_train_total, target_train_total)
multinomialNBBestParams = multinomialNB_grid.best_params_
multinomialNB_clf = multinomialNB_grid.best_estimator_
print("MultinomialNB Best Params: ")
print(multinomialNBBestParams)
print("")


#GradientBoostingClassifier
#'max_depth': (None, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30),
#'learning_rate': (0.0001, 0.001, 0.01, 0.1, 1, 1.5, 2, 4),
#'n_estimators': (5, 10, 50, 100, 150, 200, 300, 500),
#'warm_start': (False, True),
#'presort': (False, True, 'auto')
#'loss': ('deviance', 'exponencial'),

#'learning_rate': (0.001, 0.01, 0.1, 0.5),
#'n_estimators': (5, 20, 40, 50, 100),
#'max_depth': (None, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
param_grid = {
	'learning_rate': (0.01, 0.1, 0.5),
	'n_estimators': (20, 40, 50, 100), 
	'max_depth': (None, 2, 3, 4, 5, 6, 7),
	'random_state': (None, 0, 42),
}

gradientBoosting = GradientBoostingClassifier()

gradientBoosting_grid = GridSearchCV(gradientBoosting, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
#TypeError: A sparse matrix was passed, but dense data is required. Use X.toarray() to convert to a dense numpy array.
gradientBoosting_grid.fit(data_train_total.toarray(), target_train_total)
gradientBoostingBestParams = gradientBoosting_grid.best_params_
gradientBoosting_clf = gradientBoosting_grid.best_estimator_
print("Gradient Boosting Best Params: ")
print(gradientBoostingBestParams)
print("")


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

	########################################################## DECISION TREE

	#decision_tree = tree.DecisionTreeClassifier(**decisionTreeBestParams)
	decisionTree_clf.fit(data_train, target_train)

	output = decisionTree_clf.predict(data_test)
	accuracyDT.append(metrics.accuracy_score(target_test, output))




	########################################################## SVC

	#svc = svm.SVC(**svmBestParams)
	svc = svm.SVC()
	svc = svc.fit(data_train, target_train)
 
	output = svc.predict(data_test)
	accuracySvm.append(metrics.accuracy_score(target_test, output))


 	########################################################## KNN
	
	#knn = neighbors.KNeighborsClassifier(**knnBestParams)
	knn_clf.fit(data_train.toarray(), target_train)

	#toarray para resolver: kd_tree does not work with sparse matrices. Densify the data, or set algorithm='brute'
	output = knn_clf.predict(data_test.toarray())
	accuracyKnn.append(metrics.accuracy_score(target_test, output))


	########################################################## MLP
	
	#mlp = neural_network.MLPClassifier(**mlpBestParams)
	mlp = neural_network.MLPClassifier()
	mlp.fit(data_train, target_train)

	output = mlp.predict(data_test)
	accuracyMLP.append(metrics.accuracy_score(target_test, output))

	########################################################## MultinomialNB

	#multinomial = MultinomialNB(**multinomialNBBestParams)
	multinomialNB_clf.fit(data_train, target_train)

	output = multinomialNB_clf.predict(data_test)
	accuracyMultinomial.append(metrics.accuracy_score(target_test, output))
	
########################################################## AdaBoost

	#adaBoost = ABCBestParams
	#adaBoost = AdaBoostClassifier(**adaBestParams)
	ada_clf.fit(data_train.toarray(), target_train)

	output = ada_clf.predict(data_test)
	accuracyAdaBoost.append(metrics.accuracy_score(target_test, output))

########################################################## AdaBoostTree

	#adaBoost = ABCBestParams
	#adaBoostTree = AdaBoostClassifier(**adaTreeBestParams)
	adaTree_clf.fit(data_train.toarray(), target_train)
	print(adaTree_clf)

	output = adaTree_clf.predict(data_test)
	accuracyAdaBoostTree.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsRest

	oneVsRest = OneVsRestClassifier(LinearSVC())
	oneVsRest.fit(data_train, target_train)

	output = oneVsRest.predict(data_test)
	accuracyOneVsRest.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsOne

	oneVsOne = OneVsOneClassifier(LinearSVC())
	oneVsOne.fit(data_train, target_train)

	output = oneVsOne.predict(data_test)
	accuracyOneVsOne.append(metrics.accuracy_score(target_test, output))


########################################################## LogisticRegression

	#logisticRegression = LogisticRegression(**logisticRegressionBestParams)
	logisticRegression_clf.fit(data_train, target_train)

	output = logisticRegression_clf.predict(data_test)
	accuracyLR.append(metrics.accuracy_score(target_test, output))


########################################################## BaggingClassifier

	#bagging_clf = BaggingClassifier(**baggingBestParams)
	bagging_clf = BaggingClassifier()

	bagging_clf.fit(data_train, target_train)

	output = bagging_clf.predict(data_test)
	accuracyBagging.append(metrics.accuracy_score(target_test, output))

########################################################## BaggingClassifier - Decision Tree

	#baggingTree_clf = BaggingClassifier(**baggingTreeBestParams)
	baggingTree_clf = BaggingClassifier()

	baggingTree_clf.fit(data_train, target_train)

	output = baggingTree_clf.predict(data_test)
	accuracyBaggingTree.append(metrics.accuracy_score(target_test, output))


########################################################## GradientBoostingClassifier
	
	#gradientBoosting = GradientBoostingClassifier()
	gradientBoosting_cl	f.fit(data_train, target_train)

	output = gradientBoosting_clf.predict(data_test.toarray())
	accuracyGB.append(metrics.accuracy_score(target_test, output))
	


########################################################## GradientBoostingClassifier Params
	
	gradientBoosting = GradientBoostingClassifier(**gradientBoostingBestParams)
	gradientBoosting.fit(data_train, target_train)

	output = gradientBoosting.predict(data_test.toarray())
	accuracyGBparams.append(metrics.accuracy_score(target_test, output))
	

########################################################## RandomForestClassifier

	#random_clf = RandomForestClassifier(**randomBestParams)
	random_clf = RandomForestClassifier(n_estimators = 20, random_state = 0, criterion = "gini", max_depth = 20)
	random_clf.fit(data_train, target_train)

	output = random_clf.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))

########################################################## SDGC

	#sgdc_clf = SGDClassifier(**sgdcBestParams)

	sgdc_clf.fit(data_train, target_train)

	output = sgdc_clf.predict(data_test)
	accuracySGDC.append(metrics.accuracy_score(target_test, output))




##### DT AVG
avg = np.mean(accuracyDT)
std = np.std(accuracyDT)
top_avg = avg
top_avg_name = "DT"
print("DT: " +str(accuracyDT))
print("DT Media: " +str(avg))
print("")

##### SVM AVG
avg = np.mean(accuracySvm)
std = np.std(accuracySvm)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SVM"

print("SVM: " +str(accuracySvm))
print("SVM Media: " +str(avg))
print("")

##### KNN AVG
avg = np.mean(accuracyKnn)
std = np.std(accuracyKnn)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "KNN"

print("KNN: " +str(accuracyKnn))
print("KNN Media: " +str(avg))
print("")

##### MLP AVG
avg = np.mean(accuracyMLP)
std = np.std(accuracyMLP)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "MLP"

print("MLP: " +str(accuracyMLP))
print("MLP Media: " +str(avg))
print("\n")

##### Multinomial Naive Bayes AVG
avg = np.mean(accuracyMultinomial)
std = np.std(accuracyMultinomial)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Multinomial Naive Bayes"

print("Multinomial Naive Bayes: " +str(accuracyMultinomial))
print("Multinomial Naive Bayes Media: " +str(avg))
print("\n")

##### AdaBoost AVG
avg = np.mean(accuracyAdaBoost)
std = np.std(accuracyAdaBoost)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost"

print("AdaBoost: " +str(accuracyAdaBoost))
print("AdaBoost Media: " +str(avg))
print("\n")

##### AdaBoostTree AVG
avg = np.mean(accuracyAdaBoostTree)
std = np.std(accuracyAdaBoostTree)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost - Decision Tree"

print("AdaBoost Decisio Tree: " +str(accuracyAdaBoostTree))
print("AdaBoost Decision Tree Media: " +str(avg))
print("\n")

##### OneVsRest AVG
avg = np.mean(accuracyOneVsRest)
std = np.std(accuracyOneVsRest)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsRest"

print("OneVsRest: " +str(accuracyOneVsRest))
print("OneVsRest Media: " +str(avg))
print("\n")

##### OneVsOne AVG
avg = np.mean(accuracyOneVsOne)
std = np.std(accuracyOneVsOne)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsOne"

print("OneVsOne: " +str(accuracyOneVsOne))
print("OneVsOne Media: " +str(avg))
print("\n")

##### Logistic Regression AVG
avg = np.mean(accuracyLR)
std = np.std(accuracyLR)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LogisticRegression"

print("LogisticRegression: " +str(accuracyLR))
print("LogisticRegression Media: " +str(avg))
print("\n")

##### Bagging AVG
avg = np.mean(accuracyBagging)
std = np.std(accuracyBagging)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "BaggingClassifier"

print("BaggingClassifier: " +str(accuracyBagging))
print("BaggingClassifier Media: " +str(avg))
print("\n")

##### Bagging - Decision Tree AVG
avg = np.mean(accuracyBaggingTree)
std = np.std(accuracyBaggingTree)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Bagging - Decision Tree"

print("Bagging - Decision Tree: " +str(accuracyBagging))
print("Bagging - Decision Tree Media: " +str(avg))
print("\n")

##### GradientBoosting AVG
avg = np.mean(accuracyGB)
std = np.std(accuracyGB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "GradientBoosting"

print("GradientBoosting: " +str(accuracyGB))
print("GradientBoosting Media: " +str(avg))
print("\n")

##### GradientBoosting Params AVG
avg = np.mean(accuracyGBparams)
std = np.std(accuracyGBparams)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "GradientBoosting Params"

print("GradientBoosting Params: " +str(accuracyGBparams))
print("GradientBoosting Params Media: " +str(avg))
print("\n")

##### RandomForest AVG
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "RandomForest"

print("RandomForest: " +str(accuracyRF))
print("RandomForest Media: " +str(avg))
print("\n")

##### SGDC AVG
avg = np.mean(accuracySGDC)
std = np.std(accuracySGDC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SGDC"

print("SGDC: " +str(accuracySGDC))
print("SGDC Media: " +str(avg))
print("\n")

print("\n")

print("TOP AVG IS " + top_avg_name +str(": ") +str(top_avg))
