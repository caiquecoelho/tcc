#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
import time
from sklearn import model_selection
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
import csv
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import BernoulliNB
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.linear_model import Perceptron
from sklearn.neighbors import NearestCentroid
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import SVC


t0 = time.time()

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}



#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database 3, 6 categorias, min: 15 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/15", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#count_vect = CountVectorizer(ngram_range=(1, 2))
count_vect = CountVectorizer(ngram_range=(1, 2), stop_words=stop_words)
X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyPassiveAggresive = list()
accuracyPerceptron = list()
accuracyRidge = list()
accuracyLinearSVC = list()
accuracyNearestCentroid = list()
accuracyBernoulliNB = list()
accuracySVC = list()


####### Parameters tuning

#PassiveAggressiveClassifier
#'average': ['True', 10, 20],
params = {
	'C': [1, 10], 
	'fit_intercept': (False, True),
	'n_iter' : (50, 100, 200, 500, 1000, 2000, 4000),
}

passive = PassiveAggressiveClassifier()

passive_grid = GridSearchCV(passive, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

passive_grid.fit(data_train_total, target_train_total)
passiveBestParams = passive_grid.best_params_

print("Passive Aggressive Best Params: ")
print(passiveBestParams)
print("")

#Perceptron
#'tol': (1e-2, 1e-3),
params = {
	'penalty': ("l2", "l1"), 
	'fit_intercept': (False, True),
	'alpha': [10 ** x for x in range(-6, 1)],
	'n_iter' : (50, 100, 200, 500, 1000, 2000, 4000),
}

perceptron = Perceptron()

perceptron_grid = GridSearchCV(perceptron, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

perceptron_grid.fit(data_train_total, target_train_total)
perceptronBestParams = perceptron_grid.best_params_

print("Perceptron Best Params: ")
print(perceptronBestParams)
print("")


#RidgeClassifier
#'solver' : ('auto', 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga')
# SVD solver does not support sparse inputs currently
# Solver saga not understood
params = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'normalize': (False, True),
	'fit_intercept': (False, True),
	'tol': (1e-2, 1e-3),
	'solver' : ('auto', 'cholesky', 'lsqr', 'sparse_cg', 'sag')
}

ridge = RidgeClassifier()

ridge_grid = GridSearchCV(ridge, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

ridge_grid.fit(data_train_total, target_train_total)
ridgeBestParams = ridge_grid.best_params_

print("Ridge Best Params: ")
print(ridgeBestParams)
print("")

#BernoulliNB
params = {
	'binarize': (False, True),
	'alpha': [10 ** x for x in range(-6, 1)],
	'fit_prior': (True, False),
}

bernoulliNB = BernoulliNB()

bernoulliNB_grid = GridSearchCV(bernoulliNB, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

bernoulliNB_grid.fit(data_train_total, target_train_total)
bernoulliNBBestParams = bernoulliNB_grid.best_params_

print("BernoulliNB Best Params: ")
print(bernoulliNBBestParams)
print("")

#LinearSVC
# loss': ('hinge', 'squared_hinge'),
# dual : (False, True)
# Unsupported set of arguments: The combination of penalty='l1' and loss='squared_hinge' are not supported when dual=True
params = {
	'penalty': ('l2', 'l1'),
	'fit_intercept': (False, True),
	'tol': (1e-2, 1e-3),
	'C': [1, 10]
}

linearSVC = LinearSVC(dual = False, loss = 'squared_hinge')

linearSVC_grid = GridSearchCV(linearSVC, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

linearSVC_grid.fit(data_train_total, target_train_total)
linearSVCBestParams = linearSVC_grid.best_params_

print("LinearSVC Best Params: ")
print(linearSVCBestParams)
print("")

#SVC

params = {
	'tol': (1e-2, 1e-3),
	'kernel': ['linear', 'rbf', 'poly'], 
	'C': [1, 10],
	'gamma': [0.001, 0.0001],
	'decision_function_shape': ('ovo', 'ovr')
}

svc = SVC()

svc_grid = GridSearchCV(svc, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

svc_grid.fit(data_train_total, target_train_total)
svcBestParams = svc_grid.best_params_

print("SVC Best Params: ")
print(svcBestParams)
print("")


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

	
########################################################## Passive Agressive

	passive = PassiveAggressiveClassifier(**passiveBestParams)

	passive.fit(data_train, target_train)

	output = passive.predict(data_test)
	accuracyPassiveAggresive.append(metrics.accuracy_score(target_test, output))


########################################################## Perceptron

	#perceptron = Perceptron()
	perceptron = Perceptron(**perceptronBestParams)

	perceptron.fit(data_train, target_train)

	output = perceptron.predict(data_test)
	accuracyPerceptron.append(metrics.accuracy_score(target_test, output))


########################################################## Ridge

	ridge = RidgeClassifier(**ridgeBestParams)

	ridge.fit(data_train, target_train)

	output = ridge.predict(data_test)
	accuracyRidge.append(metrics.accuracy_score(target_test, output))


########################################################## LinearSVC

	linearSVC = LinearSVC(**linearSVCBestParams)

	linearSVC.fit(data_train, target_train)

	output = linearSVC.predict(data_test)
	accuracyLinearSVC.append(metrics.accuracy_score(target_test, output))


########################################################## Nearest Centroid
	
	nearestCentroid = NearestCentroid()

	nearestCentroid.fit(data_train, target_train)

	output = nearestCentroid.predict(data_test.toarray())
	accuracyNearestCentroid.append(metrics.accuracy_score(target_test, output))
	

########################################################## BernoulliNB

	bernoulliNB = BernoulliNB(**bernoulliNBBestParams)

	bernoulliNB.fit(data_train, target_train)

	output = bernoulliNB.predict(data_test)
	accuracyBernoulliNB.append(metrics.accuracy_score(target_test, output))

########################################################## SVC
	
	svc_clf = SVC(**svcBestParams)

	svc_clf.fit(data_train, target_train)

	output = svc_clf.predict(data_test.toarray())
	accuracySVC.append(metrics.accuracy_score(target_test, output))



##### Passive Agressive AVG
avg = np.mean(accuracyPassiveAggresive)
top_avg = avg
std = np.std(accuracyPassiveAggresive)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Passive Agressive"

print("Passive Agressive: " +str(accuracyPassiveAggresive))
print("Passive Agressive Media: " +str(avg))
print("\n")

##### Perceptron AVG
avg = np.mean(accuracyPerceptron)
std = np.std(accuracyPerceptron)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Perceptron"

print("Perceptron: " +str(accuracyPerceptron))
print("Perceptron Media: " +str(avg))
print("\n")

##### Ridge AVG
avg = np.mean(accuracyRidge)
std = np.std(accuracyRidge)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Ridge"

print("Ridge: " +str(accuracyRidge))
print("Ridge Media: " +str(avg))
print("\n")

##### LinearSVC AVG
avg = np.mean(accuracyLinearSVC)
std = np.std(accuracyLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LinearSVC"

print("LinearSVC: " +str(accuracyLinearSVC))
print("LinearSVC Media: " +str(avg))
print("\n")

##### NearestCentroid AVG
avg = np.mean(accuracyNearestCentroid)
std = np.std(accuracyNearestCentroid)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "NearestCentroid"

print("NearestCentroid: " +str(accuracyNearestCentroid))
print("NearestCentroid Media: " +str(avg))
print("\n")

##### BernoulliNB AVG
avg = np.mean(accuracyBernoulliNB)
std = np.std(accuracyBernoulliNB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "BernoulliNB"

print("BernoulliNB: " +str(accuracyBernoulliNB))
print("BernoulliNB Media: " +str(avg))
print("\n")

##### SVC AVG
avg = np.mean(accuracySVC)
std = np.std(accuracySVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SVC"

print("SVC: " +str(accuracySVC))
print("SVC Media: " +str(avg))
print("\n")


print("\n")

print("TOP AVG IS " + top_avg_name +str(": ") +str(top_avg))

print("\n\n")

elapsed_time_seconds = time.time() - t0

elapsed_time_minutes = int(elapsed_time_seconds) / 60

print("Tempo total executando: %.2fm" % (elapsed_time_minutes))