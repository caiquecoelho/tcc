#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import metrics
from sklearn import tree
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.ensemble import BaggingClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline


#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)



count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyBagging = list()
accuracyBaggingTree = list()


# DECISION TREE
decision_tree = tree.DecisionTreeClassifier()
param_grid = {'max_depth': [None, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
'criterion': ('gini', 'entropy'),
'random_state': (None, 0, 42, 3, 50)
}
random_search = GridSearchCV(decision_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
decisionTreeBestParams = random_search.best_params_
print("Decision Tree Best Params: ")
print(decisionTreeBestParams)
print("")


#Bagging Classifier com base_estimator
#Out of bag estimate only available if warm_start=False
#UserWarning: Some inputs do not have OOB scores. This probably means too few estimators were used to compute any reliable oob estimates.
# oob_score:  Out of bag estimation only available if bootstrap=True

param_grid = {
    'max_samples' : [0.99, 1],
    'n_estimators': (44, 45), 
    'max_features': (0.46, 0.47),
    'random_state': (3, None),
    'oob_score' : (True, False),
    'bootstrap_features': (True, False)
}

baggingTree_grid = GridSearchCV(BaggingClassifier(tree.DecisionTreeClassifier(**decisionTreeBestParams)), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
baggingTree_grid.fit(data_train_total, target_train_total)
baggingTreeBestParams = baggingTree_grid.best_params_

print("Bagging Decision Tree Best Params: ")
print(baggingTreeBestParams)
print("")


#Bagging Classifier sem base_estimator
param_grid = {
    'max_samples' : [0.94, 0.9],
    'n_estimators': (550, 545), 
    'max_features': (0.034, 0.035),
    'oob_score' : (True, False),
    'bootstrap_features': (True, False)
}

bagging_grid = GridSearchCV(BaggingClassifier(), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
bagging_grid.fit(data_train_total, target_train_total)
baggingBestParams = bagging_grid.best_params_

print("Bagging Best Params: ")
print(baggingBestParams)
print("")

for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]


########################################################## BaggingClassifier

	bagging_clf = BaggingClassifier(**baggingBestParams)
	#bagging_clf = BaggingClassifier()

	bagging_clf.fit(data_train, target_train)

	output = bagging_clf.predict(data_test)
	accuracyBagging.append(metrics.accuracy_score(target_test, output))

########################################################## BaggingClassifier - Decision Tree

	baggingTree_clf = BaggingClassifier(**baggingTreeBestParams)
	#baggingTree_clf = BaggingClassifier()

	baggingTree_clf.fit(data_train, target_train)

	output = baggingTree_clf.predict(data_test)
	accuracyBaggingTree.append(metrics.accuracy_score(target_test, output))



##### Bagging AVG
avg = np.mean(accuracyBagging)
std = np.std(accuracyBagging)


print("BaggingClassifier: " +str(accuracyBagging))
print("BaggingClassifier Media: " +str(avg))
print("\n")

##### Bagging - Decision Tree AVG
avg = np.mean(accuracyBaggingTree)
std = np.std(accuracyBaggingTree)

print("Bagging - Decision Tree: " +str(accuracyBaggingTree))
print("Bagging - Decision Tree Media: " +str(avg))
print("\n")
