#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
from sklearn import model_selection
from sklearn import neural_network
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline
from hpsklearn import HyperoptEstimator, any_classifier, any_sparse_classifier, tfidf, random_forest, svc, knn
from sklearn.datasets import fetch_20newsgroups
from sklearn import metrics
from hyperopt import tpe, hp
import numpy as np
from sklearn.feature_extraction.text import HashingVectorizer
from scipy import sparse

from sklearn.externals.six import StringIO
#import pydotplus
'''
git clone https://github.com/hyperopt/hyperopt-sklearn.git
cd hyperopt
pip install -e .

sudo pip install networkx==1.11
'''


#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/Estimativas/database_3/6_categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#vectorizer = HashingVectorizer(non_negative=True)
#train_vectors = vectorizer.fit_transform(text.data)

count_vect = CountVectorizer(stop_words=stop_words, ngram_range=(1, 2))
X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape

data_train_total = X_train_tfidf
#data_train_total = text.data
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyRF = list()



##########################################################################################
##########################################################################################

'''
clf = hp.pchoice( 'my_name', 
          [ ( 0.4, random_forest('my_name.random_forest') ),
            ( 0.3, svc('my_name.svc') ),
            ( 0.3, knn('my_name.knn') ) ])

estim = HyperoptEstimator( classifier=clf )


#estim = HyperoptEstimator( classifier=svc('mySVC') )
##########################################################################################
##########################################################################################
	
estim = HyperoptEstimator( classifier=any_sparse_classifier('clf'),
								 preprocessing=[tfidf('tfidf')],
	                            algo=tpe.suggest, trial_timeout=300)

tam = len(target_train_total)

divisao = int(tam/3)

train = divisao * 2

data_train = data_train_total[:train]
target_train = target_train_total[:train]

data_teste = data_train_total[train:]
target_teste = target_train_total[train:]

print(data_train)

estim.fit(data_train, target_train)

output = estim.predict(data_teste.toarray())
accuracyRF.append(metrics.accuracy_score(target_teste, output))

print( estim.score( data_teste.toarray(), target_test) )
# <<show score here>>
print( estim.best_model() )

'''

#estim = HyperoptEstimator( classifier=random_forest('my_name.random_forest'),
#							trial_timeout=1000)
#estim = HyperoptEstimator( classifier=any_sparse_classifier('clf'),
#							preprocessing=[tfidf('tfidf')]
#                            algo=tpe.suggest, trial_timeout=300)
estim = HyperoptEstimator( classifier=any_classifier('clf'))

estim.fit(data_train_total.toarray(), target_train_total)

#estim.fit(sparse.hstack((data_train_total,data_train_total)).A, target_train_total)

print( estim.best_model() )

print("Porcentagem de acerto: " + str(estim.score( data_train_total[:30].toarray(), target_train_total[:30]) ))


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]


########################################################## RandomForestClassifier

	#print(data_train)

	estim.fit(data_train.toarray(), target_train)

	output = estim.predict(data_test.toarray())
	accuracyRF.append(metrics.accuracy_score(target_test, output))

	print( estim.best_model() )

	print("Porcentagem de acerto: " + str(estim.score( data_test.toarray(), target_test)) )


##### RandomForest AVG
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

print("HyperoptEstimator: " +str(accuracyRF))
print("HyperoptEstimator Media: " +str(avg))
print("\n")
