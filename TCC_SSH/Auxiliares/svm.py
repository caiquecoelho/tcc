#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import svm
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline


#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)



count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)

accuracySvm = list()
accuracySVC = list()


# SVM Grid
'''
svc = svm.SVC()
#param_grid = {'C': [scipy.stats.expon(scale=100), scipy.stats.expon(scale=200)], 'gamma': [scipy.stats.expon(scale=.1), scipy.stats.expon(scale=.05)], 'kernel': ['rbf', 'linear'], 'max_iter': [1000, 2000]}
#max_iter : [5, 10, 100]
#ConvergenceWarning: Solver terminated early (max_iter=5, 10, 100).  Consider pre-processing your data with StandardScaler or MinMaxScaler.
#Ainda da este erro acima para max_iter = 150 ou 200
param_grid = {
	'tol': (1.3, 1.2, 1.1, 1),
	'kernel': ['linear', 'rbf'], 
	'C': [0.8, 1, 1.2, 10],
	'gamma': ['auto', 0.01],
	'decision_function_shape': ('ovo', 'ovr'),
	'max_iter': [-1, 500, 1900],
	'class_weight': [None, 'balanced', {0:.08, 1:.35, 2:.04, 3:.32, 4:.07, 5:.11}],
	'random_state': (None, 3)
}

grid_search = GridSearchCV(svc, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
grid_search.fit(data_train_total, target_train_total)
svmBestParams = grid_search.best_params_
print("SVM Best Params: ")
print(svmBestParams)
print("")
'''


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	


	########################################################## SVM
	svc = svm.SVC(kernel= 'linear', C= 1, max_iter= -1, decision_function_shape= 'ovo', tol= 1.2, gamma= 'auto')
	svc.fit(data_train, target_train)
 
	output = svc.predict(data_test)
	accuracySvm.append(metrics.accuracy_score(target_test, output))

	########################################################## SVC

	#svc = svm.SVC(**svmBestParams)
	svc0 = svm.SVC()
	svc0.fit(data_train, target_train)
 
	output = svc0.predict(data_test)
	accuracySVC.append(metrics.accuracy_score(target_test, output))


##### SVM AVG
avg = np.mean(accuracySvm)
std = np.std(accuracySvm)

print("SVC Grid: " +str(accuracySvm))
print("SVM Media: " +str(avg))
print("")

##### SVC AVG
avg = np.mean(accuracySVC)
std = np.std(accuracySVC)

print("SVC: " +str(accuracySVC))
print("SVC Media: " +str(avg))
print("")
