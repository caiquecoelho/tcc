#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn import metrics
from sklearn import model_selection
from sklearn import neural_network
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import ExtraTreesClassifier
import csv
from sklearn.pipeline import Pipeline

from sklearn.externals.six import StringIO
#import pydotplus

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)



count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyDT = list()
accuracyKnn = list()
accuracyMultinomial = list()
accuracyAdaBoost = list()
accuracyAdaBoostTree = list()
accuracyLR = list()
accuracyGB = list()
accuracySGDC = list()
accuracyET = list()


####### Parameters tuning

# DECISION TREE
decision_tree = tree.DecisionTreeClassifier()
param_grid = {'max_depth': [3, 4, 5, 9],
'criterion': ('gini', 'entropy'),
'random_state': (None, 42)
}
random_search = GridSearchCV(decision_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
decisionTreeBestParams = random_search.best_params_
print("Decision Tree Best Params: ")
print(decisionTreeBestParams)
print("")


# KNN
# ValueError: Expected n_neighbors <= n_samples,  but n_samples = 12, n_neighbors = 13
knn = neighbors.KNeighborsClassifier()
param_grid = {'n_neighbors': [13, 14, 15, 16, 17, 18, 19, 20], 
	'metric': ['euclidean', 'minkowski'], 
}
random_search = GridSearchCV(knn, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
knnBestParams = random_search.best_params_
print("KNN Best Params: ")
print(knnBestParams)
print("")

#LogisticRegression
#ValueError: Logistic Regression supports only liblinear, newton-cg, lbfgs and sag solvers, got saga
#ValueError: Solver newton-cg supports only l2 penalties, got l1 penalty.
#ValueError: Solver liblinear does not support a multinomial backend.
#Testar sem liblinear e sem multinomial
#'multi_class': ('ovr', 'multinomial'),

#'C': [0.001, 0.01, 0.1, 0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10, 100, 1000
#'max_iter': (50, 100, 150, 200, 400),
param_grid = {'C': [1, 1.5, 1.8, 8],
				'tol': (1e-5, 1e-4, 1e-3, 1e-2),
				'solver': ('newton-cg', 'lbfgs', 'liblinear', 'sag'),
				'max_iter': (50, 100, 150, 200),
				'warm_start': (True, False)
}
grid_search = GridSearchCV(LogisticRegression(), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
grid_search.fit(data_train_total, target_train_total)
logisticRegressionBestParams = grid_search.best_params_
print("LogisticRegression Best Params: ")
print(logisticRegressionBestParams)
print("")


#SGDC
param_grid = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'penalty': ('l1', 'l2'),
	'loss': ('hinge', 'squared_hinge', 'perceptron', 'squared_loss', 'epsilon_insensitive'),
	'learning_rate': ('constant', 'optimal', 'invscaling'),
	'eta0': (1, 0.1, 0.05, 0.01, 0.001, 0.0001, 0.00001, 0.000001),
}

sgdc = SGDClassifier()

sgdc_grid = GridSearchCV(sgdc, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
sgdc_grid.fit(data_train_total, target_train_total)
sgdcBestParams = sgdc_grid.best_params_

print("SGDC Best Params: ")
print(sgdcBestParams)
print("")

#AdaBoost sem base_estimator
param_grid = {
	'n_estimators': (10, 20, 50, 200),
	'learning_rate': (0.005, 0.01, 0.1, 0.5),
	'random_state': (None, 0, 42)
}

adaBoost = AdaBoostClassifier()

ada_grid = GridSearchCV(adaBoost, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
ada_grid.fit(data_train_total, target_train_total)
adaBestParams = ada_grid.best_params_

print("AdaBoost Best Params: ")
print(adaBestParams)
print("")

#AdaBoost com base_estimator
param_grid = {
	'n_estimators': (16, 32, 50, 100, 150, 200, 250, 300, 400, 500),
	'learning_rate': (1, 1.5, 2, 4, 8, 16),
	'random_state': (None, 0, 42)
}

adaBoost_tree = AdaBoostClassifier(tree.DecisionTreeClassifier(**decisionTreeBestParams))

adaTree_grid = GridSearchCV(adaBoost_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
adaTree_grid.fit(data_train_total, target_train_total)
adaTreeBestParams = adaTree_grid.best_params_

print("AdaBoost - Decision Tree Best Params: ")
print(adaTreeBestParams)
print("")

#MultinomialNB
param_grid = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'fit_prior': (True, False)
}

multinomialNB = MultinomialNB()

multinomialNB_grid = GridSearchCV(multinomialNB, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
multinomialNB_grid.fit(data_train_total, target_train_total)
multinomialNBBestParams = multinomialNB_grid.best_params_

print("MultinomialNB Best Params: ")
print(multinomialNBBestParams)
print("")


#GradientBoostingClassifier
#'max_depth': (None, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30),
#'learning_rate': (0.0001, 0.001, 0.01, 0.1, 1, 1.5, 2, 4),
#'n_estimators': (5, 10, 50, 100, 150, 200, 300, 500),
#'warm_start': (False, True),
#'presort': (False, True, 'auto')
#'loss': ('deviance', 'exponencial'),

#'learning_rate': (0.001, 0.01, 0.1, 0.5),
#'n_estimators': (5, 20, 40, 50, 100),
#'max_depth': (None, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
param_grid = {
	'learning_rate': (0.01, 0.1, 0.5, 0.8, 1),
	'n_estimators': (10, 16, 20, 32, 40, 50, 80, 100), 
	'max_depth': (None, 1, 2, 3, 4, 5, 6, 7, 8, 9),
	'random_state': (None, 0, 42),
}

gradientBoosting = GradientBoostingClassifier()

gradientBoosting_grid = GridSearchCV(gradientBoosting, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
#TypeError: A sparse matrix was passed, but dense data is required. Use X.toarray() to convert to a dense numpy array.
gradientBoosting_grid.fit(data_train_total.toarray(), target_train_total)
gradientBoostingBestParams = gradientBoosting_grid.best_params_

print("Gradient Boosting Best Params: ")
print(gradientBoostingBestParams)
print("")

#Extra Trees

#'n_estimators':(10, 20, 40, 80, 100),
#'min_samples_split':range(1000,2100,200),
#'min_samples_leaf':range(30,71,10),
#'max_features':range(7,20,2)
#'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

params = {
			'criterion' : ["gini", "entropy"],
			'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20],
			'min_samples_split': (2, 5, 10, 50, 100, 500, 1000),
			'n_estimators':(10, 20, 40, 80, 100, 200, 500, 1000),
          }

extraTrees = ExtraTreesClassifier()
extraTrees_grid = GridSearchCV(extraTrees, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)
extraTrees_grid.fit(data_train_total, target_train_total)
extraTreesBestParams = extraTrees_grid.best_params_
print("Extra Trees Best Params: ")
print(extraTreesBestParams)
print("")

for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

	########################################################## DECISION TREE

	decision_tree = tree.DecisionTreeClassifier(**decisionTreeBestParams)
	decision_tree = decision_tree.fit(data_train, target_train)

	output = decision_tree.predict(data_test)
	accuracyDT.append(metrics.accuracy_score(target_test, output))


 	########################################################## KNN
	
	knn = neighbors.KNeighborsClassifier(**knnBestParams)
	knn = knn.fit(data_train.toarray(), target_train)

	#toarray para resolver: kd_tree does not work with sparse matrices. Densify the data, or set algorithm='brute'
	output = knn.predict(data_test.toarray())
	accuracyKnn.append(metrics.accuracy_score(target_test, output))

	########################################################## MultinomialNB

	multinomial = MultinomialNB(**multinomialNBBestParams)
	multinomial.fit(data_train, target_train)

	output = multinomial.predict(data_test)
	accuracyMultinomial.append(metrics.accuracy_score(target_test, output))

	
########################################################## AdaBoost

	#adaBoost = ABCBestParams
	adaBoost = AdaBoostClassifier(**adaBestParams)
	adaBoost.fit(data_train.toarray(), target_train)

	output = adaBoost.predict(data_test)
	accuracyAdaBoost.append(metrics.accuracy_score(target_test, output))

########################################################## AdaBoostTree

	#adaBoost = ABCBestParams
	adaBoostTree = AdaBoostClassifier(**adaTreeBestParams)
	adaBoostTree.fit(data_train.toarray(), target_train)
	print(adaBoostTree)

	output = adaBoostTree.predict(data_test)
	accuracyAdaBoostTree.append(metrics.accuracy_score(target_test, output))


########################################################## LogisticRegression

	logisticRegression = LogisticRegression(**logisticRegressionBestParams)
	logisticRegression.fit(data_train, target_train)

	output = logisticRegression.predict(data_test)
	accuracyLR.append(metrics.accuracy_score(target_test, output))


########################################################## GradientBoostingClassifier
	
	gradientBoosting = GradientBoostingClassifier(**gradientBoostingBestParams)
	gradientBoosting.fit(data_train, target_train)

	output = gradientBoosting.predict(data_test.toarray())
	accuracyGB.append(metrics.accuracy_score(target_test, output))
	

########################################################## SDGC 

	sgdc_clf = SGDClassifier(**sgdcBestParams)

	sgdc_clf.fit(data_train, target_train)

	output = sgdc_clf.predict(data_test)
	accuracySGDC.append(metrics.accuracy_score(target_test, output))


########################################################## ET

	et_clf = ExtraTreesClassifier(**extraTreesBestParams)

	et_clf.fit(data_train, target_train)

	output = et_clf.predict(data_test)
	accuracyET.append(metrics.accuracy_score(target_test, output))




##### DT AVG
avg = np.mean(accuracyDT)
std = np.std(accuracyDT)
top_avg = avg
top_avg_name = "DT"
print("DT: " +str(accuracyDT))
print("DT Media: " +str(avg))
print("")


##### KNN AVG
avg = np.mean(accuracyKnn)
std = np.std(accuracyKnn)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "KNN"

print("KNN: " +str(accuracyKnn))
print("KNN Media: " +str(avg))
print("")

##### Multinomial Naive Bayes AVG
avg = np.mean(accuracyMultinomial)
std = np.std(accuracyMultinomial)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Multinomial Naive Bayes"

print("Multinomial Naive Bayes: " +str(accuracyMultinomial))
print("Multinomial Naive Bayes Media: " +str(avg))
print("\n")


##### AdaBoost AVG
avg = np.mean(accuracyAdaBoost)
std = np.std(accuracyAdaBoost)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost"

print("AdaBoost: " +str(accuracyAdaBoost))
print("AdaBoost Media: " +str(avg))
print("\n")

##### AdaBoostTree AVG
avg = np.mean(accuracyAdaBoostTree)
std = np.std(accuracyAdaBoostTree)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost - Decision Tree"

print("AdaBoost Decisio Tree: " +str(accuracyAdaBoostTree))
print("AdaBoost Decision Tree Media: " +str(avg))
print("\n")

##### Logistic Regression AVG
avg = np.mean(accuracyLR)
std = np.std(accuracyLR)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LogisticRegression"

print("LogisticRegression: " +str(accuracyLR))
print("LogisticRegression Media: " +str(avg))
print("\n")

##### GradientBoosting AVG
avg = np.mean(accuracyGB)
std = np.std(accuracyGB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "GradientBoosting"

print("GradientBoosting: " +str(accuracyGB))
print("GradientBoosting Media: " +str(avg))
print("\n")

##### SGDC AVG
avg = np.mean(accuracySGDC)
std = np.std(accuracySGDC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SGDC"

print("SGDC: " +str(accuracySGDC))
print("SGDC Media: " +str(avg))
print("\n")

##### ET AVG
avg = np.mean(accuracyET)
std = np.std(accuracyET)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "ET"

print("ET: " +str(accuracyET))
print("ET Media: " +str(avg))
print("\n")
