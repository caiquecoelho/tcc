#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import pandas as pd
from sklearn.grid_search import GridSearchCV
import numpy as np
import scipy
import time
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn.svm import SVC
from sklearn import metrics
from sklearn import model_selection
from sklearn import neural_network
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn import tree
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import (ExtraTreesClassifier, RandomForestClassifier, 
                              AdaBoostClassifier, GradientBoostingClassifier)


t0 = time.time()

class EstimatorSelectionHelper:
    def __init__(self, models, params):
        if not set(models.keys()).issubset(set(params.keys())):
            missing_params = list(set(models.keys()) - set(params.keys()))
            raise ValueError("Some estimators are missing parameters: %s" % missing_params)
        self.models = models
        self.params = params
        self.keys = models.keys()
        self.grid_searches = {}
    
    def fit(self, X, y, cv=3, n_jobs=-1, verbose=1, scoring='accuracy', refit=False):
        for key in self.keys:
            print("Running GridSearchCV for %s." % key)
            model = self.models[key]
            params = self.params[key]
            gs = GridSearchCV(model, params, cv=cv, n_jobs=n_jobs, 
                              verbose=verbose, scoring=scoring, refit=refit)
            gs.fit(X,y)
            self.grid_searches[key] = gs    
    
    def score_summary(self, sort_by='mean_score'):
        def row(key, scores, params):
            d = {
                 'estimator': key,
                 'min_score': min(scores),
                 'max_score': max(scores),
                 'mean_score': np.mean(scores),
                 'std_score': np.std(scores),
            }
            return pd.Series(dict(params.items() + d.items()))
                      
        rows = [row(k, gsc.cv_validation_scores, gsc.parameters) 
                     for k in self.keys
                     for gsc in self.grid_searches[k].grid_scores_]
        df = pd.concat(rows, axis=1).T.sort_values([sort_by], ascending=False)
        
        columns = ['estimator', 'min_score', 'mean_score', 'max_score', 'std_score']
        columns = columns + [c for c in df.columns if c not in columns]
        
        return df[columns]

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa

#Database 3, 6 categorias, min: 10 palavras
text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database 3, 6 categorias, min: 15 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/15", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)



#count_vect = CountVectorizer(ngram_range=(1, 2))
count_vect = CountVectorizer(ngram_range=(1, 2), stop_words=stop_words)
X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


####### Parameters tuning

'''
#Bagging Classifier
param_grid = {
	'n_estimators': (16, 32, 100, 300, 500, 700, 1000),
    'max_samples' : [0.05, 0.1, 0.2, 0.5],
    'oob_score' : ['true', 'false']
}

bagging_clf = GridSearchCV(BaggingClassifier(DecisionTreeClassifier(),
	n_estimators = 100, max_features = 0.5),
	param_grid, n_jobs = -1, scoring = 'accuracy', verbose=1)

bagging_clf.fit(data_train_total, target_train_total)
baggingClassifierBestParams = bagging_clf.best_params_
print("Bagging Classifier Best Params: ")
print(baggingClassifierBestParams)
print("")

#Random Forest
random = RandomForestClassifier()

parameters = {'n_estimators': (16, 32, 100, 300, 500, 700, 1000),
					'criterion': ('gini', 'entropy'),
	 				'max_depth': (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
					'random_state': (0, 42)}

random_clf = GridSearchCV(random, parameters, n_jobs=-1, scoring = 'accuracy', verbose=1)

random_clf.fit(data_train_total, target_train_total)
randomForestBestParams = random_clf.best_params_
print("Random Forest Best Params: ")
print(randomForestBestParams)
print("")
'''

#DecisionTree
'''
params = {'criterion' : ["gini", "entropy"],
              'splitter' :   ["best", "random"],
              'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
              'min_samples_leaf':range(30,71,10),
              'min_samples_split':range(1000,2100,200),
              'max_features':range(7,20,2),
          }

decisionTreeClassifier = DecisionTreeClassifier()

decisionTreeClassifier_grid = GridSearchCV(decisionTreeClassifier, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

decisionTreeClassifier_grid.fit(data_train_total, target_train_total)
decisionTreeClassifierBestParams = decisionTreeClassifier_grid.best_params_

print("DecisionTree Best Params: ")
print(decisionTreeClassifierBestParams)
print("")
'''
#AdaBoost
'''
params = {'learning_rate' : (0.1, 0.05, 1),
          'n_estimators':(10, 20, 40, 80, 160, 300, 500, 700, 1000)
		}
adaBoost_clf = GridSearchCV(AdaBoostClassifier(base_estimator=DecisionTreeClassifier(**decisionTreeClassifierBestParams)), params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

adaBoost_clf.fit(data_train_total, target_train_total)
adaBoostBestParams = adaBoost_clf.best_params_
print("AdaBoost Best Params: ")
print(adaBoostBestParams)
print("")
'''
#Extra Trees

#'n_estimators':(10, 20, 40, 80, 100),
#'min_samples_split':range(1000,2100,200),
#'min_samples_leaf':range(30,71,10),
#'max_features':range(7,20,2)
#'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
'''
params = {
			'criterion' : ["gini", "entropy"],
			'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20],
			'min_samples_split': (2, 5, 10, 50, 100, 500, 1000),
			'n_estimators':(10, 20, 40, 80, 100, 200, 500, 1000),
          }

extraTrees = ExtraTreesClassifier()
extraTrees_grid = GridSearchCV(extraTrees, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)
extraTrees_grid.fit(data_train_total, target_train_total)
extraTreesBestParams = extraTrees_grid.best_params_
print("Extra Trees Best Params: ")
print(extraTreesBestParams)
print("")
'''


'''
models1 = { 
    'ExtraTreesClassifier': ExtraTreesClassifier(),
    'RandomForestClassifier': RandomForestClassifier(),
    'AdaBoostClassifier': AdaBoostClassifier(),
    'GradientBoostingClassifier': GradientBoostingClassifier(),
    'SVC': SVC()
}

params1 = { 
    'ExtraTreesClassifier': { 'n_estimators': [16, 32] },
    'RandomForestClassifier': { 'n_estimators': [16, 32] },
    'AdaBoostClassifier':  { 'n_estimators': [16, 32] },
    'GradientBoostingClassifier': { 'n_estimators': [16, 32], 'learning_rate': [0.8, 1.0] },
    'SVC': [
        {'kernel': ['linear'], 'C': [1, 10]},
        {'kernel': ['rbf'], 'C': [1, 10], 'gamma': [0.001, 0.0001]},
    ]
}


helper1 = EstimatorSelectionHelper(models1, params1)
helper1.fit(data_train_total.toarray(), target_train_total, scoring='accuracy', n_jobs=-1, verbose = 1)

print(helper1.score_summary(sort_by='min_score'))
'''

#GradientBoostingClassifier
'''
params = {'n_estimators':(10, 20, 40, 80, 160, 300),
			'criterion' : ["gini", "entropy"],
              'max_depth': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
              'min_samples_leaf':range(30,71,10),
              'min_samples_split':range(1000,2100,200),
              'max_features':range(7,20,2),
          }

gradientBoosting = GradientBoostingClassifier()
gradientBoosting_grid = GridSearchCV(gradientBoosting, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)
gradientBoosting_grid.fit(data_train_total, target_train_total)
gradientBoostingBestParams = gradientBoosting_grid.best_params_
'''

data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyAdaBoost = list()
accuracyOneVsRest = list()
accuracyOneVsOne = list()
accuracyBagging = list()
accuracyGB = list()
accuracyRF = list()
accuracyDT = list()
accuracyET = list()
accuracySVC = list()
#accuracySGDC = list()


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

	
########################################################## AdaBoost
	'''
	#adaBoost = ABCBestParams
	adaBoost = AdaBoostClassifier(**adaBoostBestParams)

	adaBoost.fit(data_train, target_train)

	output = adaBoost.predict(data_test)
	accuracyAdaBoost.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsRest

	oneVsRest = OneVsRestClassifier(LinearSVC())

	oneVsRest.fit(data_train, target_train)

	output = oneVsRest.predict(data_test)
	accuracyOneVsRest.append(metrics.accuracy_score(target_test, output))


########################################################## OneVsOne

	oneVsOne = OneVsOneClassifier(LinearSVC())

	oneVsOne.fit(data_train, target_train)

	output = oneVsOne.predict(data_test)
	accuracyOneVsOne.append(metrics.accuracy_score(target_test, output))
	'''

########################################################## BaggingClassifier
	'''
	bagging_clf = BaggingClassifier(**baggingClassifierBestParams)

	bagging_clf.fit(data_train, target_train)

	output = bagging_clf.predict(data_test)
	accuracyBagging.append(metrics.accuracy_score(target_test, output))
	'''

########################################################## GradientBoostingClassifier
	'''
	gradientBoosting = GradientBoostingClassifier(**gradientBoostingBestParams)

	gradientBoosting.fit(data_train, target_train)

	output = gradientBoosting.predict(data_test.toarray())
	accuracyGB.append(metrics.accuracy_score(target_test, output))

########################################################## Decision Tree
	
	decisionTree = DecisionTreeClassifier(**decisionTreeClassifierBestParams)

	decisionTree.fit(data_train, target_train)

	output = decisionTree.predict(data_test.toarray())
	accuracyDT.append(metrics.accuracy_score(target_test, output))
	'''
	
########################################################## Extra Trees
	
	#extraTrees_clf = ExtraTreesClassifier(**extraTreesBestParams)
	extraTrees_clf = ExtraTreesClassifier(n_estimators = 500)
	print(extraTrees_clf)

	extraTrees_clf.fit(data_train, target_train)

	output = extraTrees_clf.predict(data_test.toarray())
	accuracyET.append(metrics.accuracy_score(target_test, output))
	'''

	'''
########################################################## RandomForestClassifier
	'''
	random_clf = RandomForestClassifier(**randomForestBestParams)

	random_clf.fit(data_train, target_train)

	output = random_clf.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))
	'''
########################################################## SDGC
	
	'''
	sgdc = SGDClassifier()

	parameters = {'loss': ('hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron', 'squared_loss', 'huber', 
							'epsilon_insensitive', 'squared_epsilon_insensitive'),
					'penalty': ('l2', 'l1', 'elasticnet'),
					'learning_rate': ('constant', 'optimal', 'invscaling'),
					'n_iter': (50, 100, 200, 500, 1000),
					'alpha': [10 ** x for x in range(-6, -1)],
					'eta0' : (0.01, 0.1, 1, 10),
					'l1_ratio': [0, 0.05, 0.1, 0.2, 0.5, 0.8, 0.9, 0.95, 1]}

	sgdc_clf = GridSearchCV(sgdc, parameters, n_jobs=-1)

	sgdc_clf.fit(data_train, target_train)

	output = sgdc_clf.predict(data_test)
	accuracySGDC.append(metrics.accuracy_score(target_test, output))
	'''

##### AdaBoost AVG
'''
avg = np.mean(accuracyAdaBoost)
top_avg = avg
std = np.std(accuracyAdaBoost)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "AdaBoost"

print("AdaBoost: " +str(accuracyAdaBoost))
print("AdaBoost Media: " +str(avg))
print("\n")


##### OneVsRest AVG
avg = np.mean(accuracyOneVsRest)
std = np.std(accuracyOneVsRest)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsRest"

print("OneVsRest: " +str(accuracyOneVsRest))
print("OneVsRest Media: " +str(avg))
print("\n")

##### OneVsOne AVG
avg = np.mean(accuracyOneVsOne)
std = np.std(accuracyOneVsOne)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "OneVsOne"

print("OneVsOne: " +str(accuracyOneVsOne))
print("OneVsOne Media: " +str(avg))
print("\n")
'''
##### Bagging AVG
'''
avg = np.mean(accuracyBagging)
std = np.std(accuracyBagging)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "BaggingClassifier"

print("BaggingClassifier: " +str(accuracyBagging))
print("BaggingClassifier Media: " +str(avg))
print("\n")
'''
'''
##### GradientBoosting AVG
avg = np.mean(accuracyGB)
std = np.std(accuracyGB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "GradientBoosting"

print("GradientBoosting: " +str(accuracyGB))
print("GradientBoosting Media: " +str(avg))
print("\n")

##### Decision Tree AVG
avg = np.mean(accuracyDT)
std = np.std(accuracyDT)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Decision Tree"

print("Decision Tree: " +str(accuracyDT))
print("Decision Tree Media: " +str(avg))
print("\n")
'''
##### Extra Trees AVG
avg = np.mean(accuracyET)
std = np.std(accuracyET)

#if avg > top_avg:
	#top_avg = avg
	#top_avg_name = "Extra Trees"

print("Extra Trees: " +str(accuracyET))
print("Extra Trees Media: " +str(avg))
print("\n")
'''

'''

##### RandomForest AVG
'''
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "RandomForest"

print("RandomForest: " +str(accuracyRF))
print("RandomForest Media: " +str(avg))
print("\n")
'''
##### SGDC AVG
'''Tarefas/Postagens
avg = np.mean(accuracySGDC)
std = np.std(accuracySGDC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SGDC"

print("SGDC: " +str(accuracySGDC))
print("SGDC Media: " +str(avg))
print("\n")
'''

'''
print("\n")

print("TOP AVG IS " + top_avg_name +str(": ") +str(top_avg))

print("\n\n")
'''


elapsed_time_seconds = time.time() - t0

elapsed_time_minutes = int(elapsed_time_seconds) / 60

print("Tempo total executando: %.2fm" % (elapsed_time_minutes))


