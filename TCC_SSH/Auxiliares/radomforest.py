#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
import csv
from sklearn.pipeline import Pipeline

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']
#categories = ['1', '3', '6']

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "-", ",", 
"*", "(", ")", "!", ".", "+", "_", "&", "%", "$", "#", "@", ":", ";", "<", ">", "[", "]", "{", "}", "=", "/"}

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 10 palavras
#text = load_files(container_path = "/exp/caique/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

text = load_files(container_path = "/home/minergate/TCC_SSH/Estimativas/database_3/6_Categorias/10", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


#text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/Estimativas/database_3/6_categorias/10", 
#	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)


count_vect = CountVectorizer()


X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target

kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)
accuracyRF = list()


#Random Forest
#UserWarning: Some inputs do not have OOB scores. This probably means too few trees were used to compute any reliable oob estimates.
#warn("Some inputs do not have OOB scores. ")
#'oob_score': (False, True)


balanced_manual = {0:.08, 1:.35, 2:.04, 3:.32, 4:.07, 5:.11}
#'max_features': (0.1567104988523027, 'sqrt', 'auto'),

#'random_state': (3, 50, 42)

#ValueError: Out of bag estimation only available if bootstrap=True

param_grid_alt = {
	
			'criterion': ('entropy', 'gini'),
      		'min_samples_split': (3, 4, 2), 
      		'min_weight_fraction_leaf': (0.003, 0.002),
      		'max_leaf_nodes': (90, 120, 100),
            'max_depth': (5, 2, None), 
            'min_samples_leaf': (3, 1, 2),
            'n_estimators': (10, 20, 1000),
            'oob_score': (False, True)
}


random = RandomForestClassifier()

random_grid = GridSearchCV(random, param_grid_alt,  cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_grid.fit(data_train_total, target_train_total)
randomBestParams = random_grid.best_params_

print("Random Forest Best Params: ")
print(randomBestParams)
print("")
	

for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]


########################################################## RandomForestClassifier

	random_clf = RandomForestClassifier(bootstrap= False, min_samples_leaf= 4, n_estimators=[] 1092, 
		random_state= 3, criterion= 'entropy', max_features= 0.1567104988523027, max_depth= 3)
	random_clf.fit(data_train, target_train)

	output = random_clf.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))



##### RandomForest AVG
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

print("RandomForest: " +str(accuracyRF))
print("RandomForest Media: " +str(avg))
print("\n")
