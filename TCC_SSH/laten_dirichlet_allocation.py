#!-*- coding: utf8 -*-

from __future__ import print_function
from sklearn.datasets import load_files
from sklearn import metrics
from sklearn import preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation


def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += ", ".join([feature_names[i]
                             for i in topic.argsort()[:-n_top_words - 1:-1]])
        print(message)
    print()


stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai",
"vou", "tão", "alguma", "interesse", "ter", "caso", "abaixo", "animais", "ainda", "outras", "etc.", "em", "piloto", "corrida", "a", "b", "c", 
"d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}

#Possibilidades de estimativa
categories = ['cluster_0']

#Extracao de descricoes de tarefa
#Database 3, 6 categorias, min: 15 palavras
#average_cosine, average_euclidean, complete_cosine, complete_euclidean, ward_euclidean

#/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/soutputs_silhouette_v2/ilhouette_manual/semLSA_comStemmer/10_clusters
#/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/outputs_silhouette_v2/silhouette_tfidf/lsa700_semIDF_stemmer/10_clusters
#/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/outputs_silhouette_v2/silhouette_manual/comLSA1500_comStemmer/10_clusters
#/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/outputs_silhouette_v2/silhouette_tfidf/lsa1198_semIDF_stemmer/10_clusters
#/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/outputs_silhouette_v2/silhouette_manual/comLSA2137_comStemmer/10_clusters
text = load_files(container_path = "/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/outputs_silhouette_v2/silhouette_tfidf/lsa700_semIDF_stemmer/10_clusters", 
	load_content = True, categories = categories, shuffle = True, encoding = None, random_state = 0)

n_samples = len(text.data)
data_samples = text.data[:n_samples]

#print(text.data)


tf_vectorizer = CountVectorizer(stop_words=stop_words)
tf = tf_vectorizer.fit_transform(data_samples)

max_features = tf.shape[1]

n_features = max_features #construa um vocabulario que considere apenas as principais max_features ordenadas pela frequencia do termo em todo o corpus
n_components = 6 #numero de topicos de saida
n_top_words = 20  #numero de palavras em cada topico de saída


########################################### sklearn.decomposition.LatentDirichletAllocation

# Fit the LDA model
print("Fitting LDA models with tf features, " "n_samples=%d and n_features=%d..."
	% (n_samples, n_features))

lda = LatentDirichletAllocation(n_components = n_components, max_iter=5, learning_method='online', learning_offset=50., random_state=0)

lda.fit(tf)

print("\nTopics in LDA model:")
tf_feature_names = tf_vectorizer.get_feature_names()
print_top_words(lda, tf_feature_names, n_top_words)