#!/usr/bin/env python
#!-*- coding: utf8 -*-

from __future__ import print_function

from sklearn.datasets import fetch_20newsgroups
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import load_files
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics import silhouette_samples, silhouette_score
from time import time
from optparse import OptionParser
from sklearn.preprocessing import Normalizer

import sys
import logging
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import nltk
import nltk.stem
import numpy as np
import pandas as pd
import numpy as np

dictionary_stemmer = {}

def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int",
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--use-stemmer",
              action="store_true", default=False,
              help="Stemming all words with stemmer")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")
op.add_option("--print-distribuicao-clusters",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")
op.add_option("--terms-per-cluster",
              action="store_true", default=False,
              help="Print distribution of tasks in each cluster.")

print(__doc__)
op.print_help()

argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)



def distribuicao_de_dados_no_dataset():
  print("\nDISTRIBUIÇAO DOS DADOS NO DATASET")

  total = 0
  total_classe_majoritaria = 0
  classe_majoritaria = categories[0]

  all_estimativas = []
  for t in text.target:
    all_estimativas.append(int(text.target_names[t]))
	
  for categorie in categories:
    for t in text.target:
      if(text.target_names[t] == categorie):
        total += 1 

    porcentagem = 100.0 * total/tam
    print("Total de tarefas com estimativa "+str(categorie)+": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

    if(total > total_classe_majoritaria):
      classe_majoritaria = categorie
      total_classe_majoritaria = total

    total = 0

  total_de_elementos = len(target_train_total)
  taxa_de_acerto_algoritmo_burro = 100.0 * total_classe_majoritaria/total_de_elementos

  print("\nClasse Majoritária é: " +str(classe_majoritaria))
  print("Se chutarmos todas as predicoes com " +str(classe_majoritaria) +" teremos um acerto de:")
  print(str(taxa_de_acerto_algoritmo_burro) + "%")
  all_estimativas = np.array(all_estimativas)
  desvio_dataset = all_estimativas.std()
  media_dataset = all_estimativas.mean()
  print("\nDesvio padrão do dataset inteiro: " +str(desvio_dataset))
  print("Média do dataset inteiro: " + str(media_dataset))
  print("\n")


def distribuicao_dos_dados_nos_clusters():
	print("\nDISTRIBUIÇAO DOS DADOS NOS CLUSTERS")

	total = 0

	for cluster in range(0, n_clusters):
		for index in range(0, tam):
			if(cluster_labels[index] == cluster):
				total+=1

		porcentagem = 100.0 * total/tam
		print("Total de tarefas no cluster " +str(cluster)+ ": " +str(total) + " tarefas, em porcentagem temos: {0:.2f}".format(porcentagem)  + "%")

		total = 0

def salvar_output_clusters():
	file = open(nome_arquivo, "w")
	dado = str(text.data[index])
	file.write(dado) 
	file.close()


def vetorizar_texto(texto, tradutor):
	vetor = [0] * len(tradutor)

	for palavra in texto:
		if palavra in tradutor:
			posicao = tradutor[palavra]
			vetor[posicao] += 1

	return vetor

def removeStopWords(texto):
	frases = []
	#print(stop_words)
	for palavras in texto:
		semstop = [p for p in palavras.split() if p not in stop_words]
		frases.append(semstop)

	return frases


def aplicaStemmer(texto):
  stemmer = nltk.stem.RSLPStemmer()
  frasesStemming = []
  for palavras in texto:
    #print(palavras)
    comStemming = [str(stemmer.stem(p)) for p in palavras if p not in stop_words]
    frasesStemming.append(comStemming)

    for p in palavras:
      word_stemmed = stemmer.stem(p)

      existe = False
      if(word_stemmed in dictionary_stemmer):
        existe = True      

      if(not existe):
        dictionary_stemmer[word_stemmed] = [[p,1]]

      else:
        lists_word_stemmed_from_dictionary = dictionary_stemmer[word_stemmed]
        achou = False
        for i in range(len(lists_word_stemmed_from_dictionary)):
          list_word = lists_word_stemmed_from_dictionary[i]

          if(list_word[0] == p):
            list_word[1] = list_word[1] + 1
            achou = True
            lists_word_stemmed_from_dictionary[i] == list_word
            dictionary_stemmer[word_stemmed] = lists_word_stemmed_from_dictionary

        if(not achou):
          lists_word_stemmed_from_dictionary.append([p, 1])
          dictionary_stemmer[word_stemmed] = lists_word_stemmed_from_dictionary

  return frasesStemming

def buscaPalavras(frases):
	todasPalavras = []
	for palavras in frases:
		todasPalavras.extend(palavras)

	return todasPalavras

def buscaFrequencia(palavras):
	palavras = nltk.FreqDist(palavras)
	return palavras

def buscaPalavrasUnicas(frequencia):
	freq = frequencia.keys()
	return freq

def extratorPalavras(documento):
	doc = set(documento)
	caracteristicas = {}
	for palavras in palavrasUnicas:
		caracteristicas['%s' %palavras] = (palavras in doc)

	return caracteristicas


########################################################################################################
'''
if len (sys.argv)!=2:
	print("Use:",sys.argv[0],"n_clusters")
	exit()


n_clusters = int(sys.argv[1])
'''

stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai",
"vou", "tão", "alguma", "interesse", "ter", "caso", "abaixo", "animais", "ainda", "outras", "etc.", "em", "piloto", "corrida", "a", "b", "c", 
"d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


#Extracao de palavras e vetorizacao manual
#classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database2.csv")
classificacoes = pd.read_csv("/home/caique_coelho/MEGA/UFMS/TCC/TCC_SSH/separando_tarefas/database_final.csv")
nomesTarefas = classificacoes['Tarefa']
tarefas = classificacoes['Tarefa'] +" "+ classificacoes['Descricao']
tipo = classificacoes['Tipo']
descricoesTarefas = classificacoes['Descricao']
marcacoes_all = classificacoes['Estimativa']
tam = len(nomesTarefas)

#print(tarefas[1166])

stop_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
caracteres = ["~", "^", "=", "?","!",'"', "+", ",", ".", "-", "'", "@", "#", "$", "%", "&", "*", "(", ")", "_", "<", ">", "{", "}", "[", "]", ";", ":", "\n", '\t', '/', '...']

textos = []
posicoes = []
for i in range(tam):
  if(str(tipo[i]).lower() != 'api'):
    posicoes.append(i)
    texto = str(tarefas[i])
    texto = texto.lower()

    for number in stop_numbers:
      texto = texto.replace(number, "")
    for caractere in caracteres:
      texto = texto.replace(caractere, "")

    textos.append(texto)

#print(textos)
marcacoes = []
for i in posicoes:
  marcacoes.append(marcacoes_all[i])

textosQuebrados = tarefas.str.lower().str.split(' ')


for number in stop_numbers:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(number, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
		while(indice < tamTexto):
			texto.pop(indice)
			indice +=1
			tamTexto -= 1
		indice = 0


for caractere in caracteres:
	for texto in textosQuebrados:
		indice = 0
		tamTexto = len(texto)
		for palavra in texto:
			palavraEditada = palavra.replace(caractere, "")
			if(palavraEditada != ''):
				texto[indice] = palavraEditada
				indice += 1
			elif(palavra == caractere):
				texto.pop(indice)
				indice +=1
				tamTexto -=1
		while(indice < tamTexto):
			texto.pop(indice)
			#print(texto)
			indice +=1
			tamTexto -= 1
		indice = 0


#print(textosQuebrados[0])
#for texto in textosQuebrados:
#	print(texto)

frasesStopWord = removeStopWords(textos)

if opts.use_stemmer: 
	print("\nAplying stemming...")
	palavrasStemmed = aplicaStemmer(frasesStopWord)
else:
	palavrasStemmed = frasesStopWord

#print(type(palavrasStemmed))
#print(palavrasStemmed)

#distribuicao_de_dados_no_dataset()

#set() para trasnformar a variavel dicionario em um conjunto, ou seja, um "array" que não contem elementos iguais
dicionario = set()
i = 0
for lista in palavrasStemmed:
	#print(lista)
	#print(i)
	dicionario.update(lista)
	i+=1

totalDePalavras = len(dicionario)
#zip combina em tuplas o que eu quero do lado esquerdo e do lado direito
tuplas = zip(dicionario, range(totalDePalavras))

tradutor = {palavra:indice for palavra, indice in tuplas}

#print totalDePalavras

#print vetorizar_texto(textosQuebrados[0], tradutor)
vetoresDeTexto = [vetorizar_texto(texto, tradutor) for texto in palavrasStemmed]


#print(tradutor)
print(len(dicionario))

#Verificando se a vetorizacao esta correta
'''
print(textosQuebrados[0])


indice = 0
indiceTradudor = 0
tamTradutor = len(tradutor)
#print(tradutor['chat'])
for existe in vetoresDeTexto[0]:
	if(existe != 0):
		for palavra in tradutor:
			#print(tradutor[palavra])
			if(tradutor[palavra] == indice):
				break
			indiceTradudor += 1 
		print(indiceTradudor)
		indiceTradudor = 0
	indice+=1


print(tradutor)
'''

normalizer = Normalizer(copy=False)

X = np.array(vetoresDeTexto)
X = normalizer.fit_transform(X)
y = marcacoes


#distribuicao_de_dados_no_dataset()

tamSamples = len(y)
tamFeatures = len(dicionario)
print("\nn_samples: {}, n_features: {}".format(tamSamples, tamFeatures))

if opts.n_components:
    print("\nPerforming dimensionality reduction using LSA...")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)

    #print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
   # print("Explained variance of the SVD step: {}%".format(
   #     int(explained_variance * 100)))

print()
range_n_clusters = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
#true_k = np.unique(labels).shape[0]
for n_clusters in range_n_clusters:
  # Create a subplot with 1 row and 2 columns
    #fig, (ax1, ax2) = plt.subplots(1, 2)
    #Para pltar apenas o score da silhoette em cada cluster
    fig, (ax1) = plt.subplots(1)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value
    # seed of 10 for reproducibility.

	#true_k = 2
    clusterer = KMeans(n_clusters=n_clusters, init='k-means++', random_state =10, 
                  verbose=opts.verbose)

    t0 = time()
    cluster_labels = clusterer.fit_predict(X)


    if(n_clusters == 10):
      for i in range(0, n_clusters):
        estimativas_no_cluster = []
        for j in range(len(cluster_labels)):
          if(cluster_labels[j] == i):
            estimativas_no_cluster.append(int(marcacoes[j]))

        estimativas_no_cluster = np.array(estimativas_no_cluster)
        print("\nCluster " +str(i))
        #print(estimativas_no_cluster)
        desvio = estimativas_no_cluster.std()
        media = estimativas_no_cluster.mean()
        print(estimativas_no_cluster)
        print("Desvio Padrão do Cluster " +str(i) + " é de: " +str(desvio))
        print("Média das tarefas no Cluster " +str(i) + " é de: " +str(media))
        print("")

    #print("done in %0.3fs" % (time() - t0))
    #print()

    #print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, km.labels_))
    #print("Completeness: %0.3f" % metrics.completeness_score(labels, km.labels_))
    #print("V-measure: %0.3f" % metrics.v_measure_score(labels, km.labels_))
    #print("Adjusted Rand-Index: %.3f"
    #      % metrics.adjusted_rand_score(labels, km.labels_))
    #print("Silhouette Coefficient: %0.3f"
    #      % metrics.silhouette_score(X, km.labels_, sample_size=1000))

    #print()

    if opts.terms_per_cluster:

      if(n_clusters == 10):
        print("\nTop terms per cluster with " +str(n_clusters) + " clusters:")

        if opts.n_components:
            original_space_centroids = svd.inverse_transform(clusterer.cluster_centers_)
            order_centroids = original_space_centroids.argsort()[:, ::-1]
        else:
            order_centroids = clusterer.cluster_centers_.argsort()[:, ::-1]

        terms = list(dicionario)

        for i in range(n_clusters):
            print("Cluster %d:" % i, end='')
            for ind in order_centroids[i, :10]:
              if opts.use_stemmer:
                lists_word_stemmed_from_dictionary = dictionary_stemmer[terms[ind]]

                word_maior = ''
                maior = 0
                
                for list_word in lists_word_stemmed_from_dictionary:
                  word = list_word[0]
                  vezes = list_word[1]
                  if(vezes > maior):
                    word_maior = word
                    maior = vezes

                print(' %s' % word_maior, end='')
              else:
                print(' %s' % terms[ind], end='')
            print()
        print()


    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
     # clusters
    silhouette_avg = silhouette_score(X, cluster_labels)
    print("For n_clusters =", n_clusters,
            "The average silhouette_score is :", silhouette_avg)

    if(opts.print_distribuicao_clusters):
      tam = len(y)
      distribuicao_dos_dados_nos_clusters()
      print("\n")
    

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    
    #Salva o output da clusterização com 10 clusters
    '''
    if(n_clusters == 10):
      for index in range(0, len(tarefas)):
        #print(cluster_labels)
        nome_arquivo = "cluster_" +str(cluster_labels[index]) +"_tarefa_"+str(index)
        file = open(nome_arquivo, "w")
        texto = ""
        for palavra in textosQuebrados[index]:
        	texto = texto + str(palavra) + " "
        dado = str(texto)
        file.write(dado) 
        file.close()
    '''
    

    y_lower = 10
    for i in range(n_clusters):
      # Aggregate the silhouette scores for samples belonging to
      # cluster i, and sort them
      ith_cluster_silhouette_values = \
      sample_silhouette_values[cluster_labels == i]

      ith_cluster_silhouette_values.sort()

      size_cluster_i = ith_cluster_silhouette_values.shape[0]
      y_upper = y_lower + size_cluster_i

      color = cm.spectral(float(i) / n_clusters)
      ax1.fill_betweenx(np.arange(y_lower, y_upper),
                        0, ith_cluster_silhouette_values,
                        facecolor=color, edgecolor=color, alpha=0.7)

      # Label the silhouette plots with their cluster numbers at the middle
      ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

      # Compute the new y_lower for next plot
      y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])


    # 2nd Plot showing the actual clusters formed
    #Para plotar o grafico de visualização dos dados nos cluters
    '''
    colors = cm.spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                  c=colors, edgecolor='k')

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                  c="white", alpha=1, s=200, edgecolor='k')

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                      s=50, edgecolor='k')

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")
    '''

    plt.suptitle(("Silhouette analysis for " +str(type) +" clustering on sample data "
                    "with n_clusters = %d" % n_clusters),
                   fontsize=14, fontweight='bold')

    #plt.show()
    name_image = "Silhouette_for_" +str(n_clusters) + "_clusters.png" 
    plt.savefig(name_image)