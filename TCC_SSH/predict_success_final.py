#!-*- coding: utf8 -*-
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import scipy
import time
from sklearn import model_selection
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.linear_model import RidgeClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn import svm
from sklearn import tree
from sklearn import neighbors
from sklearn import linear_model
from sklearn import naive_bayes
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn import neural_network
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import BaggingClassifier
from sklearn.linear_model import SGDClassifier

t0 = time.time()

#Possibilidades de estimativa
categories = ['1', '2', '3', '4', '6', '8']


stop_words = {"de","a","o","que","e","do","da","em","um","para","é","com","não","uma","os","no","se","na","por","mais","as","dos",
"como","mas","foi","ao","ele","das","tem","à","seu","sua","ou","ser","quando","muito","há","nos","já","está","eu","também","só","pelo",
"pela","até","isso","ela","entre","era","depois","sem","mesmo","aos","ter","seus","quem","nas","me","esse","eles","estão","você","tinha",
"foram","essa","num","nem","suas","meu","às","minha","têm","numa","pelos","elas","havia","seja","qual","será","nós","tenho","lhe","deles",
"essas","esses","pelas","este","fosse","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa",
"nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos",
"estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam",
"estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram",
"houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá",
"houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","era","éramos","eram","fui","foi","fomos","foram","fora",
"fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","serei","será","seremos","serão","seria","seríamos",
"seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos",
"tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam", "vai"}


n_features = 1000 #construa um vocabulario que considere apenas as principais max_features ordenadas pela frequencia do termo em todo o corpus

#Database Final com tarefas de todos os tipos, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/tudo", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

'''
#Database Final com tarefas de todos os tipos, sem_api, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/tudo", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, min: 10 palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/10_palavras/ios", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#######################################################################################################

#Database Final com tarefas de todos os tipos, sem limite de palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas de todos os tipos, sem limite de palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/tudo", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)

#Database Final com tarefas apenas de iOS, sem limite de palavras
text = load_files(container_path = "/home/minergate/TCC_SSH/separando_tarefas/sem_limite_palavras/ios", 
	categories = categories, load_content = True, shuffle = True, encoding = None, random_state = 0)
'''



count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(text.data)
X_train_counts.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape


data_train_total = X_train_tfidf
target_train_total = text.target


kf = model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=None)

#Colocar ONEvsONE e ONEvsRest

accuracyRidge = list()
accuracyLinearSVC = list()
accuracySVC = list()
accuracyNB = list()
accuracyET = list()
accuracyMLP = list()
accuracyBG = list()
accuracyBGDT = list()
accuracyKnn = list()
accuracyLR = list()
accuracyRF = list()
accuracySGDC = list()
accuracyPerceptron = list()
accuracyOnevsOneRidge = list()
accuracyOnevsOneLinearSVC = list()
accuracyOnevsOneSVC = list()
accuracyOnevsOneET = list()
accuracyOneVSRestRidge = list()
accuracyOneVSRestLinearSVC = list()
accuracyOnevsOneRF = list()
accuracyOnevsOneLR = list()
accuracyOnevsOneNB = list()
accuracyOnevsOneKNN = list()
accuracyOnevsOneMLP = list()
accuracyOnevsOneBG = list()
accuracyOnevsOneBGDT = list()
accuracyOnevsOnePerceptron = list()
accuracyOnevsOneSGDC = list()

####### Parameters tuning
'''

# DECISION TREE
decision_tree = tree.DecisionTreeClassifier()
param_grid = {'max_depth': [None, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
'criterion': ('gini', 'entropy'),
'random_state': (None, 0, 42)
}
random_search = GridSearchCV(decision_tree, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
decisionTreeBestParams = random_search.best_params_
print("Decision Tree Best Params: ")
print(decisionTreeBestParams)
print("")


# KNN
knn = neighbors.KNeighborsClassifier()
param_grid = {'n_neighbors': [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25], 
	'algorithm': ['auto', 'brute'],
	'metric': ['euclidean', 'minkowski'], 
	'weights': ['uniform', 'distance']
}
random_search = GridSearchCV(knn, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
random_search.fit(data_train_total, target_train_total)
knnBestParams = random_search.best_params_
print("KNN Best Params: ")
print(knnBestParams)
print("")

#LogisticRegression
param_grid = {'C': [0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10],
				'random_state': (None, 0, 42),
				'tol': (1e-5, 1e-4, 1e-3, 1e-2),
				'fit_intercept': (True, False),
				'class_weight': (None, 'balanced'),
				'solver': ('newton-cg', 'lbfgs', 'liblinear', 'sag'),
				'max_iter': (50, 100, 150, 200),
				'warm_start': (True, False)
}
grid_search = GridSearchCV(LogisticRegression(), param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
grid_search.fit(data_train_total, target_train_total)
logisticRegressionBestParams = grid_search.best_params_
print("LogisticRegression Best Params: ")
print(logisticRegressionBestParams)
print("")


#SGDC
param_grid = {
	'alpha': [10 ** x for x in range(-6, 1)], 
	'fit_intercept': (True, False),
	'penalty': ('l1', 'l2'),
	'loss': ('hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron', 'squared_loss', 'huber', 'epsilon_insensitive', 'squared_epsilon_insensitive'),
	'learning_rate': ('constant', 'optimal', 'invscaling'),
	'eta0': (1, 0.1, 0.05, 0.01, 0.001, 0.0001, 0.00001, 0.000001),
	'random_state': (None, 0, 42)
}

sgdc = SGDClassifier()

sgdc_grid = GridSearchCV(sgdc, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
sgdc_grid.fit(data_train_total, target_train_total)
sgdcBestParams = sgdc_grid.best_params_

print("SGDC Best Params: ")
print(sgdcBestParams)

#LinearSVC
params = {
	'loss': ('squared_hinge', 'hinge'),
	'fit_intercept': (False, True),
	'tol': (1e-1, 1e-2, 1e-3, 1e-4, 1e-5),
	'C': [0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2,5, 3, 5, 8, 10],
	'class_weight': ('balanced', None),
	'max_iter': (100, 200, 500, 800, 1000, 1500,  2000),
	'multi_class': ('ovr', 'crammer_singer')
}

linearSVC = LinearSVC(class_weight='auto')

linearSVC_grid = GridSearchCV(linearSVC, params, scoring = 'accuracy', verbose = 1, n_jobs=-1)

linearSVC_grid.fit(data_train_total, target_train_total)
linearSVCBestParams = linearSVC_grid.best_params_

print("LinearSVC Best Params: ")
print(linearSVCBestParams)
print("")


#MultinomialNB

param_grid = {
	'alpha': [10 ** x for x in range(-6, 1)],
	'fit_prior': (True, False)
}

multinomialNB = MultinomialNB()

multinomialNB_grid = GridSearchCV(multinomialNB, param_grid, cv = 3, scoring = 'accuracy', verbose = 1, n_jobs=-1)
multinomialNB_grid.fit(data_train_total, target_train_total)
multinomialNBBestParams = multinomialNB_grid.best_params_

print("MultinomialNB Best Params: ")
print(multinomialNBBestParams)
print("")
'''


for train, test in kf.split(data_train_total, target_train_total):
	data_train, target_train = data_train_total[train], target_train_total[train]
	data_test, target_test   = data_train_total[test], target_train_total[test]
	

########################################################## Ridge

	print("Ridge")

	ridge = RidgeClassifier(normalize= False, alpha= 0.1, solver= 'cholesky', tol= 0.001, fit_intercept= True)

	ridge.fit(data_train.toarray(), target_train)

	output = ridge.predict(data_test)
	accuracyRidge.append(metrics.accuracy_score(target_test, output))

########################################################## SVC

	print("SVC")
	
	svc_clf = SVC(kernel= 'linear', C= 1, max_iter= -1, decision_function_shape= 'ovo', tol= 1.2, gamma= 'auto')

	svc_clf.fit(data_train, target_train)

	output = svc_clf.predict(data_test.toarray())
	accuracySVC.append(metrics.accuracy_score(target_test, output))

########################################################## Linear SVC -> Verificar melhor hyper para chumbar
	
	print("Linear SVC")

	linearSvc_clf = LinearSVC(loss= 'hinge', C= 0.8, fit_intercept= False, max_iter= 100, multi_class= 'crammer_singer', tol= 0.1, class_weight= None)

	linearSvc_clf.fit(data_train, target_train)

	output = linearSvc_clf.predict(data_test.toarray())
	accuracyLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## Extra Trees

	print("Extra Trees")
	
	et_clf = ExtraTreesClassifier(max_features= 'auto', min_samples_split= 3, n_estimators= 2000, max_depth= None)

	et_clf.fit(data_train, target_train)

	output = et_clf.predict(data_test.toarray())
	accuracyET.append(metrics.accuracy_score(target_test, output))

########################################################## Bagging

	print("Bagging")
	
	baggingTree_clf = BaggingClassifier(max_features= 0.035, max_samples= 0.9, random_state= 0, n_estimators= 500, bootstrap_features= True)
	#baggingTree_clf = BaggingClassifier()

	baggingTree_clf.fit(data_train, target_train)

	output = baggingTree_clf.predict(data_test)
	accuracyBG.append(metrics.accuracy_score(target_test, output))


########################################################## Bagging with Decision Tree

	print("Bagging with Decision Tree")
	
	baggingTree_clf = BaggingClassifier(base_estimator = tree.DecisionTreeClassifier(random_state= 42, criterion= 'gini', max_depth= 5), max_features= 0.47, 
		max_samples= 0.98, oob_score= True, random_state= 3, n_estimators= 50)
	#baggingTree_clf = BaggingClassifier()

	baggingTree_clf.fit(data_train, target_train)

	output = baggingTree_clf.predict(data_test)
	accuracyBGDT.append(metrics.accuracy_score(target_test, output))

########################################################## MLP

	print("MLP")
	
	mlp = neural_network.MLPClassifier(solver= 'sgd', activation= 'identity', max_iter= 2000, alpha= 0.0015, 
		learning_rate= 'constant', hidden_layer_sizes= (800,))
	mlp.fit(data_train, target_train)

	output = mlp.predict(data_test)
	accuracyMLP.append(metrics.accuracy_score(target_test, output))

########################################################## Naive Bayes -> Verificar melhor hyper para chumbar

	print("Naive Bayes")

	multinomial = MultinomialNB(alpha= 0.1, fit_prior= True)
	multinomial.fit(data_train, target_train)

	output = multinomial.predict(data_test)
	accuracyNB.append(metrics.accuracy_score(target_test, output))


########################################################## KNN -> Verificar melhor hyper para chumbar

	print("Knn")
	
	knn = neighbors.KNeighborsClassifier(n_neighbors= 16, metric= 'euclidean', weights= 'distance', algorithm= 'auto')
	knn = knn.fit(data_train.toarray(), target_train)

	#toarray para resolver: kd_tree does not work with sparse matrices. Densify the data, or set algorithm='brute'
	output = knn.predict(data_test.toarray())
	accuracyKnn.append(metrics.accuracy_score(target_test, output))

########################################################## Perceptron

	print("Perceptron")
	
	perceptron = Perceptron(penalty= 'l2', alpha= 1e-05, n_iter= 1000, fit_intercept= True)

	perceptron.fit(data_train, target_train)

	output = perceptron.predict(data_test)
	accuracyPerceptron.append(metrics.accuracy_score(target_test, output))

########################################################## Logistic Regression -> Verificar melhor hyper para chumbar

	print("Logistic Regression")
	
	logisticRegression = LogisticRegression(warm_start= True, C= 0.5, max_iter= 50, fit_intercept= True, 
		solver= 'liblinear', random_state= None, tol= 0.01, class_weight= 'balanced')
	logisticRegression.fit(data_train, target_train)

	output = logisticRegression.predict(data_test)
	accuracyLR.append(metrics.accuracy_score(target_test, output))

########################################################## Random Forest

	print("Random Forest")
	
	random_clf = RandomForestClassifier(bootstrap= False, min_samples_leaf= 4, n_estimators= 1092, 
		random_state= 3, criterion= 'entropy', max_features= 0.1567104988523027, max_depth= 3)
	random_clf.fit(data_train, target_train)

	output = random_clf.predict(data_test)
	accuracyRF.append(metrics.accuracy_score(target_test, output))

########################################################## SDGC -> Verificar melhor hyper para chumbar

	print("SGDC")

	sgdc_clf = SGDClassifier(loss= 'modified_huber', eta0= 1, fit_intercept= True, learning_rate= 'invscaling', penalty= 'l2', random_state= None, alpha= 1e-05)

	sgdc_clf.fit(data_train, target_train)

	output = sgdc_clf.predict(data_test)
	accuracySGDC.append(metrics.accuracy_score(target_test, output))


########################################################## One VS One - Ridge

	print("One VS One - Ridge")

	oneVsOneRidge = OneVsOneClassifier(RidgeClassifier(normalize= False, alpha= 0.1, solver= 'cholesky', tol= 0.001, fit_intercept= True))

	oneVsOneRidge.fit(data_train.toarray(), target_train)

	output = oneVsOneRidge.predict(data_test)
	accuracyOnevsOneRidge.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - SVC

	print("One VS One - SVC")
	
	oneVsOneSVC = OneVsOneClassifier(SVC(kernel= 'linear', C= 1, max_iter= -1, decision_function_shape= 'ovo', tol= 1.2, gamma= 'auto'))

	oneVsOneSVC.fit(data_train, target_train)

	output = oneVsOneSVC.predict(data_test.toarray())
	accuracyOnevsOneSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Linear SVC -> Verificar melhor hyper para chumbar

	print("One VS One - Linear SVC")
	
	oneVsOneLinearSVC = OneVsOneClassifier(LinearSVC(loss= 'hinge', C= 0.8, fit_intercept= False, max_iter= 100, multi_class= 'crammer_singer', tol= 0.1, class_weight= None))

	oneVsOneLinearSVC.fit(data_train, target_train)

	output = oneVsOneLinearSVC.predict(data_test.toarray())
	accuracyOnevsOneLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Extra Tress

	print("One VS One - Extra Tress")
	
	oneVsOneET = OneVsOneClassifier(ExtraTreesClassifier(max_features= 'auto', min_samples_split= 3, n_estimators= 2000, max_depth= None))

	oneVsOneET.fit(data_train, target_train)

	output = oneVsOneET.predict(data_test.toarray())
	accuracyOnevsOneET.append(metrics.accuracy_score(target_test, output))

########################################################## One VS Rest - Ridge

	print("One VS Rest - Ridge")

	oneVsRestRidge = OneVsRestClassifier(RidgeClassifier(normalize= False, alpha= 0.1, solver= 'cholesky', tol= 0.001, fit_intercept= True))

	oneVsRestRidge.fit(data_train.toarray(), target_train)

	output = oneVsRestRidge.predict(data_test)
	accuracyOneVSRestRidge.append(metrics.accuracy_score(target_test, output))


########################################################## One VS Rest - LinearSVC -> Verificar melhor hyper para chumbar

	print("One VS Rest - LinearSVC ")

	oneVsRestLinearSVC = OneVsRestClassifier(LinearSVC(loss= 'hinge', C= 0.8, fit_intercept= False, max_iter= 100, multi_class= 'crammer_singer', tol= 0.1, class_weight= None))

	oneVsRestLinearSVC.fit(data_train, target_train)

	output = oneVsRestLinearSVC.predict(data_test)
	accuracyOneVSRestLinearSVC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - SGDC -> Verificar melhor hyper para chumbar

	print("One VS One - SGDC")
	
	oneVsOneSGDC = OneVsOneClassifier(SGDClassifier(loss= 'modified_huber', eta0= 1, fit_intercept= True, learning_rate= 'invscaling', penalty= 'l2', random_state= None, alpha= 1e-05))

	oneVsOneSGDC.fit(data_train, target_train)

	output = oneVsOneSGDC.predict(data_test.toarray())
	accuracyOnevsOneSGDC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Random Forest

	print("One VS One - Random Forest")
	
	oneVsOneSGDC = OneVsOneClassifier(RandomForestClassifier(bootstrap= False, min_samples_leaf= 4, n_estimators= 1092, 
		random_state= 3, criterion= 'entropy', max_features= 0.1567104988523027, max_depth= 3))

	oneVsOneSGDC.fit(data_train, target_train)

	output = oneVsOneSGDC.predict(data_test.toarray())
	accuracyOnevsOneSGDC.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Logistic Regression -> Verificar melhor hyper para chumbar

	print("One VS One - Logistic Regression")
	
	oneVsOneLR = OneVsOneClassifier(LogisticRegression(warm_start= True, C= 0.5, max_iter= 50, fit_intercept= True, 
		solver= 'liblinear', random_state= None, tol= 0.01, class_weight= 'balanced'))

	oneVsOneLR.fit(data_train, target_train)

	output = oneVsOneLR.predict(data_test.toarray())
	accuracyOnevsOneLR.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - KNN -> Verificar melhor hyper para chumbar

	print("One VS One - KNN")
	
	oneVsOneKnn = OneVsOneClassifier(neighbors.KNeighborsClassifier(n_neighbors= 16, metric= 'euclidean', weights= 'distance', algorithm= 'auto'))

	oneVsOneKnn.fit(data_train, target_train)

	output = oneVsOneKnn.predict(data_test.toarray())
	accuracyOnevsOneKNN.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Naive Bayes -> Verificar melhor hyper para chumbar

	print("One VS One - Naive Bayes")
	
	oneVsOneNB = OneVsOneClassifier(MultinomialNB(alpha= 0.1, fit_prior= True))

	oneVsOneNB.fit(data_train, target_train)

	output = oneVsOneNB.predict(data_test.toarray())
	accuracyOnevsOneNB.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - MLP

	print("One VS One - MLP")
	
	oneVsOneMLP = OneVsOneClassifier(neural_network.MLPClassifier(solver= 'sgd', activation= 'identity', max_iter= 2000, alpha= 0.0015, 
		learning_rate= 'constant', hidden_layer_sizes= (800,)))

	oneVsOneMLP.fit(data_train, target_train)

	output = oneVsOneMLP.predict(data_test.toarray())
	accuracyOnevsOneMLP.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - BaggingClassifier

	print("One VS One - BaggingClassifier")
	
	oneVsOneBG = OneVsOneClassifier(BaggingClassifier(max_features= 0.035, max_samples= 0.9, random_state= 0, n_estimators= 500, bootstrap_features= True))

	oneVsOneBG.fit(data_train, target_train)

	output = oneVsOneBG.predict(data_test.toarray())
	accuracyOnevsOneBG.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - BaggingClassifierWithDT

	print("One VS One - BaggingClassifierWithDT")
	
	oneVsOneBGDT = OneVsOneClassifier(BaggingClassifier(base_estimator = tree.DecisionTreeClassifier(random_state= 42, criterion= 'gini', max_depth= 5), max_features= 0.47, 
		max_samples= 0.98, oob_score= True, random_state= 3, n_estimators= 50))

	oneVsOneBGDT.fit(data_train, target_train)

	output = oneVsOneBGDT.predict(data_test.toarray())
	accuracyOnevsOneBGDT.append(metrics.accuracy_score(target_test, output))

########################################################## One VS One - Perceptron

	print("One VS One - Perceptron")
	
	oneVsOnePerceptron = OneVsOneClassifier(Perceptron(penalty= 'l2', alpha= 1e-05, n_iter= 1000, fit_intercept= True))

	oneVsOnePerceptron.fit(data_train, target_train)

	output = oneVsOnePerceptron.predict(data_test.toarray())
	accuracyOnevsOnePerceptron.append(metrics.accuracy_score(target_test, output))


##### Ridge AVG
avg = np.mean(accuracyRidge)
top_avg = avg
std = np.std(accuracyRidge)
top_avg_name = "Ridge"

print("Ridge: " +str(accuracyRidge))
print("Ridge Media: " +str(avg))
print("\n")

##### LinearSVC AVG
avg = np.mean(accuracyLinearSVC)
std = np.std(accuracyLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LinearSVC"

print("LinearSVC: " +str(accuracyLinearSVC))
print("LinearSVC Media: " +str(avg))
print("\n")

##### SVC AVG
avg = np.mean(accuracySVC)
std = np.std(accuracySVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SVC"

print("SVC: " +str(accuracySVC))
print("SVC Media: " +str(avg))
print("\n")

##### Extra Trees AVG
avg = np.mean(accuracyET)
std = np.std(accuracyET)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Extra Trees"

print("Extra Trees: " +str(accuracyET))
print("Extra Trees Media: " +str(avg))
print("\n")

##### Perceptron AVG
avg = np.mean(accuracyPerceptron)
std = np.std(accuracyPerceptron)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Perceptron"

print("Perceptron: " +str(accuracyPerceptron))
print("Perceptron Media: " +str(avg))
print("\n")

##### RF AVG
avg = np.mean(accuracyRF)
std = np.std(accuracyRF)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "RandomForest"

print("RandomForest: " +str(accuracyRF))
print("RandomForest Media: " +str(avg))
print("\n")

##### MLP AVG
avg = np.mean(accuracyMLP)
std = np.std(accuracyMLP)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "MLP"

print("MLP: " +str(accuracyMLP))
print("MLP Media: " +str(avg))
print("\n")

##### Logistic Regression AVG
avg = np.mean(accuracyLR)
std = np.std(accuracyLR)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "LogisticRegression"

print("LogisticRegression: " +str(accuracyLR))
print("LogisticRegression Media: " +str(avg))
print("\n")

##### SGDC AVG
avg = np.mean(accuracySGDC)
std = np.std(accuracySGDC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "SGDClassifier"

print("SGDClassifier: " +str(accuracySGDC))
print("SGDClassifier Media: " +str(avg))
print("\n")

##### KNN AVG
avg = np.mean(accuracyKnn)
std = np.std(accuracyKnn)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Knn"

print("Knn: " +str(accuracyKnn))
print("Knn Media: " +str(avg))
print("\n")

##### Naive Bayes AVG
avg = np.mean(accuracyNB)
std = np.std(accuracyNB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Naive Bayes"

print("Naive Bayes: " +str(accuracyNB))
print("Naive Bayes Media: " +str(avg))
print("\n")

##### Bagging AVG
avg = np.mean(accuracyBG)
std = np.std(accuracyBG)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "BaggingClassifier"

print("BaggingClassifier: " +str(accuracyBG))
print("BaggingClassifier Media: " +str(avg))
print("\n")

##### Bagging with DT AVG
avg = np.mean(accuracyBGDT)
std = np.std(accuracyBGDT)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Bagging with DT"

print("Bagging with DT: " +str(accuracyBGDT))
print("Bagging with DT Media: " +str(avg))
print("\n")


##### One vs One - Ridge AVG
avg = np.mean(accuracyOnevsOneRidge)
std = np.std(accuracyOnevsOneRidge)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - Ridge AVG"

print("One vs One - Ridge AVG: " +str(accuracyOnevsOneRidge))
print("One vs One - Ridge AVG Media: " +str(avg))
print("\n")

##### One vs One - LinearSVC AVG
avg = np.mean(accuracyOnevsOneLinearSVC)
std = np.std(accuracyOnevsOneLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - LinearSVC AVG"

print("One vs One - LinearSVC AVG: " +str(accuracyOnevsOneLinearSVC))
print("One vs One - LinearSVC AVG Media: " +str(avg))
print("\n")

##### One vs One - SVC AVG
avg = np.mean(accuracyOnevsOneSVC)
std = np.std(accuracyOnevsOneSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - SVC AVG"

print("One vs One - SVC AVG: " +str(accuracyOnevsOneSVC))
print("One vs One - SVC AVG Media: " +str(avg))
print("\n")

##### One vs One - Extra Trees AVG
avg = np.mean(accuracyOnevsOneET)
std = np.std(accuracyOnevsOneET)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS One- Extra Trees"

print("One VS One- Extra Trees: " +str(accuracyOnevsOneET))
print("One VS One- Extra Trees Media: " +str(avg))
print("\n")

##### One vs Rest- Ridge AVG
avg = np.mean(accuracyOneVSRestRidge)
std = np.std(accuracyOneVSRestRidge)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS Rest - Ridge"

print("One VS Rest - Ridge: " +str(accuracyOneVSRestRidge))
print("One VS Rest - Ridge: " +str(avg))
print("\n")

##### One vs Rest - Linear SVC AVG
avg = np.mean(accuracyOneVSRestLinearSVC)
std = np.std(accuracyOneVSRestLinearSVC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs Rest - Linear SVC"

print("One vs Rest - Linear SVC: " +str(accuracyOneVSRestLinearSVC))
print("One vs Rest - Linear SVC: " +str(avg))
print("\n")


##### One VS One Perceptron AVG
avg = np.mean(accuracyOnevsOnePerceptron)
std = np.std(accuracyOnevsOnePerceptron)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS One - Perceptron"

print("One VS One -  Perceptron: " +str(accuracyOnevsOnePerceptron))
print("One VS One - Perceptron Media: " +str(avg))
print("\n")

##### One VS One RF AVG
avg = np.mean(accuracyOnevsOneRF)
std = np.std(accuracyOnevsOneRF)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS One - RF"

print("One VS One - RF AVG: " +str(accuracyOnevsOneRF))
print("One VS One - RF AVG Media: " +str(avg))
print("\n")

##### One VS One MLP AVG
avg = np.mean(accuracyOnevsOneMLP)
std = np.std(accuracyOnevsOneMLP)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - MLP"

print("One VS One - MLP AVG: " +str(accuracyOnevsOneMLP))
print("One VS One - MLP AVG Media: " +str(avg))
print("\n")

##### One VS One - Logistic Regression AVG
avg = np.mean(accuracyOnevsOneLR)
std = np.std(accuracyOnevsOneLR)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One VS One - Logistic Regression"

print("One VS One - Logistic Regression: " +str(accuracyOnevsOneLR))
print("One VS One - Logistic Regression Media: " +str(avg))
print("\n")

##### One vs One - SGDC AVG
avg = np.mean(accuracyOnevsOneSGDC)
std = np.std(accuracyOnevsOneSGDC)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - SGDC"

print("One vs One - SGDC: " +str(accuracyOnevsOneSGDC))
print("One vs One - SGDC Media: " +str(avg))
print("\n")

##### One vs One - KNN AVG
avg = np.mean(accuracyOnevsOneKNN)
std = np.std(accuracyOnevsOneKNN)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - KNN"

print("One vs One - KNN: " +str(accuracyOnevsOneKNN))
print("One vs One - KNN Media: " +str(avg))
print("\n")

##### One vs One - Naive Bayes AVG
avg = np.mean(accuracyOnevsOneNB)
std = np.std(accuracyOnevsOneNB)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "Naive Bayes"

print("One vs One - Naive Bayes: " +str(accuracyOnevsOneNB))
print("One vs One - Naive Bayes Media: " +str(avg))
print("\n")

##### One vs One - Bagging AVG
avg = np.mean(accuracyOnevsOneBG)
std = np.std(accuracyOnevsOneBG)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - Bagging"

print("One vs One - Bagging: " +str(accuracyOnevsOneBG))
print("One vs One - Bagging Media: " +str(avg))
print("\n")

##### One vs One - Bagging with DT AVG
avg = np.mean(accuracyOnevsOneBGDT)
std = np.std(accuracyOnevsOneBGDT)

if avg > top_avg:
	top_avg = avg
	top_avg_name = "One vs One - Bagging with DT"

print("One vs One - Bagging with DT: " +str(accuracyOnevsOneBG))
print("One vs One - Bagging with DT Media: " +str(avg))
print("\n")


print("\n")

print("TOP AVG IS " + top_avg_name +str(": ") +str(top_avg))

print("\n\n")

elapsed_time_seconds = time.time() - t0

elapsed_time_minutes = int(elapsed_time_seconds) / 60

print("Tempo total executando: %.2fm" % (elapsed_time_minutes))